const Consts = require('./constants.js');
const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');

const Animal = require('./models/animal');
const Expenses = require('./models/expenses');
const Donate = require('./models/donate');
const sequelize = require('./config/database')
const db = require('./config/database');
let crypto = require("crypto");
let LiqPay = require("liqpayjs-sdk");
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const sosEmail = require('./mail_forms/sosMailReturner');

let order_id;
let email, id;

db.authenticate()
  .then(() => console.log('Database connected...'))
  .catch(err => console.log('Error: ' + err))

const app = express();
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
	next();
});

app.use(bodyParser.json());

app.use('/api/users/authenticate', require('./routes/user_auth'));
app.use('/api/donates', require('./routes/donate'));
app.use('/api/sos', require('./routes/sos'));
app.use('/api/expenses', require('./routes/expenses'));
app.use('/api/reports', require('./routes/report'));
app.use('/api/users', require('./routes/user'));
app.use('/api/animals', require('./routes/animal'));
app.use('/api/animal_images', require('./routes/animal_image'));
app.use(passport.initialize());
require('./passport/passport')(passport);
app.use('/api/role_requests', require('./routes/role_request'));
app.use('/api/shelter_contacts', require('./routes/shelter_contacts'));
app.use('/api/volunteer_images', require('./routes/volunteer'));

function chartToArrays(chart) {
  let types = [],
    amounts = [];

  chart.forEach(el => {
    types.push((typeof el.type === 'string') ? el.type.charAt(0).toUpperCase() + el.type.substring(1) : el.type);
    amounts.push(el.amount)
  });
  return {
    types,
    amounts
  }
}

app.put('/api')

app.get('/api/charts_data', (req, res) => {
  let responseObject = [];
  Expenses.findAll({
    attributes: ['type', [sequelize.fn('sum', sequelize.col('cash_amount')), 'amount']],
    where: sequelize.literal(`date between date_trunc('month', CURRENT_DATE) + interval '- 1 month' 
                              and date_trunc('month', CURRENT_DATE) + interval '- 1 day'`),
    group : ['type'],
    raw: true,
  })
  .then(result => {
    if (result.length > 0)responseObject.push({name:'Last month expenses', data: chartToArrays(result)})
    Animal.findAll({
      attributes: [['kind', 'type'], [sequelize.fn('count', sequelize.col('kind')), 'amount']],
      group : ['kind'],
      raw: true,
    })
    .then(result => {
      responseObject.push({name:'Animals', data: chartToArrays(result)})
      Animal.findAll({
        attributes: [['is_adopted', 'type'], [sequelize.fn('count', sequelize.col('is_adopted')), 'amount']],
        group : ['is_adopted'],
        raw: true,
      })
      .then(result => {
        result.map(el => {
          el.type = (el.type)? 'Adopted' : 'In shelter'
        })
        responseObject.push({name:'Adopted', data: chartToArrays(result)})
      })
      .then(() => {res.json(responseObject)})
    })
  })
  .catch(err => console.log(err));
})

app.get('/donates_url/:amount/:email/:id', (req, res) => {
  let liqpay = new LiqPay(Consts.PUBLIC_KEY, Consts.PRIVATE_KEY);
  let text = crypto.randomBytes(12).toString('hex').toUpperCase();
  order_id = text;
  email = req.params.email;
  id = req.params.id
  let html = liqpay.cnb_object({
    'action'         : 'paydonate',
    'amount'         : req.params.amount,
    'currency'       : 'UAH',
    'description'    : 'Дякуємо вам за пожертву',
    'version'        : '3',
    'result_url'     : Consts.SITE_URL,
    'product_url'    : Consts.SITE_URL,
    'server_url'     : Consts.SERVER_URL + '/status',
    'sender_address' : email,
    'sandbox'        : '1',
    "order_id"       : text,
    });
    if (+req.params.amount <= 0)
    res.sendStatus(400)
  res.json(html)
})

app.get('/status/', (req, res) => {
  let liqpay = new LiqPay(Consts.PUBLIC_KEY, Consts.PRIVATE_KEY);
  liqpay.api("request", {
  "action"   : "status",
  "version"  : "3",
  "order_id" : order_id
  }, function( json ){
    let now = new Date();
    let curr_date = now.getDate();
    let curr_month = now.getMonth() + 1;
    if (curr_month <= 9) {
      curr_month = "0" + curr_month;
    }
    if (curr_date <= 9) {
      curr_date = "0" + curr_date;
    }
    // let curr_year = now.getFullYear();
    // now = curr_year + "-" + curr_month + "-" + curr_date;

    if (json.status == 'sandbox') {
      let transporter = nodemailer.createTransport(smtpTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
          user: Consts.SHELTER_EMAIL,
          pass: Consts.DB_PASS
        }
      }));

      if (email == "null") {
        email = "anonymous";
      }
      if (id == "null") {
        id = null;
      }

      const output = `
      <h1>You have a new donate</h1>
      <h2>  Donate Details</h2>
      <ul>  
        <li><h3>Email: ${email}</h3></li>
        <li><h3>Cash amount: ${json.amount} ${json.currency}</h3></li>
        <li><h3>Date of donation: ${new Date()}</h3></li>
      </ul>
      `;

      var mailOptions = {
        to: Consts.SHELTER_EMAIL,
        subject: 'Shelter donate',
        text: 'Donate request',
        html: output,
      };

      var mailOptions2 = {
        from: Consts.SHELTER_EMAIL,
        to: email,
        subject: 'Info about volunteering',
        text: 'How is good to be volunteer?',
        html: sosEmail.returner(),
      };

      transporter.sendMail(mailOptions, function (error, info) {
        (error) ? console.log(error): console.log('Email sent: ' + info.response);
      });

      transporter.sendMail(mailOptions2, function (error, info) {
        (error) ? console.log(error): console.log('Email sent: ' + info.response);
      });
      
      Donate.create({
              date: now,
              cash_amount: json.amount,
              user_id: id,
              email: email
          })
          .then(function (rowCreated) {
            if (rowCreated === 1) {
              console.log('Created successfully');
            }
          }, function (err) {
            console.log(err);
          })
          .catch(err => {
            console.log('ERROR');
            console.log(err)
          });
      }

      order_id = null;

  }, (err, res) => {
    // console.log(err, res)
  })
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server started on port ${PORT}`));

module.exports = 
    {returner(url){
        return `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>index</title>
            <style type="text/css">
                html {
                    -webkit-text-size-adjust: none;
                    -ms-text-size-adjust: none;
                }
        
                @media only screen and (min-device-width: 660px) {
                    .table660 {
                        width: 660px !important;
                    }
                }
        
                @media only screen and (max-device-width: 660px),
                only screen and (max-width: 660px) {
                    table[class="table660"] {
                        width: 100% !important;
                    }
                }
        
                .table660 {
                    width: 660px;
                }
            </style>
        </head>
        
        <body style="margin: 0; padding: 0;">
        
            <table cellpadding="0" cellspacing="0" border="0" width="100%"
                style="background:linear-gradient(45deg, #f18c26 30%, #009688 90%); min-width: 340px; font-size: 1px; line-height: normal; height: 100vh;;">
                <tr>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="660" class="table660"
                            style="width: 100%; max-width: 660px; min-width: 340px; background: #eeeeee; margin-top: 25vh;">
                            <tr>
                                <td align="center" valign="top">
        
                                    <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                        style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                        <tr>
                                            <td align="left" valign="top">
                                                <div style="height: 30px; line-height: 30px; font-size: 28px;">&nbsp;</div>
                                                <div style="font-size: 22px;
                                                font-family: 'Bowlby One SC', cursive;
                                                color: black;">
                                                    Shelter Cat&Dog
                                                </div>
                                                <div style="height: 18px; line-height: 18px; font-size: 16px;">&nbsp;</div>
                                            </td>
                                        </tr>
                                    </table>
        
        
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%"
                                        style="width: 100% !important; min-width: 100%; max-width: 100%; border-radius: 5px; background: #ffffff;">
                                        <tr>
                                            <td align="center" valign="top">
        
                                                <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                                    style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <div style="height: 20px; line-height: 32px; font-size: 30px;">
                                                                &nbsp;</div>
                                                            <span
                                                                style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #151515; font-size: 24px; line-height: 20px;">
                                                                <h3>Email
                                                                    Confirmation</h3>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                                    style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <div style="height: 15px; line-height: 32px; font-size: 30px;">
                                                                &nbsp;</div>
                                                            <span
                                                                style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #151515; font-size: 16px; line-height: 20px;">Dear
                                                                friend thanks for joining our community, pass throught the link
                                                                below to confirm your account</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                                    style="width: 91% !important; min-width: 91% !important; max-width: 91% !important;">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <div
                                                                style="height: 15px; word-wrap: break-word; line-height: 32px; font-size: 30px;">
                                                                &nbsp;</div>
        
                                                            <div style="word-wrap: break-word; max-width: 600px;">
                                                                <a href=${url}
                                                                    style="font-family: Arial, Tahoma, Geneva, sans-serif; word-wrap: break-word !important; width: 500px !important; color: #151515; font-size: 10px; line-height: 20px;">
                                                                    ${url}
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="height: 20px; line-height: 5px; font-size: 3px;">&nbsp;</div>
                                                <div style="height: 35px; line-height: 5px; font-size: 3px;">&nbsp;</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                        style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <span
                                                    style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #9fa3b0; font-size: 12px; line-height: 18px;">Respectfully administration of web-site <a href="http://localhost:3000/" target="_blank"
                                                        style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #9fa3b0; font-size: 12px; line-height: 18px; text-decoration: none;">SHELTER</a>.</span>
                                                <div style="height: 3px; line-height: 40px; font-size: 38px;">&nbsp;</div>
                                                {disableEmailLink}
                                                <div style="height: 20px; line-height: 40px; font-size: 38px;">&nbsp;</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        
        </html>`
    }
}
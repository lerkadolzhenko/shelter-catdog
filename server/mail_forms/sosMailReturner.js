module.exports = {
    returner(){
        return  (`<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>index</title>
    <style type="text/css">
        html {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        @media only screen and (min-device-width: 660px) {
            .table660 {
                width: 660px !important;
            }
        }

        @media only screen and (max-device-width: 660px),
        only screen and (max-width: 660px) {
            table[class="table660"] {
                width: 100% !important;
            }
        }

        .table660 {
            width: 880px;
        }
    </style>
</head>

<body style="margin: 0; padding: 0;">

    <table cellpadding="0" cellspacing="0" border="0" width="100%"
        style="background:linear-gradient(45deg, #f18c26 30%, #009688 90%); min-width: 340px; font-size: 1px; line-height: normal; height: 100vh;;">
        <tr>
            <td align="center" valign="top">
                <table cellpadding="0" cellspacing="0" border="0" width="880" class="table660"
                    style="width: 100%; max-width: 1600px; min-width: 880px; background: #eeeeee; margin-top: 6vh;">
                    <tr>
                        <td align="center" valign="top">

                            <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                <tr>
                                    <td align="left" valign="top">
                                        <div style="height: 30px; line-height: 30px; font-size: 28px;">&nbsp;</div>
                                        <div style="font-size: 22px;
                                        font-family: 'Bowlby One SC', cursive;
                                        color: black;">
                                            Shelter Cat&Dog
                                        </div>
                                        <div style="height: 18px; line-height: 18px; font-size: 16px;">&nbsp;</div>
                                    </td>
                                </tr>
                            </table>


                            <table cellpadding="0" cellspacing="0" border="0" width="100%"
                                style="width: 100% !important; min-width: 100%; max-width: 100%; border-radius: 5px; background: #ffffff;">
                                <tr>
                                    <td align="center" valign="top">

                                        <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                            style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <div style="height: 20px; line-height: 32px; font-size: 30px;">
                                                        &nbsp;</div>
                                                    <span
                                                        style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #151515; font-size: 24px; line-height: 20px;">
                                                        <h3>The benefits of being a volunteer</h3>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                            style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <div style="height: 15px; line-height: 32px; font-size: 30px;">
                                                        &nbsp;</div>
                                                    <span
                                                        style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #151515; font-size: 16px; line-height: 20px;">
                                                        <style>
                                                            ol { list-style: none; }
                                                        </style>
                                                        <style>
                                                            ol li:before { font-weight: bold; }
                                                        </style>
                                                            <div style="font-size: 16pt; margin-top: 1.5em; font-weight: 800;"><img src="https://static.thenounproject.com/png/41317-200.png" alt="paw" style="height: 30px; width: 30px; position: absolute;"><span style="margin-left: 20px; vertical-align: top;"> Reveals unconditional love!</span></div>
                                                            <div style="width: 90%; margin: 0 auto 5px auto; margin-left: 60px;">
                                                                Shelter animals have a lot of love to give. Even if you are not in a position to adopt, you can still get your daily 
                                                                or weekly dose of furry love by spending time with some very lucky critters with no shortage of appreciation.
                                                            </div>
                                                            <div style="font-size: 16pt; margin-top: 1.5em; font-weight: 800;"><img src="https://static.thenounproject.com/png/41317-200.png" alt="paw" style="height: 30px; width: 30px; position: absolute;"><span style="margin-left: 20px; vertical-align: top; margin-top: 5px;"> Prepares you to adopt</span></div>
                                                            <div style="width: 90%; margin: 0 auto 5px auto; margin-left: 60px;">
                                                                Are you considering adopting? Volunteering helps you decide if it’s the right time for you to adopt a shelter pet and can lead you to your new best friend. 
                                                                You’ll learn quickly which behaviours you are willing to work with and what type of pet is the best fit for your family.
                                                            </div>   
                                                            <div style="font-size: 16pt; margin-top: 1.5em; font-weight: 800;"><img src="https://static.thenounproject.com/png/41317-200.png" alt="paw" style="height: 30px; width: 30px; position: absolute;"><span style="margin-left: 20px; vertical-align: top;"> Brings groups together</span></div>
                                                            <div style="width: 90%; margin: 0 auto 5px auto; margin-left: 60px;">
                                                                Volunteering isn’t just fun, it’s a free group outing! Grab your friends or coworkers and spend an hour or two playing with cats and walking the dogs. 
                                                                It’s a great bonding activity that inspires others to begin volunteering on their own.
                                                            </div>
                                                            <div style="font-size: 16pt; margin-top: 1.5em; font-weight: 800;"><img src="https://static.thenounproject.com/png/41317-200.png" alt="paw" style="height: 30px; width: 30px; position: absolute;"><span style="margin-left: 20px; vertical-align: top;"> Increases physical activity</span></div>
                                                            <div style="width: 90%; margin: 0 auto 5px auto; margin-left: 60px;">
                                                                Dog walking at your local shelter is a great way to stay active, in addition to giving the dogs a break from their kennels. 
                                                                It provides you and the dog with physical and mental stimulation and releases hormones that just make you feel good (see above)!
                                                            </div>
                                                            <div style="font-size: 16pt; margin-top: 1.5em; font-weight: 800;"><img src="https://static.thenounproject.com/png/41317-200.png" alt="paw" style="height: 30px; width: 30px; position: absolute;"><span style="margin-left: 20px; vertical-align: top;"> Boosts mood and de-stresses</span></div>
                                                            <div style="width: 90%; margin: 0 auto 5px auto; margin-left: 60px;">
                                                                Volunteering just feels good! Time spent with animals is never wasted; it’s a proven de-stressor that enhances your lifestyle and emotional health. 
                                                                You also cheer up the kitties and pups who are excited to meet you!
                                                            </div>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                            style="width: 91% !important; min-width: 91% !important; max-width: 91% !important;">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <div
                                                        style="height: 15px; word-wrap: break-word; line-height: 32px; font-size: 30px;">
                                                        &nbsp;</div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div style="height: 20px; line-height: 5px; font-size: 3px;">&nbsp;</div>
                                        <div style="height: 35px; line-height: 5px; font-size: 3px;">&nbsp;</div>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="91%"
                                style="width: 91% !important; min-width: 91%; max-width: 91%;">
                                <tr>
                                    <td align="center" valign="top" style='padding: 15px 0'>
                                        <span
                                            style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #9fa3b0; font-size: 13px; line-height: 18px;">
                                            You have received this email, because you subscribed to the newsletter on our website <a href="http://localhost:3000/" target="_blank"
                                                style="font-family: Arial, Tahoma, Geneva, sans-serif; color: #9fa3b0; font-size: 13px; line-height: 18px;">SHELTER</a>.</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>`)
    }
}
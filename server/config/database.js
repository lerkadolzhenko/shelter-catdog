const Sequelize = require('sequelize');

const  sequelize = new Sequelize('postgres', 'postgres', 'ololoshka378', {
  host: 'localhost',
  dialect: 'postgres',
  operatorsAliases: false,
  schema: 'shelter',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

module.exports = sequelize;
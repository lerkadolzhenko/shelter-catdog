--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

-- Started on 2019-04-24 15:45:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16822)
-- Name: shelter; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA shelter;


ALTER SCHEMA shelter OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 661 (class 1247 OID 16568)
-- Name: users_role; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.users_role AS ENUM (
    'admin',
    'volunteer',
    'default_user'
);


ALTER TYPE public.users_role OWNER TO postgres;

--
-- TOC entry 608 (class 1247 OID 16824)
-- Name: expenses_type; Type: TYPE; Schema: shelter; Owner: postgres
--

CREATE TYPE shelter.expenses_type AS ENUM (
    'Workers',
    'Food',
    'Rent',
    'Medicine'
);


ALTER TYPE shelter.expenses_type OWNER TO postgres;

--
-- TOC entry 611 (class 1247 OID 16834)
-- Name: health_status; Type: TYPE; Schema: shelter; Owner: postgres
--

CREATE TYPE shelter.health_status AS ENUM (
    'sos',
    'healthy',
    'average'
);


ALTER TYPE shelter.health_status OWNER TO postgres;

--
-- TOC entry 614 (class 1247 OID 16842)
-- Name: kind; Type: TYPE; Schema: shelter; Owner: postgres
--

CREATE TYPE shelter.kind AS ENUM (
    'cat',
    'dog',
    'parrot'
);


ALTER TYPE shelter.kind OWNER TO postgres;

--
-- TOC entry 617 (class 1247 OID 16850)
-- Name: role; Type: TYPE; Schema: shelter; Owner: postgres
--

CREATE TYPE shelter.role AS ENUM (
    'admin',
    'volunteer',
    'user'
);


ALTER TYPE shelter.role OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 16857)
-- Name: animals; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.animals (
    id integer NOT NULL,
    kind shelter.kind NOT NULL,
    name text NOT NULL,
    is_boy boolean NOT NULL,
    arrival_date text NOT NULL,
    is_sterilized boolean NOT NULL,
    birth_date text NOT NULL,
    health_status shelter.health_status NOT NULL,
    is_adopted boolean NOT NULL,
    user_id integer,
    about text NOT NULL
);


ALTER TABLE shelter.animals OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16863)
-- Name: animal_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.animal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.animal_id_seq OWNER TO postgres;

--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 199
-- Name: animal_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.animal_id_seq OWNED BY shelter.animals.id;


--
-- TOC entry 200 (class 1259 OID 16865)
-- Name: animal_images; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.animal_images (
    id integer NOT NULL,
    name text NOT NULL,
    link text NOT NULL,
    animal_id integer
);


ALTER TABLE shelter.animal_images OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16871)
-- Name: animal_image_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.animal_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.animal_image_id_seq OWNER TO postgres;

--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 201
-- Name: animal_image_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.animal_image_id_seq OWNED BY shelter.animal_images.id;


--
-- TOC entry 202 (class 1259 OID 16873)
-- Name: shelter_contacts; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.shelter_contacts (
    id integer NOT NULL,
    shelter_phone text NOT NULL,
    shelter_email text NOT NULL,
    shelter_address text NOT NULL,
    shelter_map_link text NOT NULL
);


ALTER TABLE shelter.shelter_contacts OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16879)
-- Name: contacts_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.contacts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.contacts_id_seq OWNER TO postgres;

--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 203
-- Name: contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.contacts_id_seq OWNED BY shelter.shelter_contacts.id;


--
-- TOC entry 204 (class 1259 OID 16881)
-- Name: donates; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.donates (
    id integer NOT NULL,
    date timestamp(4) with time zone NOT NULL,
    cash_amount double precision NOT NULL,
    user_id integer,
    email text
);


ALTER TABLE shelter.donates OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16887)
-- Name: donate_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.donate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.donate_id_seq OWNER TO postgres;

--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 205
-- Name: donate_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.donate_id_seq OWNED BY shelter.donates.id;


--
-- TOC entry 206 (class 1259 OID 16889)
-- Name: expenses; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.expenses (
    id integer NOT NULL,
    date date NOT NULL,
    cash_amount double precision NOT NULL,
    type shelter.expenses_type NOT NULL
);


ALTER TABLE shelter.expenses OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16892)
-- Name: expanses_expanses_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.expanses_expanses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.expanses_expanses_id_seq OWNER TO postgres;

--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 207
-- Name: expanses_expanses_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.expanses_expanses_id_seq OWNED BY shelter.expenses.id;


--
-- TOC entry 208 (class 1259 OID 16894)
-- Name: reports; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.reports (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    text text NOT NULL,
    creation_date date NOT NULL
);


ALTER TABLE shelter.reports OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16900)
-- Name: report_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.report_id_seq OWNER TO postgres;

--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 209
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.report_id_seq OWNED BY shelter.reports.id;


--
-- TOC entry 210 (class 1259 OID 16902)
-- Name: role_requests; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.role_requests (
    role shelter.role NOT NULL,
    is_approved boolean NOT NULL,
    user_id integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE shelter.role_requests OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16905)
-- Name: role_requests_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.role_requests_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.role_requests_id_seq OWNER TO postgres;

--
-- TOC entry 2955 (class 0 OID 0)
-- Dependencies: 211
-- Name: role_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.role_requests_id_seq OWNED BY shelter.role_requests.id;


--
-- TOC entry 212 (class 1259 OID 16907)
-- Name: sos; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.sos (
    id integer NOT NULL,
    description text NOT NULL
);


ALTER TABLE shelter.sos OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16913)
-- Name: sos_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.sos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.sos_id_seq OWNER TO postgres;

--
-- TOC entry 2956 (class 0 OID 0)
-- Dependencies: 213
-- Name: sos_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.sos_id_seq OWNED BY shelter.sos.id;


--
-- TOC entry 214 (class 1259 OID 16915)
-- Name: users; Type: TABLE; Schema: shelter; Owner: postgres
--

CREATE TABLE shelter.users (
    id integer NOT NULL,
    username character varying(100) NOT NULL,
    fullname character varying(150) NOT NULL,
    email character varying(100) NOT NULL,
    password text NOT NULL,
    image text,
    role shelter.role,
    phone text,
    is_confirmed boolean DEFAULT false NOT NULL
);


ALTER TABLE shelter.users OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16922)
-- Name: user_id_seq; Type: SEQUENCE; Schema: shelter; Owner: postgres
--

CREATE SEQUENCE shelter.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shelter.user_id_seq OWNER TO postgres;

--
-- TOC entry 2957 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: shelter; Owner: postgres
--

ALTER SEQUENCE shelter.user_id_seq OWNED BY shelter.users.id;


--
-- TOC entry 2758 (class 2604 OID 16924)
-- Name: animal_images id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.animal_images ALTER COLUMN id SET DEFAULT nextval('shelter.animal_image_id_seq'::regclass);


--
-- TOC entry 2757 (class 2604 OID 16925)
-- Name: animals id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.animals ALTER COLUMN id SET DEFAULT nextval('shelter.animal_id_seq'::regclass);


--
-- TOC entry 2760 (class 2604 OID 16926)
-- Name: donates id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.donates ALTER COLUMN id SET DEFAULT nextval('shelter.donate_id_seq'::regclass);


--
-- TOC entry 2761 (class 2604 OID 16927)
-- Name: expenses id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.expenses ALTER COLUMN id SET DEFAULT nextval('shelter.expanses_expanses_id_seq'::regclass);


--
-- TOC entry 2762 (class 2604 OID 16928)
-- Name: reports id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.reports ALTER COLUMN id SET DEFAULT nextval('shelter.report_id_seq'::regclass);


--
-- TOC entry 2763 (class 2604 OID 16929)
-- Name: role_requests id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.role_requests ALTER COLUMN id SET DEFAULT nextval('shelter.role_requests_id_seq'::regclass);


--
-- TOC entry 2759 (class 2604 OID 16930)
-- Name: shelter_contacts id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.shelter_contacts ALTER COLUMN id SET DEFAULT nextval('shelter.contacts_id_seq'::regclass);


--
-- TOC entry 2764 (class 2604 OID 16931)
-- Name: sos id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.sos ALTER COLUMN id SET DEFAULT nextval('shelter.sos_id_seq'::regclass);


--
-- TOC entry 2766 (class 2604 OID 16932)
-- Name: users id; Type: DEFAULT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.users ALTER COLUMN id SET DEFAULT nextval('shelter.user_id_seq'::regclass);


--
-- TOC entry 2927 (class 0 OID 16865)
-- Dependencies: 200
-- Data for Name: animal_images; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (1, 'image_1', 'https://ak9.picdn.net/shutterstock/videos/9622919/thumb/1.jpg', 1);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (2, 'image_2', 'https://previews.123rf.com/images/dixi_/dixi_0907/dixi_090700080/5273810-corgi-dog-on-white-background.jpg', 2);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (3, 'image_3', 'https://previews.123rf.com/images/fotomandm/fotomandm1512/fotomandm151200503/48928987-portrait-of-brown-eyed-cat-isolated-on-white-background.jpg', 3);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (4, 'image_4', 'https://w-dog.net/wallpapers/6/6/553278282228750.jpg', 4);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (5, 'image_5', 'https://images.wallpaperscraft.com/image/dog_white_background_white_78692_2950x2094.jpg', 5);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (6, 'image_6', 'https://image.shutterstock.com/image-photo/red-cat-isolated-on-white-260nw-384161581.jpg', 6);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (7, 'image_7', 'https://s3.envato.com/files/210241966/lxrlertje.jpg', 13);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (8, 'image_8', 'https://image.shutterstock.com/image-photo/bengal-cat-on-white-background-260nw-418680172.jpg', 8);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (9, 'image_9', 'https://cdn.wallpapersafari.com/38/59/uWqomU.jpg', 9);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (10, 'image_10', 'https://img3.goodfon.com/wallpaper/nbig/8/2b/kot-kotenok-seryy-belyy-fon.jpg', 10);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (11, 'image_11', 'http://www.allwhitebackground.com/images/3/3328.jpg', 11);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (12, 'image_12', 'https://www.wallpaperup.com/uploads/wallpapers/2014/02/27/280552/368715d66d25a80cc598f6fc817e659a.jpg', 12);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (13, 'image_13', 'https://thumbs.dreamstime.com/z/shiba-inu-dog-white-background-red-30968200.jpg', 13);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (14, 'image_14', 'https://imgc.artprintimages.com/img/print/sphynx-cat-1-year-old-in-front-of-white-background_u-l-q1a0nsx0.jpg?h=550&w=550', 12);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (15, 'image_15', 'https://i.pinimg.com/474x/50/70/c1/5070c1110fb8b140d8ceab4ac33ad0d7.jpg', 8);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (16, 'image_16', 'https://media.gettyimages.com/photos/black-cat-lying-on-a-white-background-looking-at-camera-picture-id528428665?b=1&k=6&m=528428665&s=612x612&w=0&h=zj_y0yLbShothVtyHtb2ApyN8UZPtQxMvad6N_3ELwo=', 10);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (17, 'img', 'https://st2.depositphotos.com/3470897/6325/i/950/depositphotos_63252833-stock-photo-shiba-inu-sits-on-a.jpg', 13);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (18, 'img', 'https://previews.123rf.com/images/solkaeolen/solkaeolen1208/solkaeolen120800001/22734010-sonriendo-y-gui%C3%B1ando-retrato-divertido-perro-rojo-sobre-fondo-blanco-shiba-inu-raza.jpg', 13);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (19, 'img', 'https://http2.mlstatic.com/wallmonkeys-akita-inu-purebred-puppy-dog-isolated-on-white-D_NQ_NP_779697-MLM26826516546_022018-F.jpg', 13);
INSERT INTO shelter.animal_images (id, name, link, animal_id) VALUES (20, 'img', 'https://w-dog.net/wallpapers/6/6/553278282228750.jpg', 7);


--
-- TOC entry 2925 (class 0 OID 16857)
-- Dependencies: 198
-- Data for Name: animals; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (4, 'dog', 'Poet', true, '2018-12-19', false, '2017-01-03', 'sos', false, NULL, 'Well hello there, I am the adorable Jinx! Are you looking for a running buddy? A hiking partner? Then I am your guy! I arrived at the shelter as a stray so the staff and volunteers are just learning about me, I can get pretty excited and amped up in the kennels and have not been too receptive with other dogs in my space. It`s always a good idea to take it slow and easy when introducing a new dog to resident pets. I am a high octane boy who would do best in an active household - but the good news is that I know lots of stuff and I do settle down once I`ve had some physical and mental exercise. I would do best in a home with teenagers or adults. Think we may be a great match? Come meet me today!');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (5, 'dog', 'Snickers', false, '2019-02-21', true, '2017-01-03', 'healthy', true, NULL, 'Well hello there, I am the adorable Jinx! Are you looking for a running buddy? A hiking partner? Then I am your guy! I arrived at the shelter as a stray so the staff and volunteers are just learning about me, I can get pretty excited and amped up in the kennels and have not been too receptive with other dogs in my space. It`s always a good idea to take it slow and easy when introducing a new dog to resident pets. I am a high octane boy who would do best in an active household - but the good news is that I know lots of stuff and I do settle down once I`ve had some physical and mental exercise. I would do best in a home with teenagers or adults. Think we may be a great match? Come meet me today!');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (6, 'cat', 'Pushok', true, '2019-03-05', true, '2017-01-03', 'average', false, NULL, 'Hello there! My name is Mishka I am a 2 year old neutered male who is looking for a very special home. I`m a very mellow, shy dude. I enjoy laying in the sun, climbing up high and just doing my own thing. I`m not big on snuggling, I would like to find my forever home with someone who just wants to hang out and let me chill. I`m big on climbing onto windows, cabinets and counters....I will need a tall cat tree or something similar to let me enjoy the heights. I love my treats and I didn`t mind my pomeranian dog roommate at my foster home and I`d probably get along with other dogs or cats as long as they aren`t chasing me or too noisy. 
We`re looking for a very quiet, calm home for this sensitive fluffy soul. If you`re looking for the perfect roommate Mishka is your guy!
');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (7, 'dog', 'Lucky', false, '2019-02-12', false, '2017-01-03', 'sos', true, NULL, 'Well hello there, I am the adorable Jinx! Are you looking for a running buddy? A hiking partner? Then I am your guy! I arrived at the shelter as a stray so the staff and volunteers are just learning about me, I can get pretty excited and amped up in the kennels and have not been too receptive with other dogs in my space. It`s always a good idea to take it slow and easy when introducing a new dog to resident pets. I am a high octane boy who would do best in an active household - but the good news is that I know lots of stuff and I do settle down once I`ve had some physical and mental exercise. I would do best in a home with teenagers or adults. Think we may be a great match? Come meet me today!');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (8, 'cat', 'Honney', true, '2019-01-16', false, '2017-01-03', 'healthy', true, NULL, 'Hello there! My name is Mishka I am a 2 year old neutered male who is looking for a very special home. I`m a very mellow, shy dude. I enjoy laying in the sun, climbing up high and just doing my own thing. I`m not big on snuggling, I would like to find my forever home with someone who just wants to hang out and let me chill. I`m big on climbing onto windows, cabinets and counters....I will need a tall cat tree or something similar to let me enjoy the heights. I love my treats and I didn`t mind my pomeranian dog roommate at my foster home and I`d probably get along with other dogs or cats as long as they aren`t chasing me or too noisy. 
We`re looking for a very quiet, calm home for this sensitive fluffy soul. If you`re looking for the perfect roommate Mishka is your guy!
');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (9, 'dog', 'Juckhead', true, '2019-01-22', true, '2017-01-03', 'sos', true, NULL, 'Well hello there, I am the adorable Jinx! Are you looking for a running buddy? A hiking partner? Then I am your guy! I arrived at the shelter as a stray so the staff and volunteers are just learning about me, I can get pretty excited and amped up in the kennels and have not been too receptive with other dogs in my space. It`s always a good idea to take it slow and easy when introducing a new dog to resident pets. I am a high octane boy who would do best in an active household - but the good news is that I know lots of stuff and I do settle down once I`ve had some physical and mental exercise. I would do best in a home with teenagers or adults. Think we may be a great match? Come meet me today!');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (10, 'cat', 'Jackie', false, '2018-11-27', true, '2017-01-03', 'average', false, NULL, 'I have very big brain');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (11, 'dog', 'Linda', true, '2019-03-14', true, '2017-01-03', 'healthy', true, NULL, 'Well hello there, I am the adorable Jinx! Are you looking for a running buddy? A hiking partner? Then I am your guy! I arrived at the shelter as a stray so the staff and volunteers are just learning about me, I can get pretty excited and amped up in the kennels and have not been too receptive with other dogs in my space. It`s always a good idea to take it slow and easy when introducing a new dog to resident pets. I am a high octane boy who would do best in an active household - but the good news is that I know lots of stuff and I do settle down once I`ve had some physical and mental exercise. I would do best in a home with teenagers or adults. Think we may be a great match? Come meet me today!');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (12, 'cat', 'Merloc', false, '2018-09-18', false, '2017-01-03', 'healthy', true, NULL, 'Hello there! My name is Mishka I am a 2 year old neutered male who is looking for a very special home. I`m a very mellow, shy dude. I enjoy laying in the sun, climbing up high and just doing my own thing. I`m not big on snuggling, I would like to find my forever home with someone who just wants to hang out and let me chill. I`m big on climbing onto windows, cabinets and counters....I will need a tall cat tree or something similar to let me enjoy the heights. I love my treats and I didn`t mind my pomeranian dog roommate at my foster home and I`d probably get along with other dogs or cats as long as they aren`t chasing me or too noisy. 
We`re looking for a very quiet, calm home for this sensitive fluffy soul. If you`re looking for the perfect roommate Mishka is your guy!
');
INSERT INTO shelter.animals (id, kind, name, is_boy, arrival_date, is_sterilized, birth_date, health_status, is_adopted, user_id, about) VALUES (13, 'dog', 'Kenn', true, '2019-01-14', true, '2017-01-03', 'average', false, NULL, 'Well hello there, I am the adorable Jinx! Are you looking for a running buddy? A hiking partner? Then I am your guy! I arrived at the shelter as a stray so the staff and volunteers are just learning about me, I can get pretty excited and amped up in the kennels and have not been too receptive with other dogs in my space. It`s always a good idea to take it slow and easy when introducing a new dog to resident pets. I am a high octane boy who would do best in an active household - but the good news is that I know lots of stuff and I do settle down once I`ve had some physical and mental exercise. I would do best in a home with teenagers or adults. Think we may be a great match? Come meet me today!');


--
-- TOC entry 2931 (class 0 OID 16881)
-- Dependencies: 204
-- Data for Name: donates; Type: TABLE DATA; Schema: shelter; Owner: postgres
--
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (1, '2019-03-22', 1337, NULL, 'volodymyr@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (3, '2019-03-25', 3, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (4, '2019-03-25', 1244, NULL, 'volodymyr@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (5, '2019-03-17', 1, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (6, '2019-02-11', 10000, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (7, '2019-03-29', 0.0500000000000000028, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (8, '2019-03-29', 3, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (9, '2019-03-29', 3, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (10, '2019-03-29', 0.400000000000000022, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (11, '2019-03-29', 0.0700000000000000067, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (12, '2019-03-29', 0.0700000000000000067, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (13, '2019-03-29', 0.0700000000000000067, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (14, '2019-03-29', 0.200000000000000011, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (15, '2019-03-29', 0.569999999999999951, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (16, '2019-03-29', 0.0100000000000000002, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (17, '2019-03-29', 0.400000000000000022, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (18, '2019-03-29', 0.0100000000000000002, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (19, '2019-03-29', 0.299999999999999989, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (20, '2019-03-29', 3, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (21, '2019-03-29', 0.5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (22, '2019-03-29', 4, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (23, '2019-03-29', 0.800000000000000044, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (24, '2019-03-29', 0.699999999999999956, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (25, '2019-03-29', 0.299999999999999989, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (26, '2019-03-29', 6.5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (27, '2019-03-30', 0.100000000000000006, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (28, '2019-03-30', 0.5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (29, '2019-03-30', 1, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (30, '2019-03-30', 4, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (31, '2019-03-30', 0.5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (32, '2019-03-30', 7, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (33, '2019-03-30', 1, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (34, '2019-03-30', 0.5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (35, '2019-03-30', 0.5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (36, '2019-04-01', 5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (37, '2019-04-01', 4, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (38, '2019-04-01', 4, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (39, '2019-04-01', 4.5, NULL, NULL);
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (40, '2019-04-01', 3, NULL, 'asdasf');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (41, '2019-04-01', 1, NULL, 'wsagaghadsh');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (42, '2019-04-01', 6, NULL, 'uyitkj@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (43, '2019-04-02', 7, NULL, 'mpoulos111@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (44, '2019-04-02', 6, NULL, 'null');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (45, '2019-04-02', 5, NULL, 'max1998@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (46, '2019-04-02', 4, NULL, 'mpoudsaff@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (47, '2019-04-02', 3, NULL, 'null');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (48, '2019-04-02', 10, NULL, 'sagasg@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (49, '2019-04-02', 6, NULL, 'mpoulos111@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (50, '2019-04-02', 7, NULL, 'max1998@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (51, '2019-04-02', 3, NULL, '232@gmail.cpm');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (52, '2019-04-03', 4, NULL, 'null');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (53, '2019-04-03', 552, NULL, 'null');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (54, '2019-04-03', 2000, NULL, 'null');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (55, '2019-04-03', 3, NULL, 'null');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (56, '2019-04-03', 4, NULL, 'null');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (57, '2019-04-03', 1, NULL, 'max1998@gmail.com');
INSERT INTO shelter.donates (id, date, cash_amount, user_id, email) VALUES (58, '2019-04-03', 124, NULL, 'oajwprfajwpojopojwfojojwfoj@ofjw.oiwen');


--
-- TOC entry 2938 (class 0 OID 16993)
-- Dependencies: 208
-- Data for Name: expenses; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (2, '2019-04-18', 234, 'Medicine');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (3, '2019-04-30', 8754, 'Rent');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (4, '2019-04-15', 432, 'Food');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (5, '2019-04-12', 4444, 'Workers');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (6, '2010-03-04', 2168, 'Food');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (7, '2018-09-04', 3325, 'Rent');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (8, '2019-03-11', 221, 'Medicine');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (9, '2019-03-25', 33, 'Food');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (10, '2019-03-01', 111, 'Medicine');
INSERT INTO shelter.expenses (id, date, cash_amount, type) VALUES (11, '2019-03-02', 123344, 'Rent');


--
-- TOC entry 2935 (class 0 OID 16894)
-- Dependencies: 208
-- Data for Name: reports; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.reports (id, title, text, creation_date) VALUES (67, 'Звіт №23', 'віфваіфа', '2019-03-02');
INSERT INTO shelter.reports (id, title, text, creation_date) VALUES (71, 'Звіт №2', '', '2019-03-07');
INSERT INTO shelter.reports (id, title, text, creation_date) VALUES (72, 'Звіт №23', '', '2019-03-07');
INSERT INTO shelter.reports (id, title, text, creation_date) VALUES (73, 'Звіт №23', '', '2019-03-07');


--
-- TOC entry 2937 (class 0 OID 16902)
-- Dependencies: 210
-- Data for Name: role_requests; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.role_requests (role, is_approved, user_id, id) VALUES ('user', false, 21, 9);
INSERT INTO shelter.role_requests (role, is_approved, user_id, id) VALUES ('user', false, 21, 10);
INSERT INTO shelter.role_requests (role, is_approved, user_id, id) VALUES ('user', false, 21, 11);
INSERT INTO shelter.role_requests (role, is_approved, user_id, id) VALUES ('user', false, 21, 12);


--
-- TOC entry 2929 (class 0 OID 16873)
-- Dependencies: 202
-- Data for Name: shelter_contacts; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.shelter_contacts (id, shelter_phone, shelter_email, shelter_address, shelter_map_link) VALUES (1, '+38095567894', 'catDogShelter@gmail.com', 'Theatralna str., 3, Chernivtsi, Ukraine, 58002', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2654.5409144135633!2d25.930304515206522!3d48.29244734834145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4734089965131759%3A0xbb45eac6058a9181!2z0KLQtdCw0YLRgNCw0LvRjNC90LDRjyDQv9C7LiwgMywg0KfQtdGA0L3QvtCy0YbRiywg0KfQtdGA0L3QvtCy0LjRhtC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNTgwMDA!5e0!3m2!1sru!2sua!4v1544807157392');


--
-- TOC entry 2939 (class 0 OID 16907)
-- Dependencies: 212
-- Data for Name: sos; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.sos (id, description) VALUES (1, 'Pet food and treats– A lot of the shelter’s money goes towards buying food. Think about how many animals there are to feed there. You can really help them out by bringing in a few cans or bag of food. It goes a long way. Search online for coupons so you can get a good deal.');
INSERT INTO shelter.sos (id, description) VALUES (2, ' Towels and blankets– Shelters are often cold and animals like to have a blanket to curl up on. Towels are a big help to dry animals off after being bathed or if they come in wet. Towels can also be used to line the bottoms of cages. The towels or blankets don’t have to be brand new or in perfect condition. The animals won’t mind., as long as they’re usable.');
INSERT INTO shelter.sos (id, description) VALUES (3, 'Kitty litter and cat boxes– Cats go to the bathroom- a lot. Shelters are constantly using bag after bag of litter. Their supply runs out fast.');
INSERT INTO shelter.sos (id, description) VALUES (8, 'Hand wash and hand sanitizer– People who work at shelters need to keep their hands clean for their and the animals’ health.');
INSERT INTO shelter.sos (id, description) VALUES (4, 'Puppy or kitten formula and nursing bottles– Sometimes there are situations where a young puppy or kitten who is not weaned gets separated from their mother. In these situations they need puppy or kitten formula to survive.');
INSERT INTO shelter.sos (id, description) VALUES (5, 'Old newspaper– When you’re done with your newspapers you usually just throw them away or recycle them, right? You could help animals at no cost to you if you just save up your old newspapers. Newspapers are used in the bottoms of cages. They get soiled quickly, so they’re in constant demand.');
INSERT INTO shelter.sos (id, description) VALUES (6, 'Collars, harnesses, and leashes– Dogs who are taken out on walks need a leash and collar or harness. The shelter loses some because adopted dogs often go home with their leash or harness.');
INSERT INTO shelter.sos (id, description) VALUES (7, 'Grooming supplies– Grooming supplies can include shampoo, brushes, combs, haircutting scissors, etc.  Dogs and cats often come in dirty or end up getting dirty. Grooming supplies can keep them fresh, clean, and adoptable.');
INSERT INTO shelter.sos (id, description) VALUES (9, 'Laundry detergent, fabric softener, and bleach– Towels and blankets get soiled often so the washing machines are being used a lot.');
INSERT INTO shelter.sos (id, description) VALUES (10, 'Dog and cat beds and dog houses– this can offer the animals a soft place to lay instead of a kennel or cage floor.');
INSERT INTO shelter.sos (id, description) VALUES (11, 'Flea and tick treatment– A lot of animals go through the shelters and they often have fleas or ticks.');
INSERT INTO shelter.sos (id, description) VALUES (12, 'Heating pads– Many animals come in cold or are young and need warmth. Heating pads can replace a mother’s warmth.');


--
-- TOC entry 2941 (class 0 OID 16915)
-- Dependencies: 214
-- Data for Name: users; Type: TABLE DATA; Schema: shelter; Owner: postgres
--

INSERT INTO shelter.users (id, username, fullname, email, password, image, role, phone, is_confirmed) VALUES (21, 'admin', 'Admin Admin', 'admin@admin.com', '$2a$10$0OxdIy.1msMmMtT1bEtUd.9SzQUNFMihvTXvgnipkVZrx3X9/q.ya', '', 'admin', '', true);


--
-- TOC entry 2958 (class 0 OID 0)
-- Dependencies: 199
-- Name: animal_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.animal_id_seq', 30, true);


--
-- TOC entry 2959 (class 0 OID 0)
-- Dependencies: 201
-- Name: animal_image_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.animal_image_id_seq', 30, true);


--
-- TOC entry 2960 (class 0 OID 0)
-- Dependencies: 203
-- Name: contacts_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.contacts_id_seq', 30, true);


--
-- TOC entry 2961 (class 0 OID 0)
-- Dependencies: 205
-- Name: donate_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.donate_id_seq', 200, true);


--
-- TOC entry 2962 (class 0 OID 0)
-- Dependencies: 207
-- Name: expanses_expanses_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.expanses_expanses_id_seq', 30, true);


--
-- TOC entry 2963 (class 0 OID 0)
-- Dependencies: 209
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.report_id_seq', 150, true);


--
-- TOC entry 2964 (class 0 OID 0)
-- Dependencies: 211
-- Name: role_requests_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.role_requests_id_seq', 20, true);


--
-- TOC entry 2965 (class 0 OID 0)
-- Dependencies: 213
-- Name: sos_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.sos_id_seq', 40, true);


--
-- TOC entry 2966 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: shelter; Owner: postgres
--

SELECT pg_catalog.setval('shelter.user_id_seq', 60, true);


--
-- TOC entry 2773 (class 2606 OID 16934)
-- Name: animal_images animal_image_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.animal_images
    ADD CONSTRAINT animal_image_pk PRIMARY KEY (id);


--
-- TOC entry 2769 (class 2606 OID 16936)
-- Name: animals animal_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.animals
    ADD CONSTRAINT animal_pk PRIMARY KEY (id);


--
-- TOC entry 2777 (class 2606 OID 16938)
-- Name: shelter_contacts contacts_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.shelter_contacts
    ADD CONSTRAINT contacts_pk PRIMARY KEY (id);


--
-- TOC entry 2780 (class 2606 OID 16940)
-- Name: donates donate_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.donates
    ADD CONSTRAINT donate_pk PRIMARY KEY (id);


--
-- TOC entry 2784 (class 2606 OID 16942)
-- Name: expenses expanses_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.expenses
    ADD CONSTRAINT expanses_pk PRIMARY KEY (id);


--
-- TOC entry 2787 (class 2606 OID 16944)
-- Name: reports report_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.reports
    ADD CONSTRAINT report_pk PRIMARY KEY (id);


--
-- TOC entry 2791 (class 2606 OID 16946)
-- Name: role_requests role_requests_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.role_requests
    ADD CONSTRAINT role_requests_pk PRIMARY KEY (id);


--
-- TOC entry 2794 (class 2606 OID 16948)
-- Name: sos sos_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.sos
    ADD CONSTRAINT sos_pk PRIMARY KEY (id);


--
-- TOC entry 2797 (class 2606 OID 16950)
-- Name: users user_pk; Type: CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.users
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- TOC entry 2767 (class 1259 OID 16951)
-- Name: animal_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX animal_id_uindex ON shelter.animals USING btree (id);


--
-- TOC entry 2771 (class 1259 OID 16952)
-- Name: animal_image_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX animal_image_id_uindex ON shelter.animal_images USING btree (id);


--
-- TOC entry 2775 (class 1259 OID 16953)
-- Name: contacts_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX contacts_id_uindex ON shelter.shelter_contacts USING btree (id);


--
-- TOC entry 2778 (class 1259 OID 16954)
-- Name: donate_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX donate_id_uindex ON shelter.donates USING btree (id);


--
-- TOC entry 2782 (class 1259 OID 16955)
-- Name: expanses_expanses_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX expanses_expanses_id_uindex ON shelter.expenses USING btree (id);


--
-- TOC entry 2774 (class 1259 OID 16956)
-- Name: fki_animal_image_animal_fk; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE INDEX fki_animal_image_animal_fk ON shelter.animal_images USING btree (animal_id);


--
-- TOC entry 2770 (class 1259 OID 16957)
-- Name: fki_animal_user_fk; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE INDEX fki_animal_user_fk ON shelter.animals USING btree (user_id);


--
-- TOC entry 2781 (class 1259 OID 16958)
-- Name: fki_donates_user_fk; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE INDEX fki_donates_user_fk ON shelter.donates USING btree (user_id);


--
-- TOC entry 2788 (class 1259 OID 16959)
-- Name: fki_role_users_fk; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE INDEX fki_role_users_fk ON shelter.role_requests USING btree (user_id);


--
-- TOC entry 2785 (class 1259 OID 16960)
-- Name: report_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX report_id_uindex ON shelter.reports USING btree (id);


--
-- TOC entry 2789 (class 1259 OID 16961)
-- Name: role_requests_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX role_requests_id_uindex ON shelter.role_requests USING btree (id);


--
-- TOC entry 2792 (class 1259 OID 16962)
-- Name: sos_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX sos_id_uindex ON shelter.sos USING btree (id);


--
-- TOC entry 2795 (class 1259 OID 16963)
-- Name: user_id_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX user_id_uindex ON shelter.users USING btree (id);


--
-- TOC entry 2798 (class 1259 OID 16964)
-- Name: users_user_email_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX users_user_email_uindex ON shelter.users USING btree (email);


--
-- TOC entry 2799 (class 1259 OID 16965)
-- Name: users_username_uindex; Type: INDEX; Schema: shelter; Owner: postgres
--

CREATE UNIQUE INDEX users_username_uindex ON shelter.users USING btree (username);


--
-- TOC entry 2801 (class 2606 OID 16966)
-- Name: animal_images animal_image_animal_fk; Type: FK CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.animal_images
    ADD CONSTRAINT animal_image_animal_fk FOREIGN KEY (animal_id) REFERENCES shelter.animals(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2800 (class 2606 OID 16971)
-- Name: animals animal_user_fk; Type: FK CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.animals
    ADD CONSTRAINT animal_user_fk FOREIGN KEY (user_id) REFERENCES shelter.users(id) ON DELETE SET NULL;


--
-- TOC entry 2802 (class 2606 OID 16976)
-- Name: donates donates_user_fk; Type: FK CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.donates
    ADD CONSTRAINT donates_user_fk FOREIGN KEY (user_id) REFERENCES shelter.users(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 2803 (class 2606 OID 16981)
-- Name: role_requests role_users_fk; Type: FK CONSTRAINT; Schema: shelter; Owner: postgres
--

ALTER TABLE ONLY shelter.role_requests
    ADD CONSTRAINT role_users_fk FOREIGN KEY (user_id) REFERENCES shelter.users(id) ON DELETE CASCADE;


-- Completed on 2019-04-24 15:45:33

--
-- PostgreSQL database dump complete
--


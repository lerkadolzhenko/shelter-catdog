const express = require('express');
const router = express.Router();
const User = require('../models/user');
const returner = require("../mail_forms/Confirmation_of_reg")
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const multer = require('multer');
const fs = require('fs');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/users_avatars')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.jpg')
    }
});

let upload = multer({ storage: storage }).single('avatar')

const transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    auth: {
        user: 'shelter.cv@gmail.com',
        pass: 'ololoshka378'
    }
}));
const passport = require('passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const CONSTANTS = require('../constants')

router.post('/upload', function (req, res) {
    upload(req, res, function (err) {
        if (err) {
        }
        let image = req.file;
        res.json({
            success: true,
            message: image.filename
        });
    })
});

router.put('/', function (req, res) {
    User.update({
        image: req.body.imageName
    },{
        where: {
            id: req.body.id
        }
    }).then(resolve => {
        res.sendStatus(200);
    })
});

router.get('/upload/:image', function (req, res) {
        res.sendFile(CONSTANTS.USER_AVATAR_URL + req.params.image)
});

router.delete('/image', function (req, res) {
    let img = `${CONSTANTS.USER_AVATAR_URL}` + req.query.fileToDelete;
    fs.unlink(img, err => {
        if (err) throw err;
        console.log('deleted file: ' + img)
    })
});

router.post(CONSTANTS.REGISTER_URL, (req, res) => {
    let userData = req.body;
    User.findOne({
        where: {
            email: userData.email
        }
    }).then(user => {
        if (user) {
            return res.status(400).json('Email already exists');
        }
        User.findOne({
            where: {
                username: userData.username
            }
        }).then(user => {
            if (user) {
                return res.status(400).json('Username already exists')
            }
            else {
                bcrypt.genSalt(10, (err, salt) => {
                    if (err) console.log('Error:', err);
                    else {
                        if (!(userData.password.length >= 6 && userData.password.length <= 32)) {
                            return res.status(400).json(
                                'Password is too short!'
                            )
                        }
                        bcrypt.hash(userData.password, salt, (err, hash) => {
                            if (err) console.log('Error:', err);
                            else {
                                User.create({
                                    username: userData.username,
                                    fullname: userData.fullName,
                                    email: userData.email,
                                    password: hash,
                                    image: userData.avatar,
                                    role: userData.role,
                                    phone: userData.phone,
                                })
                                    .then(function (rowCreated) {
                                        jwt.sign({ user: { id: rowCreated.dataValues.id } }, "secret", { expiresIn: '1d' }, (err, token) => {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                const url = CONSTANTS.SERVER_URL + CONSTANTS.USER_AUTH_URL + CONSTANTS.CONFIRMATION_URL + token;
                                                transporter.sendMail({
                                                    to: userData.email,
                                                    subject: "Email Confirmation",
                                                    html: returner.returner(url)
                                                }, (err, info) => {
                                                    if (err) {
                                                        console.log(err);
                                                    }
                                                })
                                            }
                                        })
                                        res.json('Registered!');
                                    }, function (err) {
                                        console.log(err);
                                    })
                                    .catch(err => {
                                        console.log('ERROR');
                                        console.log(err)
                                    })
                            }
                        })
                    }
                })
            }
        })
    })
});

router.get('/login', (req, res) => {
    User.findOne({
        where: {
            email: req.query.email
        }
    })
        .then(user => {
            if (!user) {
                return res.status(400).json('Email or password is incorrect!');
            }
            bcrypt.compare(req.query.password, user.password).then(resp => {
                if (!resp) {
                    return res.status(400).json('Email or password is incorrect!');
                }
                if (!user.is_confirmed) {
                    return res.status(400).json('Confirm your email address via link that We sent to Your Email box!');
                }
                const payload = {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    role: user.role,
                    avatar: user.image
                }
                jwt.sign(payload, 'secret', {
                    expiresIn: 3600
                }, (err, token) => {
                    if (err) console.log(err);
                    else {
                        return res.json({
                            success: true,
                            token: `Bearer ${token}`
                        })
                    }
                })
            })
        }

        )
        .catch(err => {
            console.log(err);
        });

});

router.get('/me', passport.authenticate('jwt', { session: false }), (req, res) => {
    let user = req.user;
    return res.json({
        id: user.id,
        username: user.username,
        email: user.email,
        role: user.role,
        avatar: user.image
    });
});

router.get('/' + CONSTANTS.CONFIRMATION_URL + ':token', async (req, res) => {
    try {
        const decoded = jwt.verify(req.params.token, 'secret');
        User.findOne({
            where: {
                id: decoded.user.id
            }
        })
            .then(user => {
                if (user.is_confirmed) {
                    const response = `response=${CONSTANTS.ALREADY_CONFIRMED_MESSAGE}`;
                    const url = encodeURI(`${CONSTANTS.SITE_URL}/?${response}`);
                    return res.redirect(url);
                } else {
                    User.update({
                        is_confirmed: true
                    }, {
                            where: {
                                id: decoded.user.id
                            },
                            returning: true
                        }).then(resp => {
                            const response = `response=${CONSTANTS.CONFIRMATION_MESSAGE}`;
                            const url = encodeURI(`${CONSTANTS.SITE_URL}/?${response}`);
                            return res.redirect(url);
                        }).catch(err => {
                            console.log(err);
                            return res.status(400).json(err);
                        })
                }
            })
    }
    catch (e) {
        res.send(e);
    }
});

module.exports = router;


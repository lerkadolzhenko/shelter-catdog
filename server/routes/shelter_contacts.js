const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Shelter_Contacts = require('../models/shelter_contacts');

router.put('/', (req, res) =>
    Shelter_Contacts.update({
        shelter_phone: req.body.shelter_phone,
        shelter_email: req.body.shelter_email,
        shelter_address: req.body.shelter_address,
        shelter_map_link: req.body.shelter_map_link
    }, {
        where: {
            id: req.query.id
        }
    })
    .then(function (rowCreated) {
        if (rowCreated === 1) {
            console.log('Created successfully');
        }
    }, function (err) {
        console.log(err);
    })
    .catch(err => console.log(err)));

    router.get('/', (req, res) =>
    Shelter_Contacts.findAll()
    .then(info => {
        res.json(info)
    })
    .catch(err => console.log(err)));


    module.exports = router;
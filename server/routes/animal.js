const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Animal = require('../models/animal');
const User = require('../models/user');

router.get('/', (req, res) =>
    Animal.findAll()
    .then(animals => {
        res.json(animals)
        // res.sendStatus(200);
    })
    .catch(err => console.log(err)));

    router.get('/:id', (req, res) =>
    Animal.findAll({
        where: {
            id: req.params.id
        }
    })
    .then(animals => {
        res.json(animals)
        // res.sendStatus(200);
    })
    .catch(err => console.log(err)));

router.delete('/', (req, res) => {
    Animal.destroy({
            where: {
                id: req.query.id
            }
        })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.post('/', (req, res) => {
    console.log(req.body);
    Animal.create({
            kind: req.body.kind,
            name: req.body.name,
            is_boy: req.body.is_boy,
            arrival_date: req.body.arrival_date,
            is_sterilized: req.body.is_sterilized,
            birth_date: req.body.birth_date,
            health_status: req.body.health_status,
            is_adopted: req.body.is_adopted,
            user_id: req.body.user_id,
            about: req.body.about
        })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log('ERROR');
            console.log(err)
        })
});

router.put('/', (req, res) =>
	Animal.update(
		{
            kind: req.body.kind,
            name: req.body.name,
            is_boy: req.body.is_boy,
            arrival_date: req.body.arrival_date,
            is_sterilized: req.body.is_sterilized,
            birth_date: req.body.birth_date,
            health_status: req.body.health_status,
            is_adopted: req.body.is_adopted,
            user_id: req.body.user_id,
            about: req.body.about
		},
		{
		where: {id: req.query.id}
		})
		.then(function (rowCreated) {
			if (rowCreated === 1) {
				console.log('Created successfully');
			}
		}, function (err) {
			console.log(err);
		})

        .catch(err => console.log(err)));


module.exports = router;
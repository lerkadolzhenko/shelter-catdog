const express = require('express');
const router = express.Router();
const sequelize = require('../config/database');
const Animal_Image = require('../models/animal_image');
const Animal = require('../models/animal');
const multer = require('multer');
const fs = require('fs');
const Consts = require('../constants');
let image = null;

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/animals/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.jpg')
    }
});

let upload = multer({ storage: storage }).single('animal');

router.post('/upload', function (req, res) {
    upload(req, res, function (err) {
        if (err) {
        }
        image = req.file.filename;
        res.json({
            success: true,
            message: req.file.filename,

        });
    })
});

// router.delete('/image', function (req, res) {
//     let img = 'C:/Users/User/CurrentProject/ch081ui/server/public/uploads/animals/' + req.query.fileToDelete;
//     let file = fs.createReadStream(img);
//     file.on('end', function() {
//         fs.unlink(img, err => {
//             if (err) throw err;
//             console.log('deleted file: ' + img)
//         })
//     });
//     file.pipe(res);
// });

router.delete('/image', function (req, res) {
    res.sendStatus(200);
    let img = 'C:/Users/User/CurrentProject/ch081ui/server/public/uploads/animals/' + req.query.fileToDelete;

    fs.unlink(img, err => {
        if (err) throw err;
        console.log('deleted file: ' + img)
    });
    res.end()
});

router.get('/image', function(req, res) {

    image = req.query.link;
    fs.readFile(`${Consts.ANIMAL_IMAGE_URL}${image}`, function (err, data) {
        if (err) throw err;
        res.write(data);
        res.end();

      })
     ;


});

router.delete('/image', function (req, res) {
    let img = `${Consts.ANIMAL_IMAGE_URL}` + req.query.fileToDelete;
    fs.unlink(img, err => {
        if (err) throw err;
        console.log('deleted file: ' + img)
    })
});

router.get('/', (req, res) =>
    Animal_Image.findAll({
            include: {
            model: Animal,
            where: {id: req.query.id}
        }
    },)
    .then(animal_images => {
        let links = [], names = [];
        animal_images.forEach(el => {
            links.push(el.link);
            names.push(el.name)
        });
        const { animal } = animal_images[0];
        res.json({links, names, animal})
        console.log(animal_images);
        // res.sendStatus(200);
    })
    .catch(err => console.log(err)));

    router.get('/animals', (req, res) => {
        let {amount} = req.query;
        amount = (amount)? amount : Number.MAX_SAFE_INTEGER;
        
        Animal_Image.findAll({
            attributes: [sequelize.literal('DISTINCT ON("animal_id") "animal_images"."animal_id"'), 'link', ['name', 'alt']],
            include: {
                model: Animal,
                attributes: [sequelize.literal(`animal.kind as kind, animal.name as name,
                                                animal.is_boy as is_boy, animal.arrival_date as arrival_date,
                                                animal.birth_date as birth_date, animal.is_sterilized as is_sterilized,
                                                animal.kind as kind, animal.kind as kind, animal.health_status as health_status,
                                                animal.is_adopted as is_adopted, animal.about as about`)]
            },
            order: [["animal_id" , 'DESC']],
            limit: amount,
            raw: true,
        })
            .then(animal_images => {
                res.json(animal_images)
            })
            .catch(err => console.log(err));
    });


router.delete('/', (req, res) => {
    Animal_Image.destroy({
            where: {
                id: req.query.id
            }
        })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.delete('/animals', (req, res) => {
    Animal.destroy({
        where: {
            id: req.query.id
        }
    })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                res.json();
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err));
    Animal_Image.destroy({
        where: {
            animal_id: req.query.id
        }
    })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                res.json();
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.put('/animals', (req, res) => {
    Animal.update({
        kind: req.body.kind,
        name: req.body.name,
        is_boy: req.body.is_boy,
        arrival_date: req.body.arrival_date,
        is_sterilized: req.body.is_sterilized,
        birth_date: req.body.birth_date,
        health_status: req.body.health_status,
        is_adopted: req.body.is_adopted,
        about: req.body.about
    }, {
        where: {
            id: req.body.animal_id
        }
    })
        .then(animal=> {
            res.json(animal)
            Animal_Image.update({
               link: req.body.link,
            }, {
                where: {
                    name: req.body.alt
                }
            })
            .then(animal_images => {
                res.json(animal_images)})
            .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});


router.post('/animals', (req, res) => {
    Animal.create({
        kind: req.body.kind,
        name: req.body.name,
        is_boy: req.body.is_boy,
        arrival_date: req.body.arrival_date,
        is_sterilized: req.body.is_sterilized,
        birth_date: req.body.birth_date,
        health_status: req.body.health_status,
        is_adopted: req.body.is_adopted,
        user_id: req.body.user_id,
        about: req.body.about
    })
        .then(animal=> {
            res.json(animal);
            const {id} = animal;

            Animal_Image.create({
                link: req.body.link,
                name: req.body.alt_name,
                animal_id: id
            }
            )
                .then(animal_images => {
                    res.json(animal_images)})
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});


router.post('/', (req, res) => {
    console.log(req.body);
    Animal_Image.create({
            name: req.body.name,
            link: req.body.link,
            animal_id: req.body.animal_id,
        })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log('ERROR');
            console.log(err)
        })
});

router.put('/', (req, res) =>
	Animal_Image.update(
		{
            name: req.body.name,
            link: image,
            animal_id: req.body.animal_id
		},
		{
		where: {id: req.query.id}
		})
		.then(function (rowCreated) {
			if (rowCreated === 1) {
				console.log('Created successfully');
			}
		}, function (err) {
			console.log(err);
		})
		.catch(err => console.log(err)));

module.exports = router;
const express = require('express');
const router = express.Router();
const sequelize = require('../config/database');
const Expenses = require('../models/expenses');
const Consts = require('../constants.js')

router.get('/', (req, res) =>
    Expenses.findAll()
        .then(expenses => {
            res.json(expenses)
            console.log(expenses);
        })
        .catch(err => console.log(err)));

router.delete('/', (req, res) => {
    Expenses.destroy({
        where: {
            id: req.query.id
        }
    })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.put('/', (req, res) => {
    console.log(req.body)
    const info = req.body;

    Expenses.update({
        date: req.body.date_of_payment,
        cash_amount: req.body.cash_amount,
        type: req.body.type
    }, {
            where: {
                id: info.id
            }
        })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
            res.sendStatus(200)
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.post('/', (req, res) => {
    console.log(req.body);
    Expenses.create({
        date: req.body.date,
        cash_amount: req.body.cash_amount,
        type: req.body.type
    })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log(err)
        })
});

router.get('/last_year', (req, res) => {
    Expenses.findAll({
        attributes: [[sequelize.literal('EXTRACT(MONTH FROM date)'), 'date'], ['type', 'type'], [sequelize.fn('sum', sequelize.col('cash_amount')), 'amount']],
        where: sequelize.literal(Consts.MAX_YEAR),
        group: ['type', sequelize.literal('EXTRACT(MONTH FROM "date")')],
        raw: true,
    })
        .then(result => {
            let output = {};
            output.result = result;
            output.name = 'Year expenses'
            let arr = [];
            arr.push(output)
            res.json(arr)

        })
})

module.exports = router;
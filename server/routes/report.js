const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Report = require('../models/report');
const multer = require('multer');
const fs = require('fs');
const Consts = require('../constants');
var path = require('path')
let report = null;

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/reports/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }

});

let upload = multer({ storage: storage }).single('report');

router.post('/upload', function (req, res) {
    upload(req, res, function (err) {
        if (err) {
        }
        report = req.file.filename;
        console.log(report);
        
        res.json({
            success: true,
            message: req.file.filename,
        });
    })
});

router.get('/download/:file(*)',(req, res) => {
    let file = req.params.file;
    let fileLocation = path.join('./public/uploads/reports',file);
    console.log(fileLocation);
    res.download(fileLocation, file); 
});

router.get('/', (req, res) =>
    Report.findAll()
    .then(reports => {
        res.json(reports)
    })
    .catch(err => console.log(err)));

router.post('/', (req, res) => {
    Report.create({
            title: req.body.title,
            text: report,
            creation_date: req.body.creation_date,
        })
        .then(function (rowCreated) {
            res.sendStatus(200);
            console.log('Created successfully');
        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log('ERROR');
            console.log(err)
        })
});

router.put('/', (req, res) => {
    Report.update({
        title: req.body.title,
        text: report,
        creation_date: req.body.creation_date,
    }, {
        where: {
            id: req.query.id
        }
    })
    .then(function () {

        res.json();
        console.log('Update successfully');

    }, function (err) {
        console.log(err);
    })
    .catch(err => console.log(err))
});

router.delete('/', (req, res) => {
    Report.destroy({
            where: {
                id: req.query.id
            }
        })
        .then(function (rowDeleted) {
            res.json();
            console.log('Deleted successfully');
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

module.exports = router;
const express = require('express');
const router = express.Router();

const sequelize = require('../config/database');
const Donate = require('../models/donate');
const User = require('../models/user');
const Consts = require('../constants.js')

let sum = 0;
let currentSum = 0;
let counter = 0;
router.get('/', (req, res) =>
    Donate.findAll()
    .then(donates => {
        res.json(donates)
        //console.log(donates);
    })
    .catch(err => console.log(err)));

router.get('/last/:limit_of_chips', (req, res) =>
    Donate.findAll({
        attributes: ['id', 'cash_amount', 'date'],
        include: {
            model: User,
            attributes: ['fullname', 'image']
        },
        order: [["date" , 'DESC']],
        limit: req.params.limit_of_chips,
    })
    .then(donates => {
        res.json(donates)
        //console.log(donates);
    })
    .catch(err => console.log(err)));

    
router.get('/top/:limit_of_chips', (req, res) =>
    Donate.findAll({
        attributes: ['id', 'cash_amount', 'date'],
        include: {
            model: User,
            attributes: ['fullname', 'image']
        },
        order: [["cash_amount" , 'DESC']],
        limit: req.params.limit_of_chips,
    })
    .then(donates => {
        res.json(donates)
        //console.log(donates);
    })
    .catch(err => console.log(err)));

Donate.addHook('afterCreate', () => {
    Donate.findAll({
        attributes: [sequelize.fn('sum', sequelize.col('cash_amount'))],
        where: sequelize.literal(`date between date_trunc('month', CURRENT_DATE) 
                                  and date_trunc('month', CURRENT_DATE) + interval '1 month - 1 day'`),
        raw: true,
    })
    .then(donates => {
        sum = donates;
        //console.log(donates);
    })
    .catch(err => console.log(err));
})

router.get('/last_month', (req, res) => {
    (counter == 0) ?
    Donate.findAll({
            attributes: [sequelize.fn('sum', sequelize.col('cash_amount'))],
            where: sequelize.literal(`date between date_trunc('month', CURRENT_DATE) 
                                      and date_trunc('month', CURRENT_DATE) + interval '1 month - 1 day'`),
            raw: true,
        })
        .then(donates => {
            currentSum = donates;
            res.json(currentSum);
            //console.log(donates);
            counter++;
        })
        .catch(err => console.log(err)):
        (sum != 0) ?
        res.json(sum) : res.json(currentSum);
    //console.log(sum);
})



router.delete('/', (req, res) => {
    Donate.destroy({
            where: {
                id: req.query.id
            }
        })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.post('/', (req, res) => {
    //console.log(req.body);
    Donate.create({
            date: req.body.date,
            cash_amount: req.body.cash_amount,
            user_id: req.body.user_id,
        })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log('ERROR');
            console.log(err)
        })
});

router.put('/', (req, res) =>
    User.update({
        date: req.body.date,
        cash_amount: req.body.cash_amount,
        user_id: req.body.user_id
    }, {
        where: {
            id: req.query.id
        }
    })
    .then(function (rowCreated) {
        if (rowCreated === 1) {
            console.log('Created successfully');
        }
    }, function (err) {
        console.log(err);
    })
    .catch(err => console.log(err)));

router.get('/users/:id', (req, res) =>
    Donate.findAll({
        include: {
            model: User,
            where: {
                id: req.params.id
            }
        }
    })
    .then(donates => {
        donates = donates.map((r) => (r.toJSON()))
        res.json(donates)
        //console.log(donates);
    })
    .catch(err => console.log(err)));


module.exports = router;
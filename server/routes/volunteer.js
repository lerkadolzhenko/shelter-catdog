const express = require('express');
const router = express.Router();
const Volunteer = require('../models/volunteer');
const multer = require('multer');
const fs = require('fs');
const Consts = require('../constants');
let image = null;

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/volunteers/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.jpg')
    }
});

let upload = multer({ storage: storage }).single('volunteer');

router.post('/upload', function (req, res) {
    upload(req, res, function (err) {
        if (err) {
        }
        image = req.file.filename;
        res.json({
            success: true,
            message: req.file.filename,

        });
    })
});

router.get('/image', function(req, res) {

    image = req.query.link;
    fs.readFile(`${Consts.VOLUNTEER_IMAGE_URL}${image}`, function (err, data) {
        if (err) throw err;
        res.write(data);
        res.end();

      });
});

router.delete('/image', function (req, res) {
    let img = `${Consts.VOLUNTEER_IMAGE_URL}` + req.query.fileToDelete;
    fs.unlink(img, err => {
        if (err) throw err;
        console.log('deleted file: ' + img)
    })
});

router.get('/', (req, res) =>
    Volunteer.findAll()
        .then(volunteers => {
            res.json(volunteers)
            console.log(volunteers);
        })
        .catch(err => console.log(err)));

router.delete('/', (req, res) => {
    Volunteer.destroy({
        where: {
            id: req.query.id
        }
    })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.put('/', (req, res) => {
    console.log(req.body)
    const info = req.body;

    Volunteer.update({
        image: image
    }, {
            where: {
                id: info.id
            }
        })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
            res.sendStatus(200)
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

router.post('/', (req, res) => {
    console.log(req.body);
    Volunteer.create({
        image: image,
    })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log(err)
        })
});

module.exports = router;

const express = require('express');
const router = express.Router();
const Role_Request = require('../models/role_request');

router.get('/', (req, res) =>
    Role_Request.findAll()
        .then(Role_Requests => {
            res.json(Role_Requests)
            console.log(Role_Requests);
        })
        .catch(err => console.log(err)));

router.post('/', (req, res) => {
    console.log(req.body);
    Role_Request.create({
        role: "user",
        is_approved: false,
        user_id: req.body.id
    })
        .then(function (rowCreated) {                res.json()

        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log('ERROR');
            console.log(err)
        })
});

router.put('/', (req, res) =>
    Role_Request.update({
        role: req.body.users_role,
        is_approved: req.body.is_approved,
        user_id: req.body.user_id
    }, {
            where: {
                id: req.query.id
            }
        })
        .then(function (rowCreated) {
            if (rowCreated === 1) {
                console.log('Created successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err)));

router.delete('/', (req, res) => {
    Role_Request.destroy({
        where: {
            id: req.query.id
        }
    })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

module.exports = router;
const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Sos = require('../models/sos');

router.get('/', (req, res) =>
    Sos.findAll()
    .then(sos => {
        res.json(sos)
        // console.log(sos);
        // res.sendStatus(200);
    })
    .catch(err => console.log(err)));

router.put('/', (req, res) => {
    console.log(req.body.description);
    console.log(req.query.id);
    Sos.update({
        description: req.body.description
    }, {
        where: {
            id: req.query.id
        }
    })
    .then(sos => res.json(sos))
    .catch(err => console.log(err))});

router.post('/', (req, res) => {
    console.log(req.body);
    Sos.create({
            description: req.body.description,
        })
        .then(resp => res.json(resp))
        .catch(err => {
            console.log('ERROR');
            console.log(err)
        })
});

router.delete('/', (req, res) => {
    Sos.destroy({
            where: {
                id: req.query.id
            }
        })
        .then(function (rowDeleted) {
            if (rowDeleted === 1) {
                res.json();
                console.log('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))
});

module.exports = router;
const express = require('express');
const router = express.Router();
const User = require('../models/user');

router.get('/', (req, res) =>
    User.findAll()
        .then(users => {
            res.json(users)
        })
        .catch(err => console.log(err)));

router.get('/:id', (req, res) =>
    User.findAll({
        where: {
            id: req.params.id
        }
    })
        .then(users => {
            res.json(users)
        })
        .catch(err => console.log(err)));

router.post('/', (req, res) => {
    let user = req.body;
    User.create({
        username: user.username,
        fullname: user.fullname,
        email: user.email,
        password: user.password,
        image_link: user.image_link,
        users_role: user.role,
        phone: user.phone,
    })
        .then(function (rowCreated) {
        }, function (err) {
            console.log(err);
        })
        .catch(err => {
            console.log('ERROR');
            console.log(err)
        })
});

router.delete('/', (req, res) => {
    var countAdmins = 0;
    var roleReq = '';

    User.findOne({
        where: {
            id: req.query.id
        }
    }).then((data) => roleReq = data.role);

    User.findAll({
        where: {
            role: 'admin'
        }
    }).then((count) => countAdmins = count.length)
        .then(() => roleReq !== 'admin' ? userDelete() : adminDelete())

    function userDelete() {
        User.destroy({
            where: {
                id: req.query.id
            }
        })
            .then(function (rowDeleted) {
                if (rowDeleted === 1) {
                    res.json();
                }
            }, function (err) {
                console.log(err);
            })
            .catch(err => console.log(err))
    }

    function adminDelete() {
        if (countAdmins > 1) {
            User.destroy({
                where: {
                    id: req.query.id
                }
            })
                .then(function (rowDeleted) {
                    if (rowDeleted === 1) {
                        res.json();
                    }
                }, function (err) {
                    console.log(err);
                })
                .catch(err => console.log(err))
        } else {
            // var test = 'false';
            res.send(false);
        }
    }
});

router.put('/', (req, res) => {
    // User.findOne({
    //     where: {
    //         username: req.body.username
    //     }
    // }).then(result => {
    //     if (result) {
    //         if(result.id !== req.body.id)
    //         return res.status(400).json("Username already exist!")
    //     }
    // })
    // User.findOne({
    //     where: {
    //         email: req.body.email
    //     }
    // }).then(result => {
    //     if (result) {
    //         if(result.id !== req.body.id)
    //         return res.status(400).json("Email already exist!")
    //     }
    // })
    console.log(req.body);

    User.update({
        username: req.body.username,
        fullname: req.body.fullName,
        email: req.body.email,
        role: req.body.role,
        phone: req.body.phone
    }, {
            where: {
                id: req.query.id
            }
        })
        .then(function () {
            res.json();

        }, function (err) {
            console.log(err);
        })
        .catch(err => console.log(err))

});

module.exports = router;

const Sequelize = require('sequelize');
const db = require('../config/database');
const User = require('../models/user');

const Donate = db.define('donates', {
  date: Sequelize.DATE,
  cash_amount: Sequelize.DOUBLE,
  user_id: {
    type: Sequelize.INTEGER,
    allowNull: true,
    references: {
      model: 'users',
      key: 'id'
    }
  },
  email: Sequelize.TEXT
}, {
  timestamps: false
});

Donate.belongsTo(User, {foreignKey: 'user_id'});

module.exports = Donate;
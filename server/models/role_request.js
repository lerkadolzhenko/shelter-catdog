const Sequelize = require('sequelize');
const db = require('../config/database');
const User = require('../models/user');

const Role_Request = db.define('role_requests', {
  role: Sequelize.TEXT,
  is_approved: Sequelize.BOOLEAN,
  user_id: Sequelize.INTEGER
}, {
  timestamps: false
});

Role_Request.belongsTo(User, {foreignKey: 'user_id'});
module.exports = Role_Request;
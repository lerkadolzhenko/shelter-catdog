const Sequelize = require('sequelize');
const db = require('../config/database');

const Report = db.define('reports', {
  title: Sequelize.TEXT,
  text: Sequelize.TEXT,
  creation_date: Sequelize.DATE
}, {
  timestamps: false
});

module.exports = Report;
const Sequelize = require('sequelize');
const db = require('../config/database');

const Volunteer = db.define('volunteers', {
  image: Sequelize.TEXT,
}, {
  timestamps: false
});

module.exports = Volunteer;
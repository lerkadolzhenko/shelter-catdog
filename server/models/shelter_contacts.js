const Sequelize = require('sequelize');
const db = require('../config/database');

const Shelter_Contacts = db.define('shelter_contacts', {
  shelter_phone: Sequelize.TEXT,
  shelter_email: Sequelize.TEXT,
  shelter_address: Sequelize.TEXT,
  shelter_map_link: Sequelize.TEXT
}, {
  timestamps: false
});

module.exports = Shelter_Contacts;
const Sequelize = require('sequelize');
const db = require('../config/database');
const User = require('../models/user');

const Animal = db.define('animals', {
  id: {
    type:Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  kind: Sequelize.TEXT,
  name: {
    type: Sequelize.TEXT,
    validate: {
      is: ["^[a-zA-ZА-Яа-яІіЇїЄє ,.'-]+$"],
      max: 15,
      min: 2
    }
  },
  is_boy: Sequelize.BOOLEAN,
  arrival_date: Sequelize.DATE,
  is_sterilized: Sequelize.BOOLEAN,
  birth_date: Sequelize.DATE,
  health_status: Sequelize.TEXT,
  is_adopted: Sequelize.BOOLEAN,
  user_id: Sequelize.INTEGER,
  about: Sequelize.TEXT,
}, {
  timestamps: false
});

Animal.belongsTo(User, {foreignKey: 'user_id'});
module.exports = Animal;
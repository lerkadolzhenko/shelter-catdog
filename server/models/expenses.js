const Sequelize = require('sequelize');
const db = require('../config/database');

const Expenses = db.define('expenses', {
  date: Sequelize.DATE,
  cash_amount: Sequelize.DOUBLE,
  type: Sequelize.TEXT
}, {
  timestamps: false
});

module.exports = Expenses;
const Sequelize = require('sequelize');
const db = require('../config/database');

const Sos = db.define('sos', {
  description: Sequelize.TEXT
}, {
  timestamps: false
});

module.exports = Sos;
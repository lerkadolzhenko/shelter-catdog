const Sequelize = require('sequelize');
const db = require('../config/database');

const User = db.define('users', {
  username: {
    type: Sequelize.TEXT,
    validate: {
      is: ["^[A-Za-z0-9_]{3,20}$"]
    }
  },
  fullname: Sequelize.TEXT,
  email: {
    type: Sequelize.TEXT,
    validate: {
      is: ["[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,3}$"],
      max: 30
    }
  },
  password: {
    type: Sequelize.TEXT,
  },
  image: Sequelize.TEXT,
  role: Sequelize.TEXT,
  phone: Sequelize.TEXT,
  is_confirmed: Sequelize.BOOLEAN
}, {
  timestamps: false
});

module.exports = User;
const Sequelize = require('sequelize');
const db = require('../config/database');
const Animal = require('../models/animal');

const Animal_Image = db.define('animal_images', {
  id: {
    type:Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: Sequelize.TEXT,
  link: Sequelize.TEXT,
  animal_id: {
    type: Sequelize.INTEGER,
    allowNull: true,
    references: {
      model: 'animals',
      key: 'id'
    }
  }
}, {
  timestamps: false
});

Animal_Image.belongsTo(Animal, {foreignKey: 'animal_id'});
module.exports = Animal_Image;
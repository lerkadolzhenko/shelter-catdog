import React, { Component } from 'react';
import "./donate.scss";
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { TextField, InputAdornment } from '@material-ui/core';
import { connect } from 'react-redux';
import { getDonationByMonth } from './actionCreators/actionCreator';
import axios from 'axios';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Collapse from '@material-ui/core/Collapse';

class SimpleCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      amountInput: '',
      email: '',
      t: 'wr',
      checkedB: false,
    }
  }


  donatesButton = (e) => {
    e.preventDefault();
    const id = this.props.user.id;
    
    let email = !this.state.checkedB ? null : this.state.email;
        axios
          .get(`http://localhost:5000/donates_url/${this.state.amountInput}/${email}/${id}`)
          .then(res => {
            this.renderRedirect(res.data.data, res.data.signature)
          })
          .catch(error => {
            console.log(error);
          });
    }

  componentDidMount() {
    this.props.dispatch(getDonationByMonth());
    axios
      .get(`http://localhost:5000/status/`);
  }
  renderRedirect = (data, signature) => {
    window.location.href = `https://www.liqpay.ua/api/3/checkout?data=${data}&signature=${signature}`;
  }

  handleUserInput = (e) => {
    const name = e.target.name
    const value = e.target.value;
    this.setState({ [name]: value });
  }

  setCheckbox = () => {
    this.setState({ checkedB: !this.state.checkedB });
  }
  componentDidUpdate(prevState) {
    const { email } = this.props.user
    
    if (email && this.state.email.length < 1) {
      this.setState({email})
    }
  }

  render() {
    const { donatesByMonth } = this.props;

    let test = (!donatesByMonth[0]) ? (
      <span className="sum"> nobody donate this month </span>)
      : (<span><span className="sum"> {donatesByMonth[0].sum} </span> UAH</span>);
    return (
        <Card className="cardDonate">
          <div className="title">
            Help homeless pets
          </div>
          <div className="donation-loader">
            <div className="donation-imgs">
              <img className="kost-1" src={require("../../assets/img/donate_fix.png")} alt="Myska" />
              <div className="donation-sum">As of present month were collected
                    {test} <br />We thank everybody who helps our shelter!
                  </div>
              <img className="kost-2" src={require("../../assets/img/donate_fix.png")} alt="Myska" />
            </div>
          </div>
          <div className="additional-info text-black">The funds, which you donate go for the purchase of feed and the treatment of homeless animals</div>
          <form onSubmit={this.donatesButton}>
            <div className="textField-donates">
              <TextField
                id="outlined-adornment-amount"
                variant="outlined"
                label="Donation"
                value={this.state.amountInput}
                onChange={this.handleUserInput}
                className="textField_1"
                required
                name="amountInput"
                placeholder="100"
                type="number"
                InputProps={{
                  startAdornment: <InputAdornment position="start">₴</InputAdornment>,
                }}
              />
              <Collapse in={this.state.checkedB && !this.props.isLogged}>
                <TextField
                  inputProps={{ pattern: "[a-z0-9._]+@[a-z0-9.-]+\\.[a-z]{2,3}$" }}
                  id="outlined-name"
                  label="Email"
                  required={this.state.checkedB}
                  style={{ marginTop: 30 }}
                  placeholder="name@example.com"
                  value={this.state.email}
                  onChange={this.handleUserInput}
                  margin="normal"
                  name="email"
                  variant="outlined"
                  className="textField_1"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Collapse>
              <FormControlLabel
                className="checkbox-margins"
                control={
                  <Checkbox
                    checked={this.state.checkedB}
                    value="checkedB"
                    onChange={this.setCheckbox}
                    color="primary"
                  />
                }
                label="Add your email"
              />
            </div>
            <div className="donation-div">
              <Button size="medium" variant="contained" type="submit"
                color="primary" className="donation-btn animation-button" target="_blank" style={{ fontSize: '25px', color: "#ffffff" }}>
                <img className="first-paw" alt='may' src={require("../../assets/img/paw_white.png")} />
                Donate
              <img className="second-paw" alt='may' src={require("../../assets/img/paw_white.png")} />
              </Button>
            </div>
          </form>
        </Card>
      );
  }
}

const mapStateToProps = (state) => {
  return {
    donatesByMonth: state.donatesByMonthListReducer.donatesByMonth,
    user: state.sign.user,
    isLogged: state.sign.isLogged
  }
}

export default connect(mapStateToProps)(SimpleCard);

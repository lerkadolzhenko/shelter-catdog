import { DONATION_BY_MONTH } from "../../../constants/action-types";

const initialState = {
    donatesByMonth: []
}

export const donatesByMonthListReducer = (state = initialState, action) => {
	switch (action.type) {

		case DONATION_BY_MONTH:
            return {...state,	donatesByMonth: action.payload};
            
		default:
			return state;
	}
}
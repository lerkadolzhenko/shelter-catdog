import axios from "axios";
import { apiUrl } from "../../../config/apiUrl";
import { DONATION_BY_MONTH } from "../../../constants/action-types";

export const getDonationByMonth = () => {
    return (dispatch) => {
        return axios
        .get(`${apiUrl}/donates/last_month`)
        .then(res => res.data)
        .then(data => dispatch({type: DONATION_BY_MONTH, payload: data}))
    }
}
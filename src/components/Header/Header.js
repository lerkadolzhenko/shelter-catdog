import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { store } from "../../store/app.store";
import PetsIcon from '@material-ui/icons/Pets';
import LocalPhone from '@material-ui/icons/LocalPhone';
import PhotoLibrary from '@material-ui/icons/PhotoLibrary';
import Help from '@material-ui/icons/Help';
import AttachMoney from '@material-ui/icons/AttachMoney';
import Info from '@material-ui/icons/Info';
import MoreIcon from '@material-ui/icons/MoreVert';
import './Header.scss';
import Sign from "../sign.component/sign";
import { Provider } from "react-redux";
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import * as actionCreators from "../sign.component/action-creator";
import { Tooltip, Button } from '@material-ui/core';
import {animateScroll as scroll} from "react-scroll";

function mapDispatchToProps(dispatch) {
    return {
        addAction: payload => dispatch(payload)
    }
}
class PrimarySearchAppBar extends React.Component {
    state = {
        anchorEl: null,
        mobileMoreAnchorEl: null,
    };

    handleProfileMenuOpen = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuClose = () => {
        this.setState({ anchorEl: null });
        this.handleMobileMenuClose();
    };

    handleMobileMenuOpen = event => {
        this.setState({ mobileMoreAnchorEl: event.currentTarget });
    };

    handleMobileMenuClose = () => {
        this.setState({ mobileMoreAnchorEl: null });
    };

    handleClickHome = () => {
        this.props.history.push("/");
    };

    componentDidMount() {
        this.props.addAction(actionCreators.isLogged(""));
    }

    strollToComponent = (el) => {
        this.props.strollToComponent(el);
    }

    scrollToTop = () => {
        scroll.scrollToTop();
    }
    render() {
        const { mobileMoreAnchorEl } = this.state;
        const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
        const { pathname } = this.props.location

        const renderMobileMenu = (pathname === '/')? (
            <Menu
                anchorEl={mobileMoreAnchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMobileMenuOpen}
                onClose={this.handleMenuClose}
            >
                <MenuItem onClick={() => this.strollToComponent('MiniGalery')}>
                    <PhotoLibrary className='icon-margin'/>
                    Gallery
                </MenuItem>
                <MenuItem onClick={() => this.strollToComponent('SosInfo')}>
                    <Help className='icon-margin' />
                    Ways to help
                </MenuItem>
                <MenuItem onClick={() => this.strollToComponent('Donate')}>
                    <AttachMoney className='icon-margin' />
                    Donate
                </MenuItem>
                <MenuItem onClick={() => this.strollToComponent('About')}>
                    <Info className='icon-margin' />
                    About us
                </MenuItem>
                <MenuItem onClick={() => this.strollToComponent('Contacts')}>
                    <LocalPhone className='icon-margin' />
                    Contacts
                </MenuItem>
            </Menu>
        ) : null;

        const navBar = (pathname === '/') ? (
            <div className='nav-bar'>
                <div className='sectionDesktop'>
                    <Button className='nav-button' onClick={() => this.strollToComponent('MiniGalery')}>
                        Gallery
                    </Button>
                    <Button className='nav-button' onClick={() => this.strollToComponent('SosInfo')}>
                        Ways to help us
                    </Button>
                    <Button className='nav-button donate-button-bg' onClick={() => this.strollToComponent('Donate')}>
                        Donate
                    </Button>
                    <Button className='nav-button' onClick={() => this.strollToComponent('About')}>
                        About us
                    </Button>
                    <Button className='nav-button' onClick={() => this.strollToComponent('Contacts')}>
                        Contacts
                    </Button>

                </div>
                <div className='sectionMobile'>
                    <IconButton color="inherit" onClick={() => this.strollToComponent('MiniGalery')}>
                        <PhotoLibrary  />
                    </IconButton>
                    <IconButton color="inherit" onClick={() => this.strollToComponent('SosInfo')}>
                        <Help  />
                    </IconButton>
                    <IconButton color="inherit" onClick={() => this.strollToComponent('Donate')}>
                        <AttachMoney  />
                    </IconButton>
                    <IconButton color="inherit" onClick={() => this.strollToComponent('About')}>
                        <Info  />
                    </IconButton>
                    <IconButton color="inherit" onClick={() => this.strollToComponent('Contacts')}>
                        <LocalPhone  />
                    </IconButton>
                </div>
            </div>
        ) : null

        return (
            <div className='compHeader'>
                <AppBar position="static">
                    <Toolbar>
                        <Tooltip title="Home">
                            <IconButton color="inherit" onClick={() => {
                                if (pathname === '/')
                                    this.scrollToTop()
                                else
                                    this.handleClickHome();
                            }}>
                                <PetsIcon />
                                <div className='header-white'>
                                    Shelter Cat&Dog
                            </div>
                            </IconButton>
                        </Tooltip>
                        <div className='grow' />
                        {navBar}
                        <Provider store={store}>
                            <Sign></Sign>
                        </Provider>
                        {pathname === '/'? 
                            (
                                <div className='sectionXs'>
                                    <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                                        <MoreIcon />
                                    </IconButton>
                                </div>
                            ) : null
                        }
                        
                    </Toolbar>
                </AppBar>
                {renderMobileMenu}
            </div>
        );
    }
}

export default withRouter(connect(null, mapDispatchToProps)(PrimarySearchAppBar));

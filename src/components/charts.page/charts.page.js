import React, { Component, Fragment } from 'react';
import {Button, Card, Collapse } from '@material-ui/core';
import ChartList from './chartList.component'
import './charts.page.scss'
import BarChart from './barChart.component';
import { pieColors, barCharts } from '../../style/charts.colors'
import axios from 'axios';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import { apiUrl } from '../../config/apiUrl'
import ModalLoader from '../share.components/loader/loader'

export default class Charts extends Component {

    state = {
        pieCharts: null,
        moreCharts: null,
        detailedInfo: false,
        buttonName: 'show more'
    }

    componentDidMount() {
        axios
            .get(apiUrl + "/charts_data/")
            .then(response => {
                const { data } = response;
                let pieCharts = []

                for (const i in data) {
                    pieCharts.push({
                        id: i,
                        data: {
                            labels: data[i].data.types,
                            datasets: [{
                                data: data[i].data.amounts,
                                backgroundColor: pieColors[i]
                            }]
                        },
                        name: data[i].name
                    })
                }

                this.setState({ pieCharts })
            })
            .catch(error => console.log(error));
    }

    detailedInfoChange = () => {
        axios
            .get(apiUrl + "/expenses/last_year/")
            .then(response => {
                const { data } = response;

                const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September",
                    "October", "November", "December"];

                let moreCharts = [];

                for (const i in data) {
                    let output = [], datasets = [];

                    data[i].result.forEach((item) => {
                        var existing = output.filter((v) => {
                            return v.type === item.type.charAt(0).toUpperCase() + item.type.substring(1);
                        });
                        if (existing.length) {
                            var existingIndex = output.indexOf(existing[0]);
                            output[existingIndex].amount = output[existingIndex].amount.concat(item.amount);
                            output[existingIndex].date = output[existingIndex].date.concat(item.date);
                        } else {
                            item.amount = [item.amount];
                            item.date = [item.date];
                            output.push(item);
                        }
                    });

                    for (const el in output) {
                        let temp = [...output[el].amount];

                        output[el].amount = new Array(output.length)
                        output[el].amount.fill(0)

                        for (const dateIte in output[el].date) {
                            output[el].amount[output[el].date[dateIte]] = temp[dateIte];
                        }

                        datasets.push({
                            label: output[el].type,
                            backgroundColor: barCharts[el],
                            borderWidth: 1,
                            data: output[el].amount
                        })
                    }

                    moreCharts.push({
                        id: i,
                        name: data[i].name,
                        data: {
                            labels: monthNames,
                            datasets: datasets
                        }
                    })
                }
                this.setState({ moreCharts })

            })
            .catch(error => console.log(error));
    };

    showMoreToggle = () => {
        let buttonName = (this.state.detailedInfo) ? 'show more' : 'show less';
        this.setState({ detailedInfo: !this.state.detailedInfo, buttonName })
        if (!this.state.moreCharts) this.detailedInfoChange()
    }

    render() {
        const { pieCharts, moreCharts, detailedInfo, buttonName } = this.state;

        const moreChartsComponent = (!moreCharts) ? null : moreCharts.map(({ data, id, name }) => {
            return (
                <Collapse key={id} in={detailedInfo}>
                    <Card className="chart-list chart-card">
                        <BarChart data={data} name={name} />
                    </Card>
                </Collapse >
            );
        })

        const chartsPage = (!pieCharts) ? (<div style={{ height: '84vh' }}><ModalLoader /></div>) : (
            <div className='charts-page'>

                <ChartList charts={pieCharts} />
                <div className='center-button'>
                    <Button variant="contained" onClick={this.showMoreToggle} className='button-blue'>{buttonName}</Button>
                </div>
                {moreChartsComponent}

            </div>
        );

        return (
            <Fragment>
                <Header />
                {chartsPage}
                <Footer />
            </Fragment>
        );
    }
}
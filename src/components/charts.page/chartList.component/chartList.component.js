import React from 'react'
import PieChart from '../pieChart.component'
import './chartList.component.scss';
import {Card, Grid, Grow} from '@material-ui/core';

const ChartList = ({ charts }) => {
    let transitionTime = 0;
    
    const elements = charts.map(({ data, name, id }) => {
        return (
            <Grow key={id} in={true}
                  style={{ transformOrigin: '0 0 0' }}
                  {...({timeout: transitionTime+=500})}>
                <Grid className="grid-box"item sm={12} md={6}>
                    <Card className="chart-card">
                        <PieChart data={data} name={name} />
                    </Card>
                </Grid>
            </Grow>
        );
    })

    return (
        <div className='chart-list'>
            <Grid  container spacing={16}>
                {elements}
            </Grid>
        </div>
    );
}

export default ChartList;
import React from 'react';
import { Pie } from 'react-chartjs-2';
import 'chartjs-plugin-deferred';
import './pieChart.component.scss';
import Typography from '@material-ui/core/Typography';
import 'chartjs-plugin-datalabels';

const PieChart = ({ name, data }) => {

    return (
        <div className='chart-block'>
            <Typography style={{marginBottom: '2vh'}} variant='display2'>{name}</Typography>
            <Pie
                data={data} options={{
                    maintainAspectRatio: true,
                    layout: {
                      padding: 32
                    },
                    plugins: {
                        deferred: { enabled: true, delay: 500 },
                        datalabels: { 
                            display: true,
                            align: 'end',
                            anchor: 'end',
                            offset: 14, 
                            fontSize: 14,
                            font: {
                              size: 14,
                            },
                            padding: 1,
                            color: '#777',
                        }
                    },
                    responsive: true,
                    legend: {
                        position: 'left',
                        labels: {
                            boxWidth: 14,
                            fontSize: 14,
                            padding: 4
                        }
                    }
                }}
            />
        </div>
    );
}

export default PieChart;
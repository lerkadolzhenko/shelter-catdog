import React from 'react'
import {Typography} from '@material-ui/core'
import { HorizontalBar  } from 'react-chartjs-2';
import './barChart.component.scss'

const BarChart = ({name, data}) => {

    return (
        <div className='bar-chart'>
            <Typography variant='display2'>{name}</Typography>
            <HorizontalBar 
                data={data } height={100} width={150}
                options={{
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true,
                        }]
                    },
                    plugins: {
                      datalabels: {
                         display: function(context) {
                            return context.dataset.data[context.dataIndex] !== 0; // or >= 1 or ...
                         },
                         color: 'white',
                         font: {
                           size: 11,
                         },
                      }
                    },
                }}
                ></HorizontalBar >
        </div>
    );
}

export default BarChart;

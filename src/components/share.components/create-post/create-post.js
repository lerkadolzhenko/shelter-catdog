import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import './create-post.scss'
import Popover from '@material-ui/core/Popover';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state/index';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';


const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    typography: {
        margin: theme.spacing.unit * 2,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});





class CreatePostAsVolunteer extends Component {

    constructor(props) {
        super(props);
        this.state = {title: '', desctiption: '', animal: ''};

        this.handleUserInput = this.handleUserInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.title === 'Sign In' ? this.props.handleSignIn(this.state) : this.props.handleSignUp(this.state);

    }
    handleUserInput(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    }

    render() {
        const { classes } = this.props;
        return (
            <PopupState variant="popover" popupId="demo-popup-popover">
                {popupState => (
                    <div>
                        <Button variant="contained" {...bindTrigger(popupState)}>
                            {this.props.title}
                        </Button>
                        <Popover
                            {...bindPopover(popupState)}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}
                        >
                            <main className={classes.main}>
                                <CssBaseline />
                                <Paper className={classes.paper}>
                                    <Typography component="h1" variant="h5">
                                        {this.props.title}
                                    </Typography>
                                    <form className={classes.form} onSubmit={this.handleSubmit}>
                                        <FormControl margin="normal" required fullWidth>
                                            <InputLabel htmlFor="title">Title</InputLabel>
                                            <Input value={this.state.title} onChange={this.handleUserInput} id="title" name="title" autoComplete="title" autoFocus />
                                        </FormControl>
                                        <FormControl margin="normal" required fullWidth>
                                            <InputLabel htmlFor="description">Description</InputLabel>
                                            <Input value={this.state.description} onChange={this.handleUserInput} id="description" name="description" description="description" autoComplete="description" />
                                        </FormControl>
                                        <FormControl required fullWidth className={classes.formControl}>
                                            <InputLabel htmlFor="animal-required">Animal</InputLabel>
                                            <Select
                                                value={this.state.animal}
                                                onChange={this.handleUserInput}
                                                name="animal"
                                                inputProps={{
                                                    id: 'animal-required',
                                                }}
                                                className={classes.selectEmpty}
                                            >
                                                <MenuItem value="">
                                                    <em>None</em>
                                                </MenuItem>
                                                <MenuItem value={10}>Ten</MenuItem>
                                                <MenuItem value={20}>Twenty</MenuItem>
                                                <MenuItem value={30}>Thirty</MenuItem>
                                            </Select>
                                        </FormControl>
                                        <Button
                                            type="submit"
                                            fullWidth
                                            variant="contained"
                                            color="primary"
                                            className={classes.submit}
                                        >
                                            {this.props.title}
                                        </Button>
                                    </form>
                                </Paper>
                            </main>
                        </Popover>
                    </div>
                )}
            </PopupState>
        );

    }
}



export default withStyles(styles)(CreatePostAsVolunteer);
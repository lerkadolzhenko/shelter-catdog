import React from 'react'
import './nothingToShow.component.scss'
import { Typography } from '@material-ui/core'

const NothingToShow = ({text = 'NOTHING TO SHOW :('}) => {
    return (
        <div className='default-block'>
            <Typography variant='display2' className='default-block-text'>{text}</Typography>
        </div>
    );
}

export default NothingToShow;
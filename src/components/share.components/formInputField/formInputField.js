import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MuiPhoneNumber from "material-ui-phone-number";
import { FormErrors } from '../formErrors.shared';




class FormInputField extends Component {
    render() {
        let elem =
            <div>
                <FormControl error={this.props.error} margin="normal" required fullWidth type={this.props.type}>
                    <InputLabel htmlFor={this.props.name}>{this.props.label}</InputLabel>
                    <Input disabled={this.props.disabled} onBlur={this.props.onFieldBlur} type={this.props.type} value={this.props.value} onChange={this.props.handleUserInput} name={this.props.name} id={this.props.id} autoComplete={this.props.autoComplete} autoFocus={this.props.autoFocus} />
                </FormControl>
                <FormErrors fieldName={this.props.label} errorValue={this.props.errorValue} />
            </div>;
        if (this.props.type === "file") {
            elem = <div>
                {/* <input
                    accept="image/*"
                    style={{ display: 'none' }}
                    id="raised-button-file"
                    multiple
                    type="file"
                />
                <label htmlFor="raised-button-file">
                    <Button variant="contained" component="span" color="default" >
                        Upload
                        <CloudUploadIcon />
                    </Button>
                </label> */}
            </div>
        }
        if (this.props.name === "phone") {
            elem = <div>
                <FormControl margin="normal" required fullWidth>
                    <MuiPhoneNumber disabled={this.props.disabled} label={this.props.label} onlyCountries={['de', 'es', 'ua', 'us', 'gb']} value={this.props.value} disableAreaCodes={true} defaultCountry={'us'} onChange={this.props.handlePhoneInput} />
                </FormControl>
            </div>;
        }

        return elem
    }

}



export default FormInputField;
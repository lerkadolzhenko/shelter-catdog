import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import '../../sign.component/sign'
import Link from '@material-ui/core/Link';
import { FormErrors } from '../formErrors.shared';
import FormInput from "../formInputField/formInputField";
import Avatar from '@material-ui/core/Avatar';


class SignIn extends Component {
    render() {
        let header = <Fragment>
            <Avatar className={this.props.classes.avatar}>
                <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                {this.props.title}
            </Typography>
        </Fragment>
        // if (!this.props.render) {
        //     header = <Fragment>
        //         <Typography component="h1" variant="h5">
        //             {this.props.title}
        //         </Typography>
        //         <input
        //             accept="image/*"
        //             style={{ display: 'none' }}
        //             id="raised-button-file"
        //             type="file"
        //             name="avatar"
        //             onChange={this.props.handleLoad}
        //         />
        //         <label htmlFor="raised-button-file">
        //             <UserAvatar errorValue={this.props.avatarErrorValue} toolTipTitle="Upload Avatar" iconName="add_a_photo" image={this.props.image} bigAvatar={this.props.classes.bigAvatar} bigAvatarPlaceHolder={this.props.classes.bigAvatarPlaceHolder} />
        //         </label>
        //     </Fragment>
        // }

        return (
            <Fragment>
                {header}
                <form className={this.props.classes.form} onSubmit={this.props.handleSubmit}>
                    {this.props.formFieldsData.map((inputField, index) => (
                        <FormInput disabled={this.props.loading} type={inputField.type} key={index} errorValue={inputField.fieldErrors} value={inputField.value} error={inputField.error} onFieldBlur={this.props.onFieldBlur} handlePhoneInput={this.props.handlePhoneInput} label={inputField.label} handleUserInput={this.props.handleUserInput} name={inputField.name} autoComplete={inputField.autoComplete} id={inputField.name} autoFocus={inputField.autoFocus}></FormInput>

                    ))}
                    <div className={this.props.classes.submit}>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            disabled={this.props.loading}
                            className="signSubmitButton"
                        >
                            {this.props.title}
                        </Button>
                    </div>
                    <div className={this.props.classes.error}>
                        <FormErrors fieldName="" errorValue={this.props.error} classN={this.props.classes.error} />
                    </div>
                    <div className={this.props.classes.typography}>
                        <Typography className={this.props.classes.typography}>
                            <Link onClick={this.props.loading ? null : this.props.toggle} className={this.props.classes.link}>
                                {this.props.linkText}
                            </Link>
                        </Typography>
                    </div>
                </form>
            </Fragment>
        )
    }

}

export default SignIn;
import * as actions from "../../../constants/action-types";

const initialState = {
    error: '',
    user: {
        email: null,
        id: null,
        username: null,
        role: null,
        avatar: null
    },
    isLogged: false,
    isRegistered: false,
    isLoading: true,
    imageName: null,
    isImageLoading: false
}


export function sign(state = initialState, action) {
    switch (action.type) {
        case actions.USER_SIGNED:
            return {...state, user: action.payload, error: '', isLogged: true}
        case actions.LOG_OUT_USER:
            return {...state, 
                    user: {
                        email: null,
                        id: null,
                        username: null
                        }, 
                    error: '', 
                    isLogged: false
                    }       
        case actions.ERASE_ERRORS:
            return {...state, error: '', imageName: null}
        case actions.ERASE_AVATAR:
            return {...state, imageName: null}
        case actions.IMAGE:
            return {...state, imageName: action.payload, isImageLoading: false}
        case actions.IS_IMAGE_LOADING:
            return {...state, isImageLoading: true}
        case actions.IS_LOADING:
            return {...state, isLoading: true}
        case actions.IS_LOGGED:
            return {...state, isLogged: true, isLoading: false, user: action.payload}
        case actions.SING_UP:
            break;
        case actions.USER_REGISTERED:
            return { ...state, error: '', isRegistered: true}
        case actions.DEFAULT_REGISTER:
            return { ...state, error: '', isRegistered: false}
        case actions.SIGN_ERROR:
            return { ...state, error: action.payload }
        case actions.NOT_SIGNED_UP:
            return {...state, isLoading: false};   
        case actions.PREVENT_NON_VALID_FORM:        
            return { ...state, error: action.payload }
        default:
            return state;
    }

}
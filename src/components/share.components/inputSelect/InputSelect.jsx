import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";


export class InputSelect extends Component {

	render() {
		const {label, value, items, name, handleChange, className, fullWidth} = this.props;
		let menuItems = <div/>;
		if (items !== undefined) {
			menuItems = items.map((item, index) => (
				<MenuItem key={index} value={item}>
					{item}
				</MenuItem>)
			)
		}

		return (
			<TextField
				className={className}
				select
				label={label}
				value={value}
				onChange={handleChange}
				name={name}
				margin="normal"
                variant="outlined"
                fullWidth={fullWidth}
			>
				{menuItems}
			</TextField>
		);
	}
}

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from "react-redux";
import { snackbarToggle } from "./actionCreators/actionCreators";

const styles = theme => ({
	close: {
		padding: theme.spacing.unit / 2,
	},
});

class SnackbarComponent extends React.Component {
	state = {
		open: false,
		message: ''
	};

	componentDidMount() {
		if (this.props.snackbarToggleStatus !== this.state.open) {
			this.setState({
				open: this.props.snackbarToggleStatus,
				message: this.props.message
			})
		}
	}

	handleClose = (event, reason) => {
		if (reason === 'clickaway') {
			return;
		}
		this.props.dispatch(snackbarToggle())
	};

	render() {
        const { classes } = this.props;        
		return (
			<div>
				<Snackbar
					anchorOrigin={{
						vertical: 'bottom',
						horizontal: 'left',
					}}
					open={this.state.open}
					autoHideDuration={5000}
					onClose={this.handleClose}
					ContentProps={{
						'aria-describedby': 'message-id',
					}}
					message={<span id="message-id">{this.state.message}</span>}
					action={[
						<IconButton
							key="close"
							aria-label="Close"
							color="inherit"
							className={classes.close}
							onClick={this.handleClose}
						>
							<CloseIcon />
						</IconButton>,
					]}
				/>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return { snackbarToggleStatus: state.snackbarReducer.snackbarToggleStatus }
}

export default connect(mapStateToProps)(withStyles(styles)(SnackbarComponent));

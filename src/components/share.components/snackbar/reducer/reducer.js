import { SNACKBAR_TOGGLE, SNACKBAR_RESET } from "../../../../constants/action-types";

const initialState = {
	snackbarToggleStatus: false
}
export const snackbarReducer = (state = initialState, action) => {
	switch (action.type) {
        case SNACKBAR_TOGGLE:
            // if (action.payload){
            //     return { snackbarToggleStatus: false };
            // }
			return { snackbarToggleStatus: state.snackbarToggleStatus = !state.snackbarToggleStatus };
        case SNACKBAR_RESET:
            // if (action.payload){
            //     return { snackbarToggleStatus: false };
            // }
			return { snackbarToggleStatus: false };

		default:
			return state;
	}
}

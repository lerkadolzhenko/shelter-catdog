import { SNACKBAR_TOGGLE, SNACKBAR_RESET } from "../../../../constants/action-types";

export const snackbarToggle = () =>{
    return{type: SNACKBAR_TOGGLE}
}
export const snackbarReset = () =>{
    return function (dispatch)
    {
        dispatch({type: SNACKBAR_RESET});
        dispatch({type: SNACKBAR_TOGGLE});
    }
}

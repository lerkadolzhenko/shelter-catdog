import React, {Component} from 'react';
import '../../admin.page/adminAnimalsAdd.component/adminAnimalsAdd.scss'
export class FillToSubmit extends Component {
    render() {
        const {value, show} = this.props;
        if (!show){
            return(
                <div className='belowSubmit'>
                    <span className='red'>{value}</span>
                </div>
            )
        } else return null


    }
}
import { SNACKBAR_TOGGLE } from "../../../../constants/action-types";

const initialState = {
    volunteerUser: []
}
export const snackbarReducer = (state = initialState, action) => {
    switch (action.type) {
        case SNACKBAR_TOGGLE:
            return { snackbarToggleStatus: state.snackbarToggleStatus = !state.snackbarToggleStatus };

        default:
            return state;
    }
}

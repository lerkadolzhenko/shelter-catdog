import React from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import "./buttonVolunteer.scss";
import { connect } from "react-redux";
import { VolunteerRequest } from "./actionCreators/actionCreators";

class ButtonVolunteer extends React.Component {

  state = {
    open: false,
  };
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleRequest = () => {
    this.props.dispatch(VolunteerRequest({ id: 21 }));
    this.handleClose()

  }

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div className="button-volunteer">
        <Button variant="contained" className="card_button_volunteer" onClick={this.handleClickOpen}>
          Volunteer
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          className='Dialog_Alert'
        >
          <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Do you want to apply to become a volunteer?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleRequest} color="primary" autoFocus>
              Yes!
            </Button>
            <Button onClick={this.handleClose} color="primary">
              No...
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
export default connect()(ButtonVolunteer);

import Axios from "axios";
import { apiUrl } from "../../../../config/apiUrl";
import { SNACKBAR_TOGGLE } from "../../../../constants/action-types";

export function VolunteerRequest(payload) {

    return function (dispatch) {
        return Axios
            .post(`${apiUrl}/role_requests?id=${payload.id}`, payload)
            .then(response => response.status === 200)
            .then(() => dispatch({ type: SNACKBAR_TOGGLE }))
    };
}
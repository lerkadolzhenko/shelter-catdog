import React from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import "./buttonHome.scss";
import Contacts from "../../home.page/Contacts.component/Contacts";

class ButtonHome extends React.Component {

  state = {
    open: false,
  };
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div className="button-home">
        <Button variant="contained" className="card_button_home" onClick={this.handleClickOpen}>
          Take pet
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          className='Dialog_Alert'
        >
          <DialogTitle id="alert-dialog-title">{"Find Us!"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <Contacts />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              OK!
            </Button>
          </DialogActions>
        </Dialog>

      </div>
    );
  }
}

export default ButtonHome;

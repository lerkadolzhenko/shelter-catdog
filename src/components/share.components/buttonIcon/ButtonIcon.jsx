import React, {Component} from 'react';
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";

export class ButtonIcon extends Component {

	render() {
		return (
			<Button className="button" variant="contained" color="secondary">
				{this.props.buttonTitle}
				<Icon className="icon">{this.props.buttonIcon}</Icon>
			</Button>
		);
	}
}

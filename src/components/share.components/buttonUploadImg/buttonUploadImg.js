import React, {Component} from 'react';
import Button from "@material-ui/core/Button";


export class ButtonUploadImg extends Component {

    render() {
        const {name, onChange, textValue } = this.props;
        return (
        <div>
            <label htmlFor="raised-button-file">
                <Button raised='true' component="span" className='asterisk' variant={'outlined'}>
                    {textValue}
                </Button>
            </label>
            <input
                name={name}
                onChange={onChange}
                accept="image/*"
                className='hidden'
                id="raised-button-file"
                multiple={true}
                type="file"
            />
        </div>
        );
    }
}

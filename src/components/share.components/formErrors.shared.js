import React from 'react';
import { Typography } from '@material-ui/core';

export const FormErrors = ({ fieldName, errorValue, classN }) => {
            if (errorValue) {
                return (
                    <div className={classN}>
                        <Typography color="error" variant="caption">{fieldName} {errorValue} </Typography>
                    </div>
                )
            } else {
                return '';

}}
import React, { Fragment } from 'react';
import { Icon } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import { FormErrors } from '../formErrors.shared';

export const UserAvatar = ({ image, bigAvatar, bigAvatarPlaceHolder, iconName, toolTipTitle, errorValue }) => {
    let render = <Fragment>
        <Tooltip title={toolTipTitle}>
            <Avatar src={image} className={bigAvatar}>
                <Icon className={bigAvatarPlaceHolder}>
                    {iconName}
                </Icon>
            </Avatar>
        </Tooltip>
        <FormErrors errorValue={errorValue}/>
    </Fragment>;
    return (
        <Fragment>
            {render}
        </Fragment>
    )

}
import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField";
import * as PropTypes from "prop-types";
import { FormErrors } from "../formErrors.shared";


export class InputText extends Component {

	render() {
		const { key, label, value, handleChange, className, name, disabled, errorValue, onBlur, error, fullWidth } = this.props;
		return (
			<div>
				<TextField
					key={key}
					name={name}
					label={label}
					className={className}
					value={value}
					onChange={handleChange}
                    disabled={disabled}
                    onBlur={onBlur}
                    error={error}
					margin="normal"
                    variant="outlined"
                    fullWidth={fullWidth}
				/>
                <FormErrors fieldName={label} errorValue={errorValue} />
			</div>
		);
	}
}

InputText.propsTypes = {
	label: PropTypes.string,
	value: PropTypes.string,
	handleChange: PropTypes.any,
	className: PropTypes.string

}

import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import './dialog.scss'

class AdminDialog extends React.Component {
	state = {
		open: this.props.state,
	};

	componentWillUnmount() {
		this.setState({open: false})
	}

	render() {
		const {onCloseAction = null, title = 'Are you sure?', text = '', buttons } = this.props;
        
        return (
			<div>
				<Dialog
					open={this.state.open}
					onClose={onCloseAction}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">{title}</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							{text}
						</DialogContentText>
					</DialogContent>
					<DialogActions>
                        {buttons.map((button, index) =>(
                            <Button key={index} onClick={button.action} className={button.className} color="primary">
							    {button.title}
						    </Button>
                        ))
                        }
					</DialogActions>
				</Dialog>
			</div>
		);
	}
}

export default AdminDialog;

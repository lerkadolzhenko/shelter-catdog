import React, {Component} from 'react';
import Button from "@material-ui/core/Button";


export class ButtonText extends Component {

	render() {
		const {type, propsValue, disabled} = this.props;
		return (
			<Button className="buttonSubmit" variant="contained" color="secondary" disabled={disabled} type={type}>
				{propsValue}
			</Button>
		);
	}
}

import React from 'react';
import './loader.scss'

class ModalLoader extends React.Component {


    // STRONGLY RECOMMEND use it with filter: blur(2px); css applied to loading page, because of saving our theme style!
  render() {
    return (
        <div>
            <img className="loader" src={require('../../../assets/loader.gif')} alt="lol" />
        </div>
    );
  }
}

export default ModalLoader;
import React from 'react';
import './Footer.scss'
import PetsIcon from '@material-ui/icons/Pets';

function Footer() {
  return (
    <footer className='compFooter'>
      <div className='header-white' align='center'>
        <PetsIcon /> Shelter Cat&Dog
      </div>
      <div className='text-black' align="center" component="p">
        We are waiting for you ! Our pets are waiting for you!
      </div>
    </footer>
  );
}

export default Footer;
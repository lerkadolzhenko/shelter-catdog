import * as CONST from "../../constants/action-types";
import axios from 'axios';
import * as url from "../../config/apiUrl.js";
import jwt_decode from 'jwt-decode';

export function eraseErrors(payload) {
    return {
        type: CONST.ERASE_ERRORS,
        payload: payload
    }
}
export function eraseAvatar(payload) {
    return {
        type: CONST.ERASE_AVATAR,
        payload: payload
    }
}

export function uploadImage(payload) {
    return (dispatch) => {
        dispatch({
            type: CONST.IS_IMAGE_LOADING
        })
        axios.post(url.apiUrl + url.usersAuthUrl + "/upload", payload.formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            axios.put(url.apiUrl + url.usersAuthUrl + "/", {id: payload.id, imageName: res.data.message})
                .then(resp => {
                    console.log(resp);
                    dispatch(isLogged(''));
                })
            dispatch({
                type: CONST.IMAGE,
                payload: res.data.message
            })
        })
    }
}

export function updateImage(payload) {
    return (dispatch) => {
        dispatch({
            type: CONST.IS_IMAGE_LOADING
        })
        axios.put(url.apiUrl + url.usersAuthUrl + "/upload", payload, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            dispatch({
                type: CONST.IMAGE,
                payload: res.data.message
            })
        })
    }
}

export function isLogged(payload) {
    return (dispatch) => {
        if (localStorage.getItem("authToken")) {
            dispatch({
                type: CONST.IS_LOADING
            });
            axios.get(url.apiUrl + url.usersAuthUrl + `/me`)
                .then(res => {
                    return dispatch({
                        type: CONST.IS_LOGGED,
                        payload: res.data
                    })
                })
                .catch(err => {
                    if (err.response) {                        
                        localStorage.removeItem("authToken");                        
                        return dispatch({
                            type: CONST.NOT_SIGNED_UP,
                            payload: err.response.data
                        })
                    }
                })
        } else {
            return dispatch({
                type: CONST.NOT_SIGNED_UP
            })
        }
    }
}



export function signIn(payload) {
    return (dispatch) => {
        axios.get(url.apiUrl + url.usersAuthUrl + `/login?email=${payload.email}&password=${payload.password}`, payload)
            .then(res => {
                localStorage.setItem('authToken', res.data.token)
                const user = jwt_decode(res.data.token);
                return dispatch({
                    type: CONST.USER_SIGNED,
                    payload: user
                })
            })
            .catch(err => {
                return dispatch({
                    type: CONST.SIGN_ERROR,
                    payload: err.response.data
                })
            })
    }

}

export function preventNonValidForm(payload) {
    return ({
        type: CONST.PREVENT_NON_VALID_FORM,
        payload: payload
    })
}
export function setDefaultRegisterState(payload) {
    return ({
        type: CONST.DEFAULT_REGISTER,
        payload: payload
    })
}

export function signUp(payload) {
    return (dispatch) => {
        axios.post(url.apiUrl + url.usersAuthUrl + '/register', payload)
            .then(response => response.status === 200)
            .then(() => dispatch({ type: CONST.SNACKBAR_TOGGLE }))
            .then(res => {
                return dispatch({
                    type: CONST.USER_REGISTERED,
                    payload: res.data
                })
            })
            .catch(err => {
                return dispatch({
                    type: CONST.SIGN_ERROR,
                    payload: err.response.data
                })
            })
    }
}
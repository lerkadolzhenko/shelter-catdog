export const defaultState = {
    render: true,
    username: '',
    email: '',
    password: '',
    repeatPassword: '',
    phone: '',
    fullName: '',
    avatar: '',
    role: 'user',
    valid: {
        username: false,
        email: false,
        password: false,
        repeatPassword: false,
        fullName: false,
        avatar: true
    },
    touched: {
        username: false,
        email: false,
        password: false,
        repeatPassword: false,
        fullName: false,
    },
    formErrors: {
        username: '',
        email: '',
        password: '',
        repeatPassword: '',
        fullName: '',
    },
    formValid: false
}

export function signInFormConstructor(state){
    return ([
        {
            fieldErrors: state.formErrors.email,
            value: state.email,
            error: !state.valid.email && state.touched.email,
            label: "Email",
            name: "email",
            autoComplete: "email",
            autoFocus: true
        },
        {
            fieldErrors: state.formErrors.password,
            value: state.password,
            error: !state.valid.password && state.touched.password,
            label: "Password",
            name: "password",
            type: "password",
            autoComplete: "password",
            autoFocus: false
        }
    ])
}
export function signUpFormConstructor(state){
    return ([
        {
            fieldErrors: '',
            value: state.avatar,
            error: !state.valid.avatar && state.touched.avatar,
            label: "Avatar",
            name: "avatar",
            type: "file",
        },
        {
            fieldErrors: state.formErrors.username,
            value: state.username,
            error: !state.valid.username && state.touched.username,
            label: "Username",
            name: "username",
            autoFocus: true
        },
        {
            fieldErrors: state.formErrors.fullName,
            value: state.fullName,
            error: !state.valid.fullName && state.touched.fullName,
            label: "Full Name",
            name: "fullName",
            autoFocus: false
        },
        {
            fieldErrors: state.formErrors.email,
            value: state.email,
            error: !state.valid.email && state.touched.email,
            label: "Email",
            name: "email",
            autoFocus: false
        },
        {
            fieldErrors: state.formErrors.password,
            value: state.password,
            error: !state.valid.password && state.touched.password,
            label: "Password",
            name: "password",
            type: "password",
            autoFocus: false
        },
        {
            fieldErrors: state.formErrors.repeatPassword,
            value: state.repeatPassword,
            error: !state.valid.repeatPassword && state.touched.repeatPassword,
            label: "Repeat Password",
            name: "repeatPassword",
            type: "password",
            autoFocus: false
        },
        {
            value: state.phone,
            error: false,
            label: "Phone",
            name: "phone",
            autoFocus: false
        },

    ])
}

export function changeUserDataFormConstructor(state){
    return ([
        {
            fieldErrors: state.formErrors.username,
            value: state.username,
            error: !state.valid.username && state.touched.username,
            label: "Username",
            name: "username",
            autoComplete: "username",
            autoFocus: true
        },
        {
            fieldErrors: state.formErrors.fullName,
            value: state.fullName,
            error: !state.valid.fullName && state.touched.fullName,
            label: "Full Name",
            name: "fullName",
            autoComplete: "fullName",
            autoFocus: false
        },
        {
            fieldErrors: state.formErrors.email,
            value: state.email,
            error: !state.valid.email && state.touched.email,
            label: "Email",
            name: "email",
            autoComplete: "email",
            autoFocus: false
        },
        {
            value: state.phone,
            error: false,
            label: "Phone",
            name: "phone",
            autoComplete: "phone",
            autoFocus: false
        },

    ])
}
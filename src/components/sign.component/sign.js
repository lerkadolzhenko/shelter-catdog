import React, { Component } from 'react';
import SignForm from "../share.components/signForm.modal/signForm";
import SignOut from "../signOut.modal/signout"
import { connect } from 'react-redux';
import { Provider } from "react-redux";
import { store } from "../../store/app.store";
import Popover from '@material-ui/core/Popover';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state/index';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Fab from '@material-ui/core/Fab';
import withStyles from '@material-ui/core/styles/withStyles';
import Tooltip from '@material-ui/core/Tooltip';
import * as actionCreators from "./action-creator";
import { defaultState } from "./signConsts"
import { signInFormConstructor } from "./signConsts"
import { signUpFormConstructor } from "./signConsts"
import { validateField } from "./validator"
import { validateForm } from "./validator"
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import ModalLoader from "../share.components/loader/loader"
import PropTypes from 'prop-types';
import Dialog from '../share.components/dialog/dialog';
import * as CONSTANTS from "../../constants/messages";


const styles = theme => ({
    fab: {
        boxShadow: 'none'
    }
});

const childClasses = {
    main: "mainSignIn",
    paper: "paperSignIn",
    avatar: "avatarSignIn",
    bigAvatar: "avatarSignUp",
    bigAvatarSigned: "avatarSignUp",
    bigAvatarPlaceHolder: "avatarSignUpIcon",
    bigNoAvatarPlaceHolder: "noAvatarSignUpIcon",
    form: "formSignIn",
    submit: "submitSignIn",
    typography: "typographySignIn",
    error: "errorSignIn",
    fab: "fabSignIn",
    link: "linkSignIn",
    loader: "loader",
    modal: "modalDivision"
};

function buttons(action, title, className = []) {
    let res = [];
    console.log(className[0]);
    for(let i = 0; i < action.length; i++){
        console.log(className[i]);
        res.push({
            title: title[i],
            action: action[i],
            className: className[i]
        })
    }
    return res;
}

function mapStateToProps(state) {
    return {
        isLogged: state.sign.isLogged,
        error: state.sign.error,
        isRegistered: state.sign.isRegistered,
        imageName: state.sign.imageName,
        isImageLoading: state.sign.isImageLoading,
        user: state.sign.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addAction: payload => dispatch(payload)
    }
}

class Sign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            render: true,
            username: '',
            email: '',
            password: '',
            repeatPassword: '',
            phone: '',
            fullName: '',
            avatar: null,
            role: 'user',
            valid: {
                username: undefined,
                email: undefined,
                password: undefined,
                repeatPassword: undefined,
                fullName: undefined,
                avatar: true
            },
            touched: {
                username: false,
                email: false,
                password: false,
                repeatPassword: false,
                fullName: false,
            },
            formErrors: {
                username: '',
                email: '',
                password: '',
                repeatPassword: '',
                fullName: '',
                avatar: ''
            },
            formValid: false,
            loading: false,
            isModalOpened: false,
            message: ''
        };
    }

    handleInfoModalClose = () => {
        this.setState({ isModalOpened: false });
        this.eraseErrors();
        this.props.addAction(actionCreators.setDefaultRegisterState(""));
    }

    handleSubmit = e => {
        e.preventDefault();
        const user = this.state.render ? {
            email: this.state.email,
            password: this.state.password
        } : {
                username: this.state.username,
                email: this.state.email,
                password: this.state.password,
                phone: this.state.phone,
                fullName: this.state.fullName,
                avatar: this.state.avatar,
                role: this.state.role,
            }
        if (this.state.formValid) {
            this.state.render ? this.props.addAction(actionCreators.signIn(user)) : this.props.addAction(actionCreators.signUp(user));
            this.setState({ loading: true });
        } else {
            this.props.addAction(actionCreators.preventNonValidForm("Some of fields doesn`t filled right."))
        }
    }

    handleUserInput = (event) => {
        let { name, value } = event.target;
        let validator = validateField(name, value, this.state.password);
        this.setState({ ...this.state, [name]: value, formErrors: { ...this.state.formErrors, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] } }, () => {
            const formValidator = validateForm(this.state.render, this.state.valid);
            this.setState({ ...this.state, formValid: formValidator });
            if (name === "password") {
                validator = validateField("repeatPassword", this.state.repeatPassword, value);
                this.setState({ ...this.state, formErrors: { ...this.state.formErrors, repeatPassword: validator.formErrors.repeatPassword }, valid: { ...this.state.valid, repeatPassword: validator.repeatPassword } }, () => {
                    const formValidator = validateForm(this.state.render, this.state.valid);
                    this.setState({ ...this.state, formValid: formValidator });
                })
            }
        });
    }

    handlePhoneInput = value => {
        this.setState({
            phone: value
        });
    }

    handleImageLoad = (event) => {
        let name = event.target.name;
        let formData = new FormData();
        let source = event.target;            
            if (event.target.files[0]) {
                formData.append("avatar", event.target.files[0], event.target.files[0].name);
                let file = event.target.files[0];
                let validator = validateField(name, file);
                this.setState({ ...this.state, formErrors: { ...this.state.formErrors, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] } }, () => {
                    const formValidator = validateForm(this.state.render, this.state.valid);
                    this.setState({ ...this.state, formValid: formValidator });
                    if (this.state.valid.avatar) {
                        this.props.addAction(source !== "updateAvatar" ? actionCreators.uploadImage({formData, ...this.props.user}) : actionCreators.updateImage(formData));
                    } else {
                        this.setState({avatar: ''})
                        this.props.addAction(actionCreators.eraseAvatar(''))
                    }
                });
            } else {
                this.setState({formErrors: {...this.state.formErrors, avatar: ''}, avatar: '', valid: {...this.state.valid, avatar: true}, formValid: true})
                this.props.addAction(actionCreators.eraseAvatar(''))
            }
    }

    onFieldBlur = event => {
        const name = event.target.name;
        this.setState({ ...this.state, touched: { ...this.state.touched, [name]: true } });
    }

    toggle = () => {
        this.eraseErrors();
    }

    eraseErrors = () => {
        const validator = validateField('clearErrors', null, this);
        this.setState({ ...defaultState, formErrors: validator, render: !this.state.render }, () => {
        });
        this.props.addAction(actionCreators.eraseErrors(''));
    }

    componentDidUpdate(nextProps) {
        if ((this.props.isLogged || (this.props.error.length !== 0)) && (this.state.loading !== false)) {
            this.setState({ ...this.state, loading: false })
        }
        if ((nextProps.isRegistered !== this.props.isRegistered) && (nextProps.isRegistered === false)) {
            this.setState({ ...this.state, loading: false })
            this.toggle();
        }
        if ((this.props.message === CONSTANTS.CONFIRM_YOUR_EMAIL_MESSAGE) && (this.props.message !== nextProps.message)) {
            this.setState({ message: this.props.message });
            this.setState({ isModalOpened: true });
        }
        if (this.props.isRegistered && !this.state.isModalOpened) {
            this.setState({ isModalOpened: true });
        }
        if (("http://localhost:5000/api/users/authenticate/upload/" + this.props.imageName) !== this.state.avatar && this.props.imageName) {
            this.setState({ avatar: this.props.imageName ? "http://localhost:5000/api/users/authenticate/upload/" + this.props.imageName : this.props.imageName });
        }
    }

    render() {
        const { classes } = this.props;
        let sign = null;
        const formFieldsData = this.state.render ? signInFormConstructor(this.state) : signUpFormConstructor(this.state)
        childClasses.paper = this.state.loading ? "paperSignInLoading" : "paperSignIn";
        let loader = null;
        let dialog = null;
        if (this.state.loading) {
            loader = <ModalLoader />
        }
        if (!this.props.isLogged) {
            const title = this.state.render ? 'Sign In' : 'Sign Up';
            const linkText = this.state.render ? 'Not Signed Up?' : 'Already have an account?';
            sign = <SignForm avatarErrorValue={this.state.formErrors.avatar} image={this.state.avatar} handleLoad={this.handleImageLoad} error={this.props.error} render={this.state.render} loading={this.state.loading} classes={childClasses} linkText={linkText} title={title} toggle={this.toggle} formFieldsData={formFieldsData} superState={this.state} handlePhoneInput={this.handlePhoneInput} handleUserInput={this.handleUserInput} handleSubmit={this.handleSubmit} onFieldBlur={this.onFieldBlur}></SignForm>;
        }
        else {
            sign = <SignOut eraseErrors={this.eraseErrors} avatarErrorValue={this.state.formErrors.avatar} handleLoad={this.handleImageLoad} image={this.state.avatar} bigAvatar={childClasses.bigAvatarSigned} bigAvatarPlaceHolder={childClasses.bigNoAvatarPlaceHolder}></SignOut>;
        }
        if ((this.props.isRegistered && this.state.isModalOpened) || (this.state.message.length > 0 && this.state.isModalOpened)) {
            dialog = <Dialog
                state={this.state.isModalOpened}
                onCloseAction={this.handleInfoModalClose}
                title={this.state.message ? "Confirm your Email!" : "Confirmation"}
                text={this.state.message ? this.state.message : "Please confirm your account via link that we sent to Your email. Link will expire in 1 day!"}
                buttons={buttons([this.handleInfoModalClose], ["Close"], ["dialogCloseButtonFront"])}
            ></Dialog>
        }
        return (
            <div>
                <PopupState variant="popover" popupId="demo-popup-popover">
                    {popupState => (
                        <div>
                            <Tooltip title="Profile">
                                <Fab color="primary" aria-label="Add" {...bindTrigger(popupState)} className={classes.fab}>
                                    <AccountCircle />
                                </Fab>
                            </Tooltip>
                            <Popover
                                elevation={24}
                                {...bindPopover(popupState)}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'center',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                }}
                                className={childClasses.modal}
                            >
                                <Provider store={store}>
                                    <main className={childClasses.main}>
                                        {loader}
                                        <CssBaseline />
                                        <Paper className={childClasses.paper}>
                                            {sign}
                                        </Paper>
                                    </main>
                                </Provider>
                            </Popover>
                        </div>
                    )}
                </PopupState>
                {dialog}
            </div>
        )
    }
}

Sign.propTypes = {
    isLogged: PropTypes.bool.isRequired,
    isRegistered: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Sign));
import { defaultState } from "./signConsts"

export function validateField(fieldName, value, password = '') {
    let fieldValidationErrors = {
        username: '',
        email: '',
        password: '',
        repeatPassword: '',
        fullName: '',
        name: '',
        kind: '',
        birth_date: '',
        arrival_date: '',
        health_status: '',
        link: '',
        shelter_phone: '',
        text: '',
        title: '',
        creation_date: ''
    };
    const emailRegEx = /([a-z]+[0-9]*[._-]?){3,30}[a-z0-9]+@([a-z0-9]+[._-]?){1,30}\w+\.[a-z]{2,6}/i;
    const userNameRegEx = /^\w+$/;
    const nameRegEx = /^[a-zа-яіїє ,.'-]+$/i;
    const dateRegEx = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
    const phoneRegEx = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
    const imgRegEx = /([a-zа-яіїє0-9\s_\\.\-:])+(.jpe?g|.png)$/i;
    const reportsRegEx = /([a-zа-яіїє0-9\s_\\.\-:])+(.doc?x|.pdf)$/i;

    let emailValid = false;
    let avatarValid = false;
    let shelter_phoneValid = false;
    let passwordValid = false;
    let usernameValid = false;
    let fullNameValid = false;
    let repeatPasswordValid = false;
    let nameValid = false;
    let kindValid = false;
    let birthDateValid = false;
    let creationDateValid = false;
    let arrivalDateValid = false;
    let healthStatusValid = false;
    let linkValid = false;
    let textValid = false;
    let titleValid = false;

    switch (fieldName) {
        case 'email':
            emailValid = !!value.match(emailRegEx);
            fieldValidationErrors.email = emailValid ? '' : ' formatting is wrong. Try something like example@mail.co';
            return {
                formErrors: fieldValidationErrors,
                email: emailValid,
            }
        case 'avatar':
            let avatarTypeValid = false;
            let avatarSizeValid = false;
            avatarSizeValid = value.size < 1000000;
            fieldValidationErrors.avatar = avatarSizeValid ? '' : 'Image is too large for avatar. Max size 1MB.';
            avatarTypeValid = value.type.slice(0, 5) === "image";
            fieldValidationErrors.avatar = avatarTypeValid ? fieldValidationErrors.avatar : 'You can upload only images.';
            avatarValid = avatarTypeValid && avatarSizeValid;
            return {
                formErrors: fieldValidationErrors,
                avatar: avatarValid,
            }
        case 'password':
            passwordValid = value.length >= 6;
            fieldValidationErrors.password = passwordValid ? '' : ' length less than 6 symbols.';
            repeatPasswordValid = password === value;
            fieldValidationErrors.repeatPassword = repeatPasswordValid ? '' : ' doesn`t match.';
            return {
                formErrors: fieldValidationErrors,
                password: passwordValid,
            }
        case 'username':
            usernameValid = !!(value.length >= 4 && value.match(userNameRegEx));
            if (!usernameValid) {
                fieldValidationErrors.username = value.length >= 8 ? 'symbols can contains only "a-z, 0-9 and _".' : ' length less than 4 symbols.';
            } else {
                fieldValidationErrors.username = '';
            }
            return {
                formErrors: fieldValidationErrors,
                username: usernameValid
            }
        case 'fullName':
            fullNameValid = value.length >= 3 && value.match(/^[^0-9]+$/) ? true : false;
            if (!fullNameValid) {
                fieldValidationErrors.fullName = value.length >= 3 ? 'symbols can contains only "a-z, 0-9 and _".' : ' length less than 3 symbols.';
            } else {
                fieldValidationErrors.fullName = '';
            }
            fieldValidationErrors.fullName = fullNameValid ? '' : ' length less than 3 symbols.';
            return {
                formErrors: fieldValidationErrors,
                fullName: fullNameValid,
            }
        case 'repeatPassword':
            repeatPasswordValid = password === value;
            fieldValidationErrors.repeatPassword = repeatPasswordValid ? '' : ' doesn`t match.';
            return {
                formErrors: fieldValidationErrors,
                repeatPassword: repeatPasswordValid,
            }

        case 'shelter_phone':
            shelter_phoneValid = !!(value.length >= 10 && value.match(phoneRegEx));
            if (!shelter_phoneValid) {
                fieldValidationErrors.shelter_phone = value.length >= 11 ? 'symbols can contains only "+, 0-9 and -".' : ' length less than 11 symbols.';
            } else {
                fieldValidationErrors.shelter_phone = '';
            }
            return {
                formErrors: fieldValidationErrors,
                shelter_phone: shelter_phoneValid
            }

        case 'clearErrors':
            return { ...defaultState.formErrors }

        case 'name':
            nameValid = !!(value.length >= 2 && value.length <= 15 && value.match(nameRegEx));
            if (!nameValid) {
                if (/\d/.test(value)) {
                    fieldValidationErrors.name = 'can`t contain numbers';
                } else {
                    fieldValidationErrors.name = value.length > 15 ? 'can contain "a-z, 0-9, `, - _". and can`t be longer 15 characters' : 'contains forbidden symbols or it\'s length less than 2 symbols.'
                }
                ;
            } else {
                fieldValidationErrors.name = '';
            }
            return {
                formErrors: fieldValidationErrors,
                name: nameValid
            };

        case 'kind':
            kindValid = value.length >= 1;
            fieldValidationErrors.kind = kindValid ? '' : 'kind is required';
            return {
                formErrors: fieldValidationErrors,
                kind: kindValid
            };

        case 'birth_date':
            birthDateValid = (value.length === 10 && value.match(dateRegEx));
            if (!birthDateValid) {
                fieldValidationErrors.birth_date = birthDateValid ? '' : 'format is 2019-12-31';
            }

            return {
                formErrors: fieldValidationErrors,
                birth_date: birthDateValid
            };

        case 'arrival_date':
            arrivalDateValid = (value.length === 10 && value.match(dateRegEx));
            if (!arrivalDateValid) {
                fieldValidationErrors.arrival_date = arrivalDateValid ? '' : 'format is 2019-12-31';
            }

            return {
                formErrors: fieldValidationErrors,
                arrival_date: arrivalDateValid
            };
        case 'creation_date':
            creationDateValid = !!(value.length === 10 && value.match(dateRegEx));
            fieldValidationErrors.creation_date = creationDateValid ? '' : 'format is 2019-12-31';
            return {
                formErrors: fieldValidationErrors,
                creation_date: creationDateValid
            };

        case 'health_status':
            healthStatusValid = value.length >= 1;
            fieldValidationErrors.health_status = healthStatusValid ? '' : 'health status is required';
            return {
                formErrors: fieldValidationErrors,
                health_status: healthStatusValid
            };

        case 'link':
            linkValid = !!(value.length >= 1 && value.match(imgRegEx));
            if (!linkValid){
                fieldValidationErrors.link = 'only .jpeg/.jpg/.png extensions are allowed';
            } else {
                fieldValidationErrors.link = '';
            }
            return {
                formErrors: fieldValidationErrors,
                link: linkValid
            };

        case 'text':
            textValid = !!(value.length >= 1 && value.match(reportsRegEx));
            if (!textValid){
                fieldValidationErrors.text = 'Only .doc/.docx/.pdf extensions are allowed';
            } else {
                fieldValidationErrors.text = '';
            }
            return {
                formErrors: fieldValidationErrors,
                text: textValid
            };

        case 'title':
            titleValid = value.length >= 1;
            fieldValidationErrors.title = titleValid ? '' : 'Title is required';
            return {
                formErrors: fieldValidationErrors,
                title: titleValid
            };

        default:
            return { formErrors: '' }
    }
}

export function validateForm(render, state) {
    if (render) {
        return state.email && state.password
    } else {
        return state.email &&
            state.password &&
            state.username &&
            state.fullName &&
            state.repeatPassword &&
            state.avatar
    }
}

export function validateAnimalForm(state) {
    return state.name &&
        state.kind &&
        state.birth_date &&
        state.arrival_date &&
        state.health_status &&
        state.link
}

export function validateReportForm(state) {
    return state.title &&
        state.creation_date &&
        state.text
}

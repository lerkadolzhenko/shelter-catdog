import React, {Component} from 'react'
import { connect } from "react-redux";
import { ButtonText } from "../../share.components/buttonText/ButtonText";
import TextField from '@material-ui/core/TextField'
import actions from '../../home.page/sosInfo.component/actionCreators/actionCreator'
import { adminUrl } from "../../../config/apiUrl";
import { Redirect } from "react-router-dom";

class AdminSosListForm extends Component {

    state = {
        description: '',
        id: null
    }

    componentDidMount() {
        this.props.getSosList()
        console.log(this.props.sosList);
        
        if (this.props.type !== 'add') {
            const id = Number(this.props.match.params.id);
            const {description} = this.props.sosList.find((item) => item.id === id);
            this.setState({description, id})
        }
    }
    onHandleSubmit = (event) => {
        const {id, description} = this.state
        event.preventDefault();
        if (this.props.type === 'add') {
            this.props.addSosList({description});
        } else {
            this.props.editSosList({id, description});
        }
    };
    onHandleChange = (e) => {        
        this.setState({description: e.target.value});
    }

    render() {
        const {description} = this.state

        if (this.props.reqStatus) {
            return <Redirect to={`${adminUrl}/The shelter needs`} />;
        }
        
        return (
            <div>
                <form className="admin-users-form" onSubmit={this.onHandleSubmit}>
                    <TextField
                        style={{width: '100%'}}
                        id="outlined-multiline-static"
                        label="Description"
                        multiline
                        rows="4"
                        value={description}
                        margin="normal"
                        variant="outlined"
                        onChange={this.onHandleChange}
                        />
                    <ButtonText type="submit" propsValue="Submit"/>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
	return {
        sosList: state.sosListReducer.sosList,
        reqStatus: state.sosListReducer.reqStatus,
    }
}
export default connect(mapStateToProps, actions)(AdminSosListForm)

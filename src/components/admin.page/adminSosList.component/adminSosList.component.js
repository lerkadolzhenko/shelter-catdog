import React, { Component } from 'react'
import { connect } from 'react-redux'
import {Table, TableHead, TableRow, TableCell, TableBody, Paper, CssBaseline, Icon } from '@material-ui/core'
import { Link } from "react-router-dom";
import { ButtonIcon } from "../../share.components/buttonIcon/ButtonIcon";
import { InputText } from "../../share.components/inputText/inputText";
import actions from '../../home.page/sosInfo.component/actionCreators/actionCreator'
import AdminDialog from "../../share.components/dialog/dialog";
import SnackbarComponent from "../../share.components/snackbar/snackbar";
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';


export class AdminSosList extends Component {
    state = {
        searchValue: '',
        sosList: null,
        openDialog: false,
        id: 0,
    }
    
    getSosListData = () => {
		this.setState({sosList: this.props.sosList})
	}
    
    componentDidMount() {
        this.props.getSosList()
        this.getSosListData()
    }

    componentDidUpdate(prevProps, prevState) {
		if (this.props.sosList !== prevProps.sosList) {
			this.getSosListData();
		}
        const {searchValue} = this.state;

		if (searchValue !== prevState.searchValue) {
            const sosList = this.props.sosList.filter((item) => item.description.toLowerCase().includes(searchValue.toLowerCase()))

            this.setState({sosList});
            
			if (searchValue === '') {
				this.getSosListData();
			}
        }
        
    }

    dialogButton = (title, action) => {
        return  {
            title: title,
            action: action
        }
    }
        
    onHandleChange = event => {        
        const name = event.target.name;
        const value = event.target.value;
        
        this.setState({[name]: value});
    }

	showDialog = () => {
		this.setState(prevState => ({
			openDialog: !prevState.openDialog
		}))
    }
    
    submitFromModal = () => {
		this.showDialog();
        this.props.deleteSosList(this.state.id)
        this.props.getSosList()
        this.setState({isDeleted: true, isAdded: false, isEdited: false})
	}

    render() {
        const {sosList, openDialog} = this.state;
        const {snackbarToggle, sosListDeleted, sosListAdded, sosListEdited} = this.props
        
        let sosListBlock = null;

        const modal = (openDialog)? <AdminDialog buttons={[
                                                    this.dialogButton('yes', this.submitFromModal), 
                                                    this.dialogButton('no', this.showDialog)
                                                 ]}
                                                 title='Delete'
                                                 text={`Are you sure you want to remove this item?`}
                                                 state={openDialog}
                                            /> : null
        
        if (sosList) {
            
            sosListBlock = sosList.map((item) => (
                <TableRow key={item.id}>
                    <TableCell component="th" scope="row">
                        {item.description}
                    </TableCell>
    
                    <TableCell align="right">
                        <div>
                            <Link to={this.props.match.path + '_edit/' + item.id}>
                                <Icon>edit</Icon>
                            </Link>
                            <DeleteOutlinedIcon className='pointer' onClick={() => {
                                this.showDialog();
                                this.setState({id: item.id})
                            }}/>
                        </div>
                    </TableCell>
                </TableRow>
            ))
        }
        
        let snackbar = null
        if (snackbarToggle && sosListAdded) {
            snackbar = <SnackbarComponent message='Added'/>;
        } else if (snackbarToggle && sosListEdited) {
            snackbar = <SnackbarComponent message='Updated' />;
        } else if (snackbarToggle && sosListDeleted) {
            console.log('asfasf');
            
            snackbar = <SnackbarComponent message='Deleted' />;
        }

        return (
			<div className='mt5prs' >
				<div className="add-btn">
					<Link to={this.props.match.path + '_add/'}>
					    <ButtonIcon buttonTitle='Add' buttonIcon="add"/>
					</Link>
				</div>
				<InputText label="Search" className='searchBar'  value={this.state.searchValue} handleChange={this.onHandleChange} name="searchValue"/>

				<Paper className='paper-margin'> 
					<CssBaseline/>
					<Table >
						<TableHead className='tableHeader' >
							<TableRow>
								<TableCell className='white ' >Text</TableCell>
								<TableCell className='white width30'  align="right">Actions</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{sosListBlock}
						</TableBody>
					</Table>
                    {modal}
				</Paper>
                {snackbar}
			</div>
        )
    }
}
const mapStateToProps = state => {
	return {
        sosList: state.sosListReducer.sosList,
        snackbarToggle: state.snackbarReducer.snackbarToggleStatus,
        sosListAdded: state.sosListReducer.sosListAdded,
        sosListEdited: state.sosListReducer.sosListEdited,
        sosListDeleted: state.sosListReducer.sosListDeleted
    }
}
export default connect(mapStateToProps, actions)(AdminSosList);


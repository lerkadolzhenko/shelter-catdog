import {
	USER_DELETED,
	USERS_LOADED,
	EDIT_USER,
    SNACKBAR_TOGGLE,
    SNACKBAR_RESET,
    REQ_ERROR,
    SETTING_DEFAULTS,
    USER_EDITED
} from "../../../../constants/action-types";
import axios from "axios";
import { usersUrl, apiUrl } from "../../../../config/apiUrl";

export function getData() {
	return function (dispatch) {
		return axios
			.get(`${apiUrl}${usersUrl}`)
			.then(response => response.data)
			.then(json => {
				dispatch({type: USERS_LOADED, payload: json});
			});
	};
}

export const editUser = (payload) => {
	return (dispatch) => {
		return axios
			.put(`${apiUrl}${usersUrl}?id=${payload.id}`, payload)
			.then(response => response.status === 200)
			.then(() => {
                dispatch({type: EDIT_USER, payload: payload})
                dispatch({type: USER_EDITED, payload: payload})
                setTimeout(() => {
                    dispatch({type: SETTING_DEFAULTS, payload: payload})
                }, 500)
				
            })
            .then(() => dispatch({type: SNACKBAR_RESET}))
            .then(() => dispatch({type: SNACKBAR_TOGGLE}))
            .catch(err => {
                if(err.response)
                console.log(err.response.data);
                dispatch({
                    type: REQ_ERROR,
                    payload: err.response.data
                })
            })
	}
}
export const deleteUser = (payload) => {
	return (dispatch) => {
		return axios
			.delete(`${apiUrl}${usersUrl}?id=${payload}`)
			.then(response => response.status === 200 && response.data === ''
				? dispatch({type: USER_DELETED, payload: payload})
				: dispatch({type: SNACKBAR_TOGGLE}))

	}
}

export const eraseError = (payload) => {
	return (dispatch) => dispatch({
        type: SETTING_DEFAULTS,
        payload: payload
    })
}




import {
	USER_DELETED,
	EDIT_USER,
    USERS_LOADED,
    REQ_ERROR,
    SETTING_DEFAULTS,
    USER_EDITED
	}
from "../../../../constants/action-types";

const initialState = {
    users: [],
    error: '',
    isUserEdited: false
};
export const adminUsersListReducer = (state = initialState, action) => {
	switch (action.type) {

		case USERS_LOADED:
			return {...state, users: action.payload.reverse()};

		case EDIT_USER:
			return {
				...state, isUserEdited: false, users: state.users.map((user) =>
					user.id === action.payload.id ? {...action.payload} : user).reverse()
			};

		case USER_DELETED:
            return {...state, users: state.users.filter((item) => item.id !== action.payload)};
		case USER_EDITED:
            return {...state, isUserEdited: true};
            
		case REQ_ERROR:
            return {...state, error: action.payload};

        case SETTING_DEFAULTS:
            return {...state, error: '', isUserEdited: false};
            
		default:
			return state;
	}
}

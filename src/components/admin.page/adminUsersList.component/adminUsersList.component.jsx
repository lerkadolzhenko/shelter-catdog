import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Icon from '@material-ui/core/Icon';
import { deleteUser, getData } from "./actionCreators/actionCreators";
import SnackbarComponent from "../../share.components/snackbar/snackbar";
import AdminDialog from "../../share.components/dialog/dialog";
import queryString from 'query-string'
import './adminUsersList.scss'
import { InputText } from "../../share.components/inputText/inputText";
import * as MESSAGES from "../../../constants/messages";
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';


const styles = theme => ({
	root: {
		width: '100%',
		marginTop: theme.spacing.unit,
		overflowX: 'auto',
	},
	table: {
		minWidth: 700,
	},
});

function buttons(action, title, className) {
    let res = [];
    for(let i = 0; i < action.length; i++){
        console.log(className[i]);
        res.push({
            title: title[i],
            action: action[i],
            className: className[i]
        })
    }
    return res;
}

class AdminUsersList extends React.Component {
	state = {
		openDialog: false,
		id: 0,
		searchValue: '',
        usersList: null,
        snackbarMessage: ''
	}

	componentDidMount() {
        this.props.dispatch(getData())
        this.setState({...this.state, snackbarMessage: queryString.parse(this.props.location.search)[Object.keys(queryString.parse(this.props.location.search))[0]]},() => {
        })
    }

	componentDidUpdate(prevProps, prevState, snapshot) {
		if (this.props.users !== prevProps.users) {
			this.getUsersData();
		}
		if (this.state.searchValue !== prevState.searchValue) {
			this.setState({
				usersList: this.props.users.filter((item) => item.username.includes(this.state.searchValue))
			})
			if (this.state.searchValue === '') {
				this.getUsersData();
			}
        }
    }
    

	getUsersData = () => {
		this.setState({
			usersList: this.props.users
		})
	}

	showDialog = () => {
		this.setState(prevState => ({
			openDialog: !prevState.openDialog
		}))
	}

	dialogButton = (title, action) => {
        return  {
            title: title,
            action: action
        }
    }

	onHandleChange = event => {
		let name = event.target.name;
		let value = event.target.value;
		this.setState({[name]: value});
	}

	submitFromModal = () => {
        this.showDialog();
		this.props.dispatch(deleteUser(this.state.id))
	}

	render() {
		const {classes} = this.props;
		const {usersList} = this.state;
		let modal = null;
		let snackbar = null;
		let tablesItems = null;
		if (usersList !== null) {
			tablesItems = usersList.map((userItem, index) => (
				<TableRow key={index}>
					<TableCell component="th" scope="row">
						{userItem.username}
					</TableCell>
					<TableCell component="th" scope="row">
						{userItem.fullname}
					</TableCell>
					<TableCell component="th" scope="row">
						{userItem.email}
					</TableCell>
					<TableCell component="th" scope="row">
						{userItem.role}
					</TableCell>
					<TableCell align="right">
						<div>
							<Link to={this.props.match.path + '_edit/' + userItem.id}>
								<Icon>edit</Icon>
							</Link>

							<DeleteOutlinedIcon className="userDeleteIcon pointer" onClick={() => {
								this.showDialog();
								if (userItem.role === "admin") {
									this.setState({snackbarMessage: MESSAGES.ADMIN_ERROR_LAST_ADMIN})
								}
								this.setState({id: userItem.id})
							}}/>


						</div>
					</TableCell>
				</TableRow>
			))
		}
		if (this.state.openDialog) {
			modal =
                <AdminDialog showToggle={this.showDialog} title={"Delete"} text={"Are you sure you want to delete this user?"} buttons={buttons([this.submitFromModal, this.showDialog], ["Delete", "Close"], ["dialogDeleteButton", "dialogCloseButton"])} state={this.state.openDialog}/>;
		}
		if (this.props.snackbarToggle && this.state.snackbarMessage) {
			snackbar = <SnackbarComponent message={this.state.snackbarMessage}/>
		}
		return (
			<div className='user-list'>
				<InputText className={"userListSearchBar"} label="Search" value={this.state.searchValue} handleChange={this.onHandleChange} name="searchValue"/>
				<Paper className={classes.root}>
					<CssBaseline/>
					<Table className={classes.table}>
						<TableHead className='tableHeader'  >
							<TableRow>
								<TableCell className='white' >User name</TableCell>
								<TableCell className='white'  >Full name</TableCell>
								<TableCell className='white'  >E-mail</TableCell>
								<TableCell className='white'  >Role</TableCell>
								<TableCell className='white'   align="right">Actions</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{tablesItems}
						</TableBody>
					</Table>
					{snackbar}
					{modal}
				</Paper>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		users: state.adminUsersListReducer.users,
		isEdited: state.adminUsersListReducer.isUserEdited,
		snackbarToggle: state.snackbarReducer.snackbarToggleStatus
	}
}

export default connect(mapStateToProps)(withStyles(styles)(AdminUsersList));

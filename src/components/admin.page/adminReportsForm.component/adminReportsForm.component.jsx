import React from 'react';
import { editReport, createReport, uploadReport } from "../adminReportsList.component/actionCreators/actionCreators";
import { connect } from "react-redux";
import { ButtonText } from "../../share.components/buttonText/ButtonText";
import TextField from '@material-ui/core/TextField';
import './adminReportsForms.scss';
import { Redirect } from "react-router-dom";
import { adminUrl, reportsUrl } from "../../../config/apiUrl";
import { validateField, validateForm } from '../../sign.component/validator';
import { FormErrors } from '../../share.components/formErrors.shared';
import Button from '@material-ui/core/Button';
import {FillToSubmit} from "../../share.components/fillToSubmit/fillToSubmit";

class AdminReportsForm extends React.Component {
	state = {
		title: '',
		text: '',
		creation_date: '',
        formErrors:{
			creation_date: '',
			text: '',
			title: ''
        },
        valid:{
			creation_date: false,
			text: false,
			title: false
		},
        formValid: false
	}

	componentDidMount() {
		
		if (this.props.type !== 'add') {
			const id = Number(this.props.match.params.id);

			const reportItem = this.props.reports.find((report) => report.id === id);
			this.setState({
				title: reportItem.title,
				text: reportItem.text,
				creation_date: reportItem.creation_date,
				id: reportItem.id,
				valid: {
					creation_date: true,
					title: true,
					text: true
				}
			})
        }
	}

	onInputReport = event => {
		const name = 'text'
		let value = '';
		if (event.target.files[0]) {
			value = event.target.files[0].name;
			let validator = validateField(name, value);
			this.setState({ ...this.state, [name]: value, formErrors: { ...this.state.formErrors, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] } }, () => {
			const formValidator = validateForm(this.state.render, this.state.valid);
			this.setState({ ...this.state, formValid: formValidator }, () => {
				if (this.state.valid.text) {
					this.props.dispatch(uploadReport(bodyFormData));
				}
			});
			}); 
        	let bodyFormData = new FormData();
			bodyFormData.append('report', event.target.files[0]);
			this.setState({
				text: value
			})
		}

    };
	
	onHandleChange = event => {
		let name = event.target.name;
		let value = event.target.value;
        let validator = validateField(name, value);
        this.setState({ ...this.state, [name]: value, formErrors: { ...this.state.formErrors, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] } }, () => {
            const formValidator = validateForm(this.state.render, this.state.valid);
            this.setState({ ...this.state, formValid: formValidator });
        });
	}

	onHandleSubmit = (event) => {
		if (this.props.type === 'add') {
			event.preventDefault();
            this.setState({...this.state})
			this.props.dispatch(createReport(this.state));
		} else {
			event.preventDefault();
			this.setState({...this.state});
			this.props.dispatch(editReport(this.state));
		}
	}

	render() {
		let redir = null;
        if (this.props.reqStatus) {
			redir = <Redirect to={`${adminUrl}${reportsUrl}`} />;
        }

		return (
			<div>
				<form className="admin-users-form" onSubmit={this.onHandleSubmit}>
					<TextField margin="normal" error={!this.state.valid.title && this.state.title !== ''}
						variant="outlined" label="Title" style={{ width: 300 }} className='asterisk' value={this.state.title} onChange={this.onHandleChange} name='title' />
					<FormErrors errorValue={this.state.formErrors.title} />

					<label label="Text" htmlFor="raised-button-file">
                                        <Button raised='true' component="span" className='asterisk button-margin' variant={'outlined'} value={this.state.text}>
                                            Upload report
                                        </Button>
                                    </label>
                                        <input
                                            name='text'
                                            onChange={this.onInputReport}
                                            accept= "application/pdf, .doc, .docx"
                                            className='hidden'
                                            id="raised-button-file"
                                            multiple={true}
											type="file"
                                        />
					<FormErrors errorValue={this.state.formErrors.text} />

					<TextField error={!this.state.valid.creation_date && this.state.creation_date !== ''} label="Date" className='asterisk' style={{ width: 300 }} margin="normal"
						variant="outlined" value={this.state.creation_date} onChange={this.onHandleChange} name='creation_date' />
					<FormErrors fieldName="Date" errorValue={this.state.formErrors.creation_date} />

                    <ButtonText type="submit" disabled={!this.state.valid.creation_date || !this.state.valid.title || !this.state.valid.text} propsValue="Submit" />
					<FillToSubmit show={this.state.valid.creation_date && this.state.valid.title && this.state.valid.text} value='Fill the required fields to submit'/>
				</form>
				{redir}
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return { 
		reports: state.adminReportsListReducer.reports, 
		reqStatus: state.adminReportsListReducer.reqStatus 
	}
}

export default connect(mapStateToProps)(AdminReportsForm)

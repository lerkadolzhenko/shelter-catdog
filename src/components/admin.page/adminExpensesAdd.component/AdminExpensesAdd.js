import React from 'react';
import { connect } from "react-redux";
import { InputText } from "../../share.components/inputText/inputText";
import './AdminExpensesAdd.scss'
import { addExpense } from "../adminExpensesList.component/actionCreators/actionCreators";
import { InputSelect } from "../../share.components/inputSelect/InputSelect";
import Grid from "@material-ui/core/Grid";
import { ButtonText } from "../../share.components/buttonText/ButtonText";
import { Redirect } from "react-router-dom";
import { adminUrl, expenseUrl } from "../../../config/apiUrl";
// import FormControlLabel from "@material-ui/core/FormControlLabel";
// import { validateField } from "../../sign.component/validator";
// import * as validate from "../../sign.component/validator"
// import { FillToSubmit } from "../../share.components/fillToSubmit/fillToSubmit";

const selects = {
    types: ['Rent', 'Food', 'Medicine', 'Workers'],
};
let isNew = true;

class AdminExpensesAdd extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            date: '',
            type: '',
            cash_amount: '',
        }
    }
    // errorInfo: {
    //     name: '',
    //     type: '',
    //     amount: '',
    // },
    // valid: {
    //     date: false,
    //     type: false,
    //     amount: false
    // },
    // formValid: false,


    componentDidUpdate(prevProps, prevState, snapshot) {
        isNew = true;
        //     if (this.props.expense && (this.state.expense_id !== this.props.expense.id)) {
        //         this.setState({
        //             expense_id: this.props.expense.id
        //         })
        //     }

    }

    // onInputData = event => {
    //     let { name, value } = event.target;
    //     let validator = validateField(name, value);

    //     this.setState({ ...this.state, [name]: value, errorInfo: { ...this.state.errorInfo, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] } }, () => {
    //         const formValidator = validate.validateExpenseForm(this.state.valid);
    //         this.setState({ ...this.state, formValid: formValidator });
    //     });

    // };


    onHandleChange = event => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });

    }

    onHandleSubmit = (event) => {
        isNew = false;
        event.preventDefault();
        this.props.dispatch(addExpense(this.state));
    };
    // static isError(value) {
    //     return value !== '';
    // }


    render() {
        // const stateKeys = Object.keys(this.state);

        let redirect = null;
        if (!isNew) {
            redirect = <Redirect to={`${adminUrl}${expenseUrl}`} />;
        }


        // if (this.props.snackbarToggle) {
        //     return <Redirect to={`${adminUrl}${expenseUrl}`} />;
        // }

        return (

            <div>
                <form className="admin-expenses-add" onSubmit={this.onHandleSubmit}>
                    <Grid container spacing={24} >
                        <Grid item xs={2}>
                            <InputText label="Date" value={this.state.date} handleChange={this.onHandleChange} name='date' className='input' />
                        </Grid>
                        <Grid item xs={2}>
                            <InputSelect
                                className='input'
                                label="Type"
                                value={this.state.type}
                                items={selects.types}
                                name='type'
                                handleChange={this.onHandleChange}
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <InputText label="Cash amount" value={this.state.cash_amount} handleChange={this.onHandleChange} name='cash_amount' className='input' />
                        </Grid>
                        {/* <Grid item xs={2}> */}
                        {/* </Grid> */}
                    </Grid>
                    <ButtonText className='button' type="submit" propsValue="Submit" horizontal='center' />
                </form>
                {redirect}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        snackbarToggle: state.snackbarReducer.snackbarToggleStatus,
        user: state.sign.user,
    }
};

export default connect(mapStateToProps)(AdminExpensesAdd)
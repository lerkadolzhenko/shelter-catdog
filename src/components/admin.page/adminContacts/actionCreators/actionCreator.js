import axios from "axios";
import { apiUrl , contactsUrl} from "../../../../config/apiUrl";
import {GETTING_CONTACTS, EDIT_CONTACT, SNACKBAR_TOGGLE, SNACKBAR_RESET} from "../../../../constants/action-types";


export const getContacts = () => {
    return(dispatch) =>{//action for zapros
    return axios//zapros
    .get(`${apiUrl}${contactsUrl}`)
    .then(response => response.data)
    .then(data=>dispatch({type:GETTING_CONTACTS, payload: data}))
    }
}

export const editContacts = (payload) => {
	return (dispatch) => {
		return axios
			.put(`${apiUrl}${contactsUrl}?id=${payload.id}`, payload)
			.then(response => response.status === 200)
			.then(() => {
				dispatch({type: EDIT_CONTACT, payload: payload})
			})
			.then(() => dispatch({type: SNACKBAR_RESET}))
			.then(dispatch({type: SNACKBAR_TOGGLE}))
	}
}

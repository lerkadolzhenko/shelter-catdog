import React from 'react';
import { connect } from 'react-redux';
import { getContacts, editContacts } from './actionCreators/actionCreator';
import { InputText } from '../../share.components/inputText/inputText';
import { ButtonText } from '../../share.components/buttonText/ButtonText';
import SnackbarComponent from "../../share.components/snackbar/snackbar";
import './adminContacts.scss'

class AdminContacts extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			shelter_phone: '',
			shelter_email: '',
			shelter_address: '',
			shelter_map_link: '',
			id: 1
		}
	}

	componentDidMount() {
		this.props.dispatch(getContacts(1));
	}

	componentDidUpdate() {
		// console.log(this.props.snackbarToggle);
		if (!this.state.shelter_map_link && this.props.contacts[0]) {
			this.setState({
				...this.state,
				shelter_phone: this.props.contacts[0].shelter_phone,
				shelter_email: this.props.contacts[0].shelter_email,
				shelter_address: this.props.contacts[0].shelter_address,
				shelter_map_link: this.props.contacts[0].shelter_map_link,
			},
			 () => {
				// console.log(this.state);

			})
		}
	}

	onInputData = event => {
		let name = event.target.name;
		let value = event.target.value;
		this.setState({ [name]: value });
	};

	onHandleSubmit = (event) => {
		event.preventDefault();
		this.props.dispatch(editContacts(this.state));
	};

	render() {
		let snackbar = null;
        if (this.props.snackbarToggle) {
            snackbar = <SnackbarComponent message='Contacts updated!' />;
        }
		// console.log(this.props);

		return (
			<div className='formAlign'>
				<form className="admin-contacts-form" onSubmit={this.onHandleSubmit}>
					<InputText label="Address" className='contacts_input' value={this.state.shelter_address} handleChange={this.onInputData} name='shelter_address' />
					<InputText label="Phone" className='contacts_input' value={this.state.shelter_phone} handleChange={this.onInputData} name='shelter_phone' />
					<InputText label="E-mail" className='contacts_input' value={this.state.shelter_email} handleChange={this.onInputData} name='shelter_email' />
					<InputText label="Map Link" className='contacts_input' value={this.state.shelter_map_link} handleChange={this.onInputData} name='shelter_map_link' />
					<ButtonText type="submit"  propsValue="Submit" />
				</form>
				{snackbar}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		contacts: state.getContactsReducer.contacts,
		snackbarToggle: state.snackbarReducer.snackbarToggleStatus
	}
}
export default connect(mapStateToProps)(AdminContacts);

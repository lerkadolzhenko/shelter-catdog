import { GETTING_CONTACTS, EDIT_CONTACT } from "../../../../constants/action-types";

const initialState = {
	contacts: [], //massiv with contacts
};
export const getContactsReducer = (state = initialState, action) => {
	switch (action.type) {

		case GETTING_CONTACTS:
			return { ...state, contacts: action.payload };
		case EDIT_CONTACT:
			return {
				...state, contacts: state.allAnimals.map((contact) =>
				contact.id === action.payload.id ? {...action.payload} : contact)
				};

		default:
			return state;
	}
}

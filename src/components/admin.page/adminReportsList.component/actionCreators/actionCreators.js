import axios from "axios";
import { apiUrl, reportsUrl} from "../../../../config/apiUrl";
import { EDIT_REPORT, SNACKBAR_TOGGLE, DELETE_REPORT, REPORTS_LOADED, CREATE_REPORT, SET_DEFAULT_REPORT_STATUS, UPLOAD_REPORT } from "../../../../constants/action-types";

export function getReports() {
	return function (dispatch) {
		return axios
			.get(`${apiUrl}${reportsUrl}`)
			.then(response => response.data)
			.then(json => {
				dispatch({type: REPORTS_LOADED, payload: json});
				dispatch({type: SET_DEFAULT_REPORT_STATUS})
			});
	};
}

export const createReport = (payload) => {
	return (dispatch) => {
		return axios
			.post(`${apiUrl}${reportsUrl}`, payload)
			.then(response => response.status === 200)
			.then(() => {
				dispatch({type: CREATE_REPORT, payload: payload})
			}).then(() => dispatch({type: SNACKBAR_TOGGLE}))
	}
}

export const uploadReport = (bodyFormData) => {
    return  (dispatch) => {
        return axios({
                        method: 'post',
                        url: `${apiUrl}${reportsUrl}/upload`,
                        data: bodyFormData,
                        config: { headers: { 'Content-Type': 'multipart/form-data'}}
                    })
            .then((res) => {
                dispatch({type: UPLOAD_REPORT, payload: res})
            })
            .catch(res => console.error(res))

    }
};

export const downloadReport = (name) => {
    return  (dispatch) => {
		return axios({
			url: `${apiUrl}${reportsUrl}/download/${name}`,
			method: 'GET',
			responseType: 'blob',
		  }).then((response) => {
			 const url = window.URL.createObjectURL(new Blob([response.data]));
			 const link = document.createElement('a');
			 link.href = url;
			 link.setAttribute('download', name);
			 document.body.appendChild(link);
			 link.click();
		  });
    }
};

export const editReport = (payload) => {
	return (dispatch) => {
		return axios
			.put(`${apiUrl}${reportsUrl}?id=${payload.id}`, payload)
			.then(response => response.status === 200)
			.then(() => {
				dispatch({type: EDIT_REPORT, payload: payload})
			}).then(() => dispatch({type: SNACKBAR_TOGGLE}))
	}
}

export const deleteReport = (payload) => {
	return (dispatch) => {
		return axios
			.delete(`${apiUrl}${reportsUrl}?id=${payload}`)
			.then(response => response.status === 200 && response.data === ''
				? dispatch({type: DELETE_REPORT, payload: payload})
				: dispatch({type: SNACKBAR_TOGGLE}))

	}
}

export default {getReports, downloadReport}
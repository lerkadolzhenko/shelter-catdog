import {CREATE_REPORT, EDIT_REPORT, REPORTS_LOADED, DELETE_REPORT, SET_DEFAULT_REPORT_STATUS, UPLOAD_REPORT } from "../../../../constants/action-types";

const initialState = {
	reports: [],
	reqStatus: false
};
export const adminReportsListReducer = (state = initialState, action) => {
	switch (action.type) {
		case REPORTS_LOADED:
			return { ...state, reports: action.payload };

		case CREATE_REPORT:
			return { ...state, reports: action.payload, reqStatus: true };

		case SET_DEFAULT_REPORT_STATUS: 
			return { ...state, reqStatus: false };

		case EDIT_REPORT:
			return { ...state, reqStatus: true};

		case DELETE_REPORT:
			return {...state, reports: state.reports.filter((item) => item.id !== action.payload), reqStatus: true};	
		
		case UPLOAD_REPORT:
			return { ...state, reports: action.payload };

		default:
			return state;
	}
}

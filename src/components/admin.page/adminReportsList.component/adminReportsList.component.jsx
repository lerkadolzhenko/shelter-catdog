import React from 'react';
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Paper from "@material-ui/core/Paper";
import { ButtonIcon } from "../../share.components/buttonIcon/ButtonIcon";
import { Link } from "react-router-dom";
import { deleteReport, getReports, downloadReport } from "./actionCreators/actionCreators";
import Icon from "@material-ui/core/Icon";
import TableBody from "@material-ui/core/TableBody";
import CssBaseline from "@material-ui/core/CssBaseline";
import { InputText } from "../../share.components/inputText/inputText";
import AdminDialog from "../../share.components/dialog/dialog";

import "./adminReportsList.scss"
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';


const styles = theme => ({
	root: {
		width: '100%',
		marginTop: theme.spacing.unit * 10,
		overflowX: 'auto',
		display: 'flex',
		flexDirection: "column",

	}
});

const THNames = [ 'Title', 'Date of creation'];

class AdminReportsListComponent extends React.Component {
	state = {
		reportsItems: null,
		searchValue: '',
		openDialog: false,
		id: 0,
	}

	componentDidMount() {
		this.props.dispatch(getReports())
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if (this.props.reports !== prevProps.reports) {
			this.getReportsData();
		}
		if (this.state.searchValue !== prevState.searchValue) {
			this.setState({
				reportsItems: this.props.reports.filter((item) => item.title.toLowerCase().includes(this.state.searchValue.toLowerCase()))
			})
			if (this.state.searchValue === '') {
				this.getReportsData();
			}
		}
	}


	getReportsData = () => {
		this.setState({
			reportsItems: this.props.reports
		})
	}

	showDialog = () => {
		this.setState(prevState => ({
			openDialog: !prevState.openDialog
		}))
	}

	dialogButton = (title, action) => {
        return  {
            title: title,
            action: action
        }
	}

	submitFromModal = () => {
		this.showDialog();
		this.props.dispatch(deleteReport(this.state.id))
		this.props.dispatch(getReports())
	}

	onHandleChange = event => {
		let name = event.target.name;
		let value = event.target.value;
		this.setState({[name]: value});
	}

	render() {
		const {reportsItems} =this.state;
		const {classes} = this.props;

		let modal = null;

		let reportsList = null;

		if (reportsItems != null) {
			reportsList = reportsItems.map((report, index) => (
				<TableRow key={index} >
					<TableCell component="th" scope="row">
						{report.title}
					</TableCell>
					<TableCell component="th" scope="row">
						{report.creation_date}
					</TableCell>
					<TableCell align="right">
						<div >
							<Icon className='pointer icon-margin' onClick={() => {
								console.log(report.text)
								this.props.dispatch(downloadReport(report.text))
							}}>cloud_download</Icon>
							<Link to={this.props.match.path + '_edit/' + report.id}>
								<Icon className='pointer icon-margin'>edit</Icon>
							</Link>
							<DeleteOutlinedIcon className='pointer icon-margin' onClick={() => {
								this.showDialog();
								this.setState({id: report.id})
							}}/>


						</div>
					</TableCell>
				</TableRow>
			))
		}

		modal = (this.state.openDialog)? <AdminDialog buttons={[
			this.dialogButton('yes', this.submitFromModal), 
			this.dialogButton('no', this.showDialog)
		 ]}
		 title='Delete'
		 text={`Are you sure you want to remove this item?`}
		 state={this.state.openDialog}
	/> : null;



			return (
			<div className='mt5prs'>
				<div className="add-btn">
					<Link to={this.props.match.path + '_add'}>
						<ButtonIcon buttonTitle='Add' buttonIcon="add"/>
					</Link>
				</div>

				<InputText label="Search" className='searchBar' value={this.state.searchValue} handleChange={this.onHandleChange} name="searchValue"/>

				<Paper className="paper-margin">
					<CssBaseline/>
	
					<Table className={classes.table}>
						<TableHead className='tableHeader' >
							<TableRow>
								{THNames.map((name, index) => (
									<TableCell className='white'  key={index}>{name}</TableCell>
								))}
								<TableCell className='white'  align="right">Actions</TableCell>

							</TableRow>
						</TableHead>
						<TableBody>
							{reportsList}
						</TableBody>
					</Table>
	
				</Paper>
				{modal}
			</div>

		);
	}
}

const mapStateToProps = state => {
	return {
		reports: state.adminReportsListReducer.reports,
	}
}
export default connect(mapStateToProps)(withStyles(styles)(AdminReportsListComponent));


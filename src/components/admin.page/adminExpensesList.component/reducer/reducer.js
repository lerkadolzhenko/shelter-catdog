import { EXPENSES_LOADED, EDIT_EXPENSE, CREATE_EXPENSE } from "../../../../constants/action-types";


const initialState = {
    expenses: [],
    expenseAdded: false,
    expenseEdited: false,
};
export const adminExpensesListReducer = (state = initialState, action) => {
    switch (action.type) {

        case EXPENSES_LOADED:

            return {
                ...state, expenses: action.payload
            };

        case CREATE_EXPENSE:
            return { expense: action.payload, expenseEdited: false, expenseAdded: true };

        case EDIT_EXPENSE:
            return {
                ...state, expenses: state.expenses.map((expense) =>
                    expense.id === action.payload.id ? { ...action.payload } : expense)
            };

        default:
            return state;
    }
}

import axios from "axios";
import { EXPENSES_LOADED, EDIT_EXPENSE, SNACKBAR_TOGGLE, CREATE_EXPENSE, } from "../../../../constants/action-types";
import { expenseUrl, apiUrl } from "../../../../config/apiUrl";

export function getExpenses() {
    return function (dispatch) {
        return axios
            .get(`${apiUrl}${expenseUrl}`)
            .then(response => response.data)
            .then(json => {
                dispatch({ type: EXPENSES_LOADED, payload: json });
            });
    };
}

export const addExpense = (data) => {
    return (dispatch) => {
        return axios
            .post(`${apiUrl}${expenseUrl}`, data)
            .then(res => res.status === 200)
            .then(() => {
                dispatch({ type: CREATE_EXPENSE, payload: data })
            })
            .then(() => dispatch({ type: SNACKBAR_TOGGLE }));
    }
};

export const editExpense = (payload) => {
    return (dispatch) => {
        return axios
            .put(`${apiUrl}${expenseUrl}`, payload)
            .then(response => response.status === 200)
            .then(() => {
                dispatch({ type: EDIT_EXPENSE, payload: payload })
            }).then(() => dispatch({ type: SNACKBAR_TOGGLE }))
    }
}




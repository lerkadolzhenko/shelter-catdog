import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Icon from '@material-ui/core/Icon';
import AdminDialog from "../../share.components/dialog/dialog";
import { getExpenses } from './actionCreators/actionCreators';
import { ButtonIcon } from '../../share.components/buttonIcon/ButtonIcon';
import SnackbarComponent from '../../share.components/snackbar/snackbar';


import './adminExpensesList.scss'

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 5,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

class AdminExpensesList extends React.Component {
    state = {
        openDialog: false,
        id: 0,
        expensesList: null
    }

    componentDidMount() {
        this.props.dispatch(getExpenses())

    }

    componentDidUpdate(prevProps) {

        if (this.props.expenses !== prevProps.expenses) {
            this.getExpenseData();
        }
    }

    getExpenseData = () => {
        this.setState({
            expensesList: this.props.expenses
        })
    }

    showDialog = () => {
        this.setState(prevState => ({
            openDialog: !prevState.openDialog
        }))
    }

    onHandleChange = event => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
    }

    submitFromModal = () => {
        this.showDialog();
    }


    render() {
        const { expensesList } = this.state;
        let modal = null;
        let snackbar = null;
        let tablesItems = null;


        if (expensesList !== null) {
            tablesItems = expensesList.map((expenseItem, index) => (
                <TableRow key={index}>
                    <TableCell component="th" scope="row">
                        {expenseItem.date}
                    </TableCell>
                    <TableCell component="th" scope="row">
                        {expenseItem.type}
                    </TableCell>
                    <TableCell component="th" scope="row">
                        {expenseItem.cash_amount}
                    </TableCell>
                    <TableCell align="right">
                        <div>
                            <Link to={this.props.match.path + '_edit/' + expenseItem.id}>
                                <Icon>edit</Icon>
                            </Link>
                        </div>
                    </TableCell>
                </TableRow>
            ))
        }

        if (this.state.openDialog) {
            modal =
                <AdminDialog showToggle={this.showDialog} state={this.state.openDialog} />;
        }
        if (this.props.snackbarToggle) {
            snackbar = <SnackbarComponent message='Expense updated!' />;
        }

        return (

            <div className='expenses-list'>

                <div className="add-btn">
                    <Link to={this.props.match.path + '_add/'}>
                        <ButtonIcon buttonTitle='Add' buttonIcon="add" />
                    </Link>
                </div>

                <Paper>
                    <CssBaseline />

                    <Table >
                        <TableHead className='tableHeader' >
                            <TableRow>
                                <TableCell className='white' >Date</TableCell>
                                <TableCell className='white' >Type</TableCell>
                                <TableCell className='white' >Amount</TableCell>
                                <TableCell className='white'  align="right">Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tablesItems}
                        </TableBody>
                    </Table>
                    {snackbar}
                    {modal}
                </Paper>

            </div>

        );
    }
}


const mapStateToProps = state => {
    return {
        expenses: state.adminExpensesListReducer.expenses,
        snackbarToggle: state.snackbarReducer.snackbarToggleStatus
    }
}

export default connect(mapStateToProps)(withStyles(styles)(AdminExpensesList));

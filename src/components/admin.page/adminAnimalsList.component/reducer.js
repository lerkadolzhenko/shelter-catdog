import {
    CREATE_ANIMAL,
    DELETE_ANIMAL,
    EDIT_ANIMAL_INFO,
    GET_ANIMALS_LIST,
    GET_ONE_ANIMAL, UPLOAD_IMG
} from "../../../constants/action-types";
import {animalImg, apiUrl} from "../../../config/apiUrl";

const initialState = {
    allAnimals: [],
    allPets: [],
    animal: {},
    img: null,
    redirect: false,
    animalAdded: false,
    animalEdited: false,
    animalDeleted: false

};

export const  adminAnimalListReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ANIMALS_LIST:
            return { allAnimals: action.payload, allPets: action.payload, img: null};

        case CREATE_ANIMAL:
            return {animal: action.payload, animalDeleted: false, animalEdited: false, animalAdded: true, redirect: true};

        case UPLOAD_IMG:
            return  { img: apiUrl+animalImg+'?link='+action.payload.data.message};

        case EDIT_ANIMAL_INFO:
            return {  animalDeleted: false, animalEdited: true, animalAdded: false, redirect: true };

        case GET_ONE_ANIMAL:
            return {allPets: action.payload};

        case DELETE_ANIMAL:
            return {animalDeleted: true, animalEdited: false, animalAdded: false };

        default: return state;
    }



};
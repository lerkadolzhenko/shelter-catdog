import axios from 'axios';
import {apiUrl, animalFull, animalImgUpload, animalImg} from "../../../config/apiUrl";
import {
    EDIT_ANIMAL_INFO,
    GET_ANIMALS_LIST,
    SNACKBAR_TOGGLE,
    CREATE_ANIMAL,
    DELETE_ANIMAL, SNACKBAR_RESET, UPLOAD_IMG
} from "../../../constants/action-types";

export function getAnimalsToAdmin() {
    return function (dispatch) {
        return axios
            .get(`${apiUrl}${animalFull}`)
            .then(res => res.data)
            .then(res => {dispatch({type: GET_ANIMALS_LIST, payload: res})
            });
    }
}
export const addAnimal = (data) => {
    return  (dispatch) => {
        return axios
            .post(`${apiUrl}${animalFull}`, data)
            .then(res => res.status === 200)
            .then(() => {
                dispatch({type: CREATE_ANIMAL, payload: data})
            })
            .then(() => dispatch({type: SNACKBAR_RESET}))
            .then(() => dispatch({type: SNACKBAR_TOGGLE}));
    }
};



export const uploadImg = (bodyFormData) => {
    return  (dispatch) => {
        return axios({
                        method: 'post',
                        url: `${apiUrl}${animalImgUpload}`,
                        data: bodyFormData,
                        config: { headers: { 'Content-Type': 'multipart/form-data'}}
                    })
            .then((res) => {
                dispatch({type: UPLOAD_IMG, payload: res})
            })
            .catch(res => console.error(res))

    }
};

export const deleteImg = (imgName) => {
    return (dispatch) => {
        return axios

            .delete(`${apiUrl}${animalImg}?fileToDelete=${imgName}`)
            .then(response => response.status === 200)
    }
};

export const editAnimalInfo = (payload) => {
    return (dispatch) => {
        return axios
            .put(`${apiUrl}${animalFull}?id=${payload.animal_id}`, payload)
            .then(response => response.status === 200)
            .then(() => {
                dispatch({type: EDIT_ANIMAL_INFO, payload: payload})
            })
            .then(() => dispatch({type: SNACKBAR_RESET}))
            .then(() => dispatch({type: SNACKBAR_TOGGLE}))
    }
};

export const deleteAnimal = (payload) => {
    return (dispatch) => {
        return axios
            .delete(`${apiUrl}${animalFull}?id=${payload}`)
            .then(response => response.status === 200 && response.data === '',
                 dispatch({type: DELETE_ANIMAL, payload: payload}))
            .then(() => dispatch({type: SNACKBAR_RESET}))
            .then(() => dispatch({type: SNACKBAR_TOGGLE}))
    }
};


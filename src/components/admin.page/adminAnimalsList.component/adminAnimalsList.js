import React, {Component} from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import './adminAnimalsList.scss'
import {deleteAnimal, getAnimalsToAdmin} from "./actionCreators";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import CssBaseline from "@material-ui/core/CssBaseline";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import Icon from '@material-ui/core/Icon';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

import {ButtonIcon} from "../../share.components/buttonIcon/ButtonIcon";
import SnackbarComponent from "../../share.components/snackbar/snackbar";
import AdminDialog from "../../share.components/dialog/dialog";
const THNames = [ 'Name', 'Kind', 'Sex', 'Castrated', 'Health status', 'Adopted', 'Birth date', 'Arrival_date', 'Actions'];

class AdminAnimalsList extends Component {

    state = {
        animalAdded: this.props.animalAdded,
        animalEdited: this.props.animalEdited,
        openDialog: false,
        id: 0,
    };

    componentDidMount() {
        this.props.dispatch(getAnimalsToAdmin())
    }

    showDialog = () => {
        this.setState(prevState => ({
            openDialog: !prevState.openDialog
        }))
    };
    dialogButton = (title, action) => {
        return  {
            title: title,
            action: action
        }
    };

    deleteAnimal = () => {
        this.props.dispatch(deleteAnimal(this.state.id));
        this.showDialog();
        this.setState({
            animalDeleted: true,
            animalAdded: false,
            animalEdited: false,
        });
        setTimeout(() => {
            this.props.dispatch(getAnimalsToAdmin());
        }, 50);
    }

    render() {
        const { animals } = this.props;
        const { openDialog } = this.state;
        let animalsList = null;


        const modal = (openDialog)? <AdminDialog buttons={[this.dialogButton('yes', this.deleteAnimal), this.dialogButton('no', this.showDialog)]}
                                                 title='Delete'
                                                 text={`Are you sure you want to remove this item?`}
                                                 state={openDialog}
        /> : null;
        
    if (animals) {
        animalsList = animals.map((animal, index) => (
            <TableRow  key={index} >
                <TableCell component="th"    scope="row">
                    {animal.name}
                </TableCell>
                <TableCell component="th"   scope="row">
                    {animal.kind}
                </TableCell>
                <TableCell component="th"    scope="row">
                    {animal.is_boy.toString()}
                </TableCell>
                <TableCell component="th"   scope="row">
                    {animal.is_sterilized.toString()}
                </TableCell>
                <TableCell   component="th" scope="row">
                    {animal.health_status}
                </TableCell >
                <TableCell component="th"   scope="row">
                    {animal.is_adopted.toString()}
                </TableCell>
                <TableCell component="th"  scope="row">
                    {animal.birth_date.slice(0, 10)}
                </TableCell>
                <TableCell component="th"    scope="row">
                    {animal.arrival_date.slice(0, 10)}
                </TableCell>
                <TableCell  align={"center"}>
                    <div>
                        <Link to={this.props.match.path + '_edit/' + animal.animal_id}>
                            <Icon>edit</Icon>
                        </Link>
                            <DeleteOutlinedIcon className='pointer' onClick={() => {
                                this.showDialog();
                                this.setState({id: animal.animal_id})
                                // this.deleteAnimal(animal.animal_id);
                            }}/>
                    </div>
                </TableCell>

            </TableRow>
        ))
    }
    let snackbar = null;
        if (this.props.snackbarToggle && this.state.animalAdded) {
            snackbar = <SnackbarComponent message='Added'/>;
        } else if (this.props.snackbarToggle && this.state.animalEdited) {
            snackbar = <SnackbarComponent message='Updated' />;
        } else if (this.props.snackbarToggle && this.state.animalDeleted) {
            snackbar = <SnackbarComponent message='Deleted' />;
        }

    return(

        <div className='mt5prs'>
            <div className="add-btn">
                <Link to={this.props.match.path + '_add/'}>
                    <ButtonIcon buttonTitle='Add' buttonIcon="add"/>
                </Link>
            </div>
            <Paper >
                <CssBaseline/>

                <Table padding={'checkbox'}>
                    <TableHead className='tableHeader  ' >
                        <TableRow  >
                            {THNames.map((name, index) => (
                                <TableCell className='white14 ' key={index}>{name}</TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {animalsList}
                    </TableBody>
                </Table>
                {modal}
            </Paper>
            {snackbar}
        </div>
    )
}
}

const mapStateToProps = state => {
    return {
        animals: state.adminAnimalListReducer.allAnimals,
        snackbarToggle: state.snackbarReducer.snackbarToggleStatus,
        animalAdded: state.adminAnimalListReducer.animalAdded,
        animalEdited: state.adminAnimalListReducer.animalEdited,
        animalDeleted: state.adminAnimalListReducer.animalDeleted
    }
};

export default connect(mapStateToProps)(AdminAnimalsList);

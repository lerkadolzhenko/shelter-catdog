import React from 'react';
import { InputText } from "../../share.components/inputText/inputText";
import { connect } from "react-redux";
import { editExpense } from "../adminExpensesList.component/actionCreators/actionCreators";
import { ButtonText } from "../../share.components/buttonText/ButtonText";
import { Redirect } from "react-router-dom";
import { adminUrl, expenseUrl } from '../../../config/apiUrl.js';

import './adminExpensesForm.scss';
import { InputSelect } from '../../share.components/inputSelect/InputSelect';



const selects = {
    types: ['Rent', 'Food', 'Medicine', 'Workers'],
};


class AdminExpensesForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            date: '',
            type: '',
            cash_amount: '',
        }
    }


    componentDidMount() {
        const id = Number(this.props.match.params.id);
        const expense = this.props.expenses.find(expense => expense.id === id);
        this.setState({
            date: expense.date,
            type: expense.type,
            cash_amount: expense.cash_amount,
            id: expense.id
        })
    }

    onHandleChange = event => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });

    }

    onHandleSubmit = (event) => {
        event.preventDefault();
        this.props.dispatch(editExpense(this.state));

    }

    render() {
        if (this.props.snackbarToggle) {
            return <Redirect to={`${adminUrl}${expenseUrl}`} />;
        }
        return (
            <div>
                <form className="admin-expenses-form" onSubmit={this.onHandleSubmit}>
                    <div className='admin-expenses-inputs'>
                            <InputText label="Date" value={this.state.date} handleChange={this.onHandleChange} name='date' className='input' />
                       
                            <InputSelect
                                className='input'
                                label="Type"
                                value={this.state.type}
                                items={selects.types}
                                name='type'
                                handleChange={this.onHandleChange}
                            />
                       
                            <InputText label="Cash amount" value={this.state.cash_amount} handleChange={this.onHandleChange} name='cash_amount' className='input' />
                      </div>
                        {/* <Grid item xs={2}> */}
                        {/* </Grid> */}
                  
                    <ButtonText type="submit" className='button' propsValue="Submit" horizontal='center' />
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        expenses: state.adminExpensesListReducer.expenses,
        snackbarToggle: state.snackbarReducer.snackbarToggleStatus
    }
}

export default connect(mapStateToProps)(AdminExpensesForm);

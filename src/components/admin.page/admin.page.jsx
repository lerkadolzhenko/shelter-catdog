import React, { Fragment } from 'react';
import { Link, Route } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AdminUsersList from "./adminUsersList.component/adminUsersList.component"
import AdminUsersForm from "./adminUsersForm.component/adminUsersForm.component";
import AdminReportsList from "./adminReportsList.component/adminReportsList.component";
import AdminSosList from './adminSosList.component/adminSosList.component'
import AdminSosListForm from './adminSosList.component/form.component'
import * as actionCreators from "../sign.component/action-creator";
import './admin.page.scss'
import AdminReportsForm from "./adminReportsForm.component/adminReportsForm.component";
import AdminVolunteersPhotos from './adminVolunteersPhotos.component'
import AdminExpensesForm from './adminExpensesForm.component/adminExpensesForm.component';
import AdminExpensesList from './adminExpensesList.component/adminExpensesList.component';
import { connect } from 'react-redux';
import AdminAnimalsList from "./adminAnimalsList.component/adminAnimalsList"
import AdminAnimalsForm from "./adminAnimalsEdit.component/adminAnimalsEdit"
import AdminAnimalsAdd from "./adminAnimalsAdd.component/adminAnimalsAdd"
import AdminContacts from './adminContacts/adminContacts';
import { Redirect } from "react-router-dom";
// import NothingToShow from '../share.components/nothingToShow.component'

import './admin.page.scss';
import ModalLoader from '../share.components/loader/loader';
import AdminExpensesAdd from './adminExpensesAdd.component/AdminExpensesAdd';


function mapDispatchToProps(dispatch) {
    return {
        addAction: payload => dispatch(payload)
    }
}

function mapStateToProps(state) {
    return {
        user: state.sign.user,
        isLoading: state.sign.isLoading,
        isLogged: state.sign.isLogged
    }
}

const drawerWidth = 240;

const menuItems = ['Animals', 'Users', 'The shelter needs', 'Reports', 'Expenses', 'Contacts', 'Volunteers photos'];
const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
    },
});


class AdminPage extends React.Component {

    state = {
        mobileOpen: false,
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    componentDidMount() {
        this.props.addAction(actionCreators.isLogged(""));
    }
    
    clicked(item){
    	this.setState({
			active: item
		})
	}


    render() {
        const { theme, classes } = this.props;
        const drawer = (
            <div>
                <div className={classes.toolbar} />
                <Divider />
                <List>
                    {menuItems.map((item, index) => {
                        if (item === this.state.active) {
                            return (
                                <Link to={"/admin/" + item} key={index}>
                                    <ListItem className='bgclr' onClick={() => this.clicked(item)} button key={index}>
                                        <Typography className='menuItemText white'>{item}</Typography>
                                    </ListItem>
                                </Link>
                            )
                        } else {
                            return (
                                <Link to={"/admin/" + item} key={index}>
                                    <ListItem onClick={() => this.clicked(item)} button key={index}>

                                        <Typography className='menuItemText'>{item}</Typography>

                                    </ListItem>
                                </Link>
                            )
                        }


                    })}
                </List>
            </div>
        );
        let render = <ModalLoader></ModalLoader>
        if (!this.props.isLoading && this.props.isLogged && (this.props.user.role === "admin")) {
            render = <div className={classes.root}>
                <CssBaseline />
                <AppBar className={classes.appBar}>
                    <Toolbar className='toolbar'>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" noWrap>
                            {this.props.location.pathname}
                        </Typography>

                    </Toolbar>
                </AppBar>
                <nav className={classes.drawer}>
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={this.props.container}
                            variant="temporary"
                            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                            open={this.state.mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
                <main className={classes.content}>
                    <Route path={'/admin/animals'} render={({ ...props }) => <AdminAnimalsList {...props} />} />
                    <Route path={'/admin/animals_add'} render={({ ...props }) => <AdminAnimalsAdd {...props} />} />
                    <Route path={'/admin/animals_edit/:id'} render={({ ...props }) => <AdminAnimalsForm {...props} />} />
                    <Route path={'/admin/users'} render={({ ...props }) => <AdminUsersList {...props} />} />
                    <Route path={'/admin/users_edit/:id'} render={({ ...props }) => <AdminUsersForm {...props} />} />
                    <Route path={'/admin/reports'} render={({ ...props }) => <AdminReportsList {...props} />} />
                    <Route path={'/admin/reports_edit/:id'} render={({ ...props }) => <AdminReportsForm {...props} />} />
                    <Route path={'/admin/reports_add'} render={({ ...props }) => <AdminReportsForm type='add' {...props} />} />
                    <Route path={'/admin/the shelter needs'} render={({ ...props }) => <AdminSosList {...props} />} />
                    <Route path={'/admin/the shelter needs_edit/:id'} render={({ ...props }) => <AdminSosListForm {...props} />} />
                    <Route path={'/admin/the shelter needs_add'} render={({ ...props }) => <AdminSosListForm type='add' {...props} />} />
                    <Route path={'/admin/contacts'} render={({ ...props }) => <AdminContacts {...props} />} />
                    <Route path={'/admin/exnpenses'} render={({ ...props }) => <AdminExpensesList {...props} />} />
                    <Route path={'/admin/expenses_edit/:id'} render={({ ...props }) => <AdminExpensesForm {...props} />} />
                    <Route path={'/admin/expenses_add'} render={({ ...props }) => <AdminExpensesAdd {...props} />} />
                    <Route path={'/admin/volunteers photos'} render={({ ...props }) => <AdminVolunteersPhotos {...props} />} />
                </main>
            </div>;
        }
        if (!this.props.isLoading && (!this.props.isLogged || (this.props.user.role !== "admin"))) {
            render = <Redirect to={``} />;
        }

        return (
            <Fragment>
                {render}
            </Fragment>
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(AdminPage));

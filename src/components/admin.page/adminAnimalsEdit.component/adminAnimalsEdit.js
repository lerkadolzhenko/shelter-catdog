import React, { Component } from 'react';
import { connect } from "react-redux";
import { InputText } from "../../share.components/inputText/inputText";
import './adminAnimalsEdit.scss'
import { InputSelect } from "../../share.components/inputSelect/InputSelect";
import { ButtonText } from "../../share.components/buttonText/ButtonText";
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import Grid from "@material-ui/core/Grid";
import {deleteImg, editAnimalInfo, getAnimalsToAdmin, uploadImg} from "../adminAnimalsList.component/actionCreators";
import { Redirect } from "react-router-dom";
import { adminUrl, animalsUrl } from "../../../config/apiUrl";
import {validateField} from "../../sign.component/validator";
import * as validate from "../../sign.component/validator";
import {Switch, TextField} from "@material-ui/core";
import {FillToSubmit} from "../../share.components/fillToSubmit/fillToSubmit";
import Icon from "@material-ui/core/Icon";
import {ButtonUploadImg} from "../../share.components/buttonUploadImg/buttonUploadImg";
import {FormErrors} from "../../share.components/formErrors.shared";
import {indigo} from "@material-ui/core/colors";
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    colorSwitchBase: {
        color: indigo[700],
        '& + $colorBar': {
            backgroundColor: indigo[500],
        },
        '&$colorChecked': {
            color: '#f50057',
            '& + $colorBar': {
                backgroundColor: '#ff4081',
            },
        },

    },
    colorBar: {},
    colorChecked: {},

});
const selects = {
    kind: ['dog', 'cat'],
    health_status: ['healthy', 'average', 'sos']
};
const labels = {
    name: 'Name',
    kind: 'Kind',
    arrival_date: 'Arrival date',
    birth_date: 'Birth date',
    health_status: 'Health status',
    is_adopted: ['Adopted','Not adopted'],
    is_sterilized: ['Sterilized','Not sterilized'],
    is_boy: ['Good Boy', 'Good Girl'],
    link: 'Link',
    alt: 'Image name',
    about: 'About',
};
// let animalsList =[];

class AdminAnimalsEdit extends Component {

    state = {
        animal_id: null,
        name: "",
        kind: "",
        birth_date: "",
        arrival_date: "",
        health_status: "",
        is_boy: true,
        is_sterilized: true,
        is_adopted: false,
        link: "",
        img: this.link,
        imgFile: null,
        fileToDelete: null,
        alt: '',
        about: '',
        shouldDelete: false,
        errorInfo: {
            name: '',
            kind: '',
            arrival_date: '',
            birth_date: '',
            health_status: '',
            link: '',
            alt: '',
            about: ''

        },
        valid: {
            name: true,
            kind: true,
            birth_date: true,
            arrival_date: true,
            health_status: true,
            link: true
        },
        formValid: true,

    };

    componentDidMount() {
        // animalsList = this.props.animalsList;
        const id = Number(this.props.match.params.id);

        if (this.props.animalsList.length > 0){
            const animal = this.props.animalsList.find(animal => animal.animal_id === id);
            this.setState({
                animal_id: animal.animal_id,
                link: animal.link,
                fileToDelete: animal.link.slice(51),
                alt: animal.alt,
                kind: animal.kind,
                name: animal.name,
                is_boy: animal.is_boy,
                arrival_date: animal.arrival_date,
                birth_date: animal.birth_date,
                is_sterilized: animal.is_sterilized,
                health_status: animal.health_status,
                is_adopted: animal.is_adopted,
                about: animal.about
            });
        }


    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.alt === '') {
            this.setState({
                alt: Math.random()*100000000
            })
        }
        if (this.props.img !== null && (this.props.img !== this.state.link)){
            this.setState({
                link: this.props.img
            })
        }

        if ((this.props.img) && (this.state.link !== this.props.img)) {
            this.setState({...this.state, link: this.props.img });

        }
        if (prevState.link !== this.state.link){
            this.setState({
                shouldDelete: true,
            });

        }



    }


    onInputData = event => {
        let {name, value } = event.target;
        let validator = validateField(name, value);

        this.setState({ ...this.state, [name]: value, errorInfo : {...this.state.errorInfo, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] }}, () => {
            const formValidator = validate.validateAnimalForm(this.state.valid);
            this.setState({ ...this.state, formValid: formValidator});
        });
        // console.log(this.state);
        // console.log(this.props)
        console.log(this.state.fileToDelete)
    };

    minDate = (name) => {
        if (this.state.arrival_date < this.state.birth_date){
            this.setState({
                arrival_date: this.state.birth_date
            })
        }
        if (name === 'arrival_date'){
            return this.state.birth_date
        } else return '2000-01-01 00:00:00.000 +00:00';

    };


    onInputImg = event => {

        let bodyFormData = new FormData();
        bodyFormData.append('animal', event.target.files[0]);
        // img = bodyFormData;

        const name = 'link';
        const value = event.target.files[0].name;
        let validator = validateField(name, value);
        this.setState({ ...this.state, imgFile: bodyFormData, link: URL.createObjectURL(event.target.files[0]),  errorInfo : {...this.state.errorInfo, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] }}, () => {
            const formValidator = validate.validateAnimalForm(this.state.valid);
            this.setState({ ...this.state, formValid: formValidator});
        });

    };

    onChangeCheckbox = event => {
        let {name, checked } = event.target;
        this.setState({ [name]: checked });
    };
    onHandleSubmit = (event) => {
        this.props.dispatch(getAnimalsToAdmin());
        event.preventDefault();
        if(this.state.shouldDelete){
            this.props.dispatch(deleteImg(this.state.fileToDelete))
        }
        if (this.state.imgFile){
            this.props.dispatch(uploadImg(this.state.imgFile));
        }

        setTimeout(() => {
            this.props.dispatch(editAnimalInfo(this.state));
        }, 100);
        this.props.dispatch(getAnimalsToAdmin())

    };

    static isError(value){
        return value !== '';
    }

    check = () => {
        if (this.state.fileToDelete) {
            console.log(`${this.state} 
                        yes`)
        } else console.log(`${this.state} 
                            no`)
    };

    render() {
        const {classes} = this.props;
        const stateKeys = Object.keys(this.state);
        let redirect = null;
        if (this.props.redirect || this.props.redirect === false) {

            redirect = <Redirect to={`${adminUrl}${animalsUrl}`} />;
        }

        return (
            <div className='mw70 mt5prs'>
                <form className="admin-users-form" onSubmit={this.onHandleSubmit}>
                    <Grid container spacing={24} >
                        {stateKeys.map((name, index) => {
                            if (name === 'animal_id'
                                || name === 'errorInfo'
                                || name === 'valid'
                                || name === 'formValid'
                                || name === 'fileToDelete'
                                || name === 'shouldDelete'
                                || name === 'imgFile'
                                || name === 'alt') {
                                return null
                            } else if (typeof this.state[name] === 'boolean') {
                                return (
                                    <Grid item xs={3} key={index}>


                                        <span className='responsiveText'>{labels[name][1]}</span>
                                        <Switch   className='fullWidth '
                                                  label={labels[name]}
                                                  name={name}
                                                  onChange={this.onChangeCheckbox}
                                                  checked={this.state[name]}
                                                  classes={{
                                                      switchBase: classes.colorSwitchBase,
                                                      checked: classes.colorChecked,
                                                      bar: classes.colorBar,
                                                  }}
                                        >

                                        </Switch>
                                        <span className='responsiveText'>{labels[name][0]}</span>

                                    </Grid>
                                )
                            } else if (name === 'birth_date' || name === 'arrival_date') {
                                return (
                                    <Grid item xs={3} key={index} className='negativeMarginBott30px'>

                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <DatePicker
                                                margin="normal"
                                                minDate={this.minDate(name)}
                                                label={labels[name]}
                                                value={this.state[name]}
                                                onChange={(date) => this.setState({[name]:date})}
                                                variant={"outlined"}
                                            />
                                            <Icon className='dateIcon'>date_range</Icon>
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                )

                            } else if (name === 'kind' || name === 'health_status') {
                                return (
                                    <Grid item xs={3} key={index}>
                                        <InputSelect
                                            className='fullWidth asterisk'
                                            label={labels[name]}
                                            value={this.state[name]}
                                            items={selects[name]}
                                            name={name}
                                            handleChange={this.onInputData}
                                        />
                                    </Grid>
                                )

                            }

                            else if (name === 'link') {
                                return (
                                        <Grid item xs={2} key={index} >
                                            <ButtonUploadImg name={name} onChange={this.onInputImg} textValue='Upload image'/>
                                            <FormErrors errorValue={this.state.errorInfo[name]} />

                                        </Grid>
                                )
                            }
                            else if(name === 'img'){
                                return (
                                    <Grid item xs={4} key={index} >
                                        <img  src={this.state.link} className='img100x100edit'  alt=""/>
                                    </Grid>
                                )

                            }
                            else if (name === 'about'){
                                return (
                                    <Grid item xs={12} key={index}>
                                        <TextField multiline={true} rows={6}  variant={"outlined"} className='fullWidth' label={labels[name]} value={this.state[name]} onChange={this.onInputData} name={name}/>
                                    </Grid>
                                )
                            }

                            else {
                                return (
                                    <Grid item xs={3} key={index}>
                                        <InputText className='fullWidth asterisk' label={labels[name]} value={this.state[name]} handleChange={this.onInputData} name={name}
                                                   errorValue={this.state.errorInfo[name]} error={AdminAnimalsEdit.isError(this.state.errorInfo[name])}/>

                                    </Grid>
                                )
                            }

                        })
                        }

                    </Grid>



                    <ButtonText   type="submit" propsValue='Submit'  disabled={!this.state.formValid}/>
                    <FillToSubmit show={this.state.formValid} value='Fill the required fields to submit'/>
                </form>
                <button onClick={this.check}>check</button>

                {redirect}
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        animalsList: state.adminAnimalListReducer.allAnimals,
        redirect: state.adminAnimalListReducer.redirect,
        img: state.adminAnimalListReducer.img
    }
}

export default connect(mapStateToProps)(withStyles(styles)(AdminAnimalsEdit));

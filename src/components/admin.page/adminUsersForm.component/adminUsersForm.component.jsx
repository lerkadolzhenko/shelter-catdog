import React from 'react';
import { InputText } from "../../share.components/inputText/inputText";
import { connect } from "react-redux";
import { editUser, eraseError } from "../adminUsersList.component/actionCreators/actionCreators";
import './adminUsersForm.scss'
import { ButtonText } from "../../share.components/buttonText/ButtonText";
import { InputSelect } from "../../share.components/inputSelect/InputSelect";
import { baseUsersRole } from "../../../config/baseUsersRole";
import { Redirect } from "react-router-dom";
import { adminUrl, usersUrl } from "../../../config/apiUrl";
import * as validate from "../../sign.component/validator";
import * as constructor from "../../sign.component/signConsts";
import Grid from '@material-ui/core/Grid';
import { FormErrors } from '../../share.components/formErrors.shared';
import * as CONSTANTS from "../../../constants/messages";

class AdminUsersForm extends React.Component {
    state = {
        username: '',
        fullName: '',
        email: '',
        password: '',
        role: '',
        phone: '',
        isUser: true,
        valid: {
            username: true,
            email: true,
            password: true,
            repeatPassword: true,
            fullName: true,
            avatar: true
        },
        touched: {
            username: false,
            email: false,
            password: false,
            repeatPassword: false,
            fullName: false,
        },
        formErrors: {
            username: '',
            email: '',
            password: '',
            repeatPassword: '',
            fullName: '',
        },
        formValid: false,
        error: '',
        isButtonDisabled: false
    }

    componentDidMount() {
        const id = Number(this.props.match.params.id);

        const user = this.props.users.find((user) => user.id === id);
        if (user) {
            this.setState({
                username: user.username,
                fullName: user.fullname,
                email: user.email,
                password: user.password,
                role: user.role,
                phone: user.phone,
                id: user.id,
                isUser: true,
                formValid: true
            })
        } else {
            this.setState({
                isUser: false
            })
        }
    }
    componentWillUnmount() {
        this.props.addAction(eraseError(''));
    }

    onFieldBlur = event => {
        const name = event.target.name;
        let value = event.target.value;
        this.setState({ ...this.state, touched: { ...this.state.touched, [name]: true } }, () => {
            let validator = validate.validateField(name, value, this.state.password);
            this.setState({ ...this.state, formErrors: {...this.state.formErrors, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] }}, () => {
            const formValidator = validate.validateForm(this.state.render, this.state.valid);
            this.setState({ ...this.state, formValid: formValidator});
            });
        });
    }

    componentDidUpdate(){
        if (this.props.error !== this.state.error && (this.props.error.length > 0)) {
            this.setState({...this.state, error: this.props.error, isButtonDisabled: true});
        }
    }
    componentDidCatch

    onHandleChange = event => {
        let name = event.target.name;
        let value = event.target.value;
        let validator = validate.validateField(name, value, this.state.password);
        this.setState({[name]: value, formErrors: {...this.state.formErrors, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] }, isButtonDisabled: false}, () => {
            const formValidator = validate.validateForm(this.state.render, this.state.valid);
            this.setState({ formValid: formValidator},() => {
            });
        });
    }

    onHandleSubmit = (event) => {
        this.props.addAction(eraseError(''));
        event.preventDefault();
        if (!this.state.formValid) {
            this.setState({...this.state, error: "Some of fields doesn't filled right!", isButtonDisabled: true});
        } else{
            this.props.addAction(editUser(this.state));
        }

    }

    render() {
        const formFieldsData = constructor.changeUserDataFormConstructor({ ...this.state, ...this.state.user });
        if (this.props.isEdited) {
            return <Redirect to={`${adminUrl}${usersUrl}?response=${CONSTANTS.ADMIN_USER_UPDATED}`} />;
        }
        if (!this.state.isUser) {
            return <Redirect to={`${adminUrl}${usersUrl}`} />;
        }
        return (
            <div>
                <form className="admin-users-form" onSubmit={this.onHandleSubmit}>
                    <Grid container spacing={24}>
                        {formFieldsData.map((inputItem, index) => (
                            <Grid item xs={3} key={index}>
                                <InputText fullWidth={true} error={inputItem.error} onBlur={this.onFieldBlur} label={inputItem.label} errorValue={inputItem.fieldErrors} value={inputItem.value} handleChange={this.onHandleChange} name={inputItem.name}></InputText>
                            </Grid>
                        ))}
                        <Grid item xs={12}>
                            <InputSelect
                                label="Role"
                                value={this.state.role}
                                items={baseUsersRole}
                                name='role'
                                handleChange={this.onHandleChange}
                                fullWidth={true}
                            />
                        </Grid>
                    </Grid>
                    <ButtonText disabled={this.state.isButtonDisabled} type="submit" propsValue="Submit" />
                    <div className='asd'>
                        <FormErrors fieldName="" errorValue={this.state.error} classN="requestError" />
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        users: state.adminUsersListReducer.users,
        snackbarToggle: state.snackbarReducer.snackbarToggleStatus,
        error: state.adminUsersListReducer.error,
        isEdited: state.adminUsersListReducer.isUserEdited,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        addAction: payload => dispatch(payload)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminUsersForm);

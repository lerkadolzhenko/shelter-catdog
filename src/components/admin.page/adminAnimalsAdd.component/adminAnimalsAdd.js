import React, { Component } from 'react';
import { connect } from "react-redux";
import { InputText } from "../../share.components/inputText/inputText";
import './adminAnimalsAdd.scss'
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import {addAnimal, uploadImg} from "../adminAnimalsList.component/actionCreators";
import { InputSelect } from "../../share.components/inputSelect/InputSelect";
import Grid from "@material-ui/core/Grid";
import {ButtonText} from "../../share.components/buttonText/ButtonText";
import {Redirect} from "react-router-dom";
import {adminUrl, animalsUrl } from "../../../config/apiUrl";
import {validateField} from "../../sign.component/validator";
import * as validate from  "../../sign.component/validator"
import {Switch, TextField} from "@material-ui/core";
import {FillToSubmit} from "../../share.components/fillToSubmit/fillToSubmit";
import {FormErrors} from "../../share.components/formErrors.shared";
import {ButtonUploadImg} from "../../share.components/buttonUploadImg/buttonUploadImg";
import Icon from "@material-ui/core/Icon";
import {indigo} from "@material-ui/core/colors";
import { withStyles } from '@material-ui/core/styles';




const styles = theme => ({
    colorSwitchBase: {
        color: indigo[700],
        '& + $colorBar': {
            backgroundColor: indigo[500],
        },
        '&$colorChecked': {
            color: '#f50057',
            '& + $colorBar': {
                backgroundColor: '#ff4081',
            },
        },

    },
    colorBar: {},
    colorChecked: {},

});

const selects = {
    kind: ['dog', 'cat'],
    health_status: ['healthy', 'average', 'sos'],
    is_boy: [true, false]
};
const labels = {
    name: 'Name',
    kind: 'Kind',
    arrival_date: 'Arrival date',
    birth_date: 'Birth date',
    health_status: 'Health status',
    is_sterilized: ['Sterilized','Not sterilized'],
    is_adopted: ['Adopted','Not adopted'],
    is_boy: ['Good Boy', 'Good Girl'],
    link: 'Link',
    alt_name: 'Image name',
    about: 'About',
};
let img = {};
class AdminAnimalsAdd extends Component {

    state = {
        name: '',
        kind: '',
        birth_date: new Date(),
        arrival_date: new Date(),
        health_status: '',
        is_boy: false,
        is_sterilized: false,
        is_adopted: false,
        user_id: 0,
        link: '',
        alt_name: '',
        about: '',
        img: null,
        errorInfo: {
            name: '',
            kind: '',
            arrival_date: '',
            birth_date: '',
            health_status: '',
            link: '',
            alt_name: ''

        },
        valid: {
            name: false,
            kind: false,
            birth_date: true,
            arrival_date: true,
            health_status: false,
            link: false
        },
        formValid: false,
    };


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.user && (this.state.user_id !== this.props.user.id)) {
            this.setState({
                user_id: this.props.user.id
            })
        }
        if ((this.props.img !== null) && (this.state.link !== this.props.img)) {
            this.setState({...this.state, link: this.props.img, alt_name: Math.random()*100000000 })
        }

    }

    onInputData = event => {
        let {name, value} = event.target;
        let validator = validateField(name, value);

        this.setState({ ...this.state, [name]: value, errorInfo : {...this.state.errorInfo, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] }}, () => {
            const formValidator = validate.validateAnimalForm(this.state.valid);
            this.setState({ ...this.state, formValid: formValidator});
        });
        console.log(this.state);

    };

    minDate = (name) => {
        if (this.state.arrival_date < this.state.birth_date){
            this.setState({
                arrival_date: this.state.birth_date
            })
        }
        if (name === 'arrival_date'){
            return this.state.birth_date
        } else return '2000-01-01 00:00:00.000 +00:00';

    };
    maxDate = (name) => {
       if (name === 'arrival_date'){
            return '2080-01-01 00:00:00.000 +00:00'
        } else return new Date();
    };

    onInputImg = event => {

        let bodyFormData = new FormData();
        bodyFormData.append('animal', event.target.files[0]);
        img = bodyFormData;

        const name = 'link';
        const value = event.target.files[0].name;
        let validator = validateField(name, value);
        this.setState({ ...this.state, link: URL.createObjectURL(event.target.files[0]),  errorInfo : {...this.state.errorInfo, [name]: validator.formErrors[name] }, valid: { ...this.state.valid, [name]: validator[name] }}, () => {
            const formValidator = validate.validateAnimalForm(this.state.valid);
            this.setState({ ...this.state, formValid: formValidator});
        });

    };


    onChangeCheckbox = event => {
        let {name, checked } = event.target;
        this.setState({[name]: checked});
    };

    onHandleSubmit = (event) => {

        event.preventDefault();
        // this.props.dispatch(addAnimal(this.state));
        this.props.dispatch(uploadImg(img));

        setTimeout(() => {
            this.props.dispatch(addAnimal(this.state));
        }, 100)
    };
    static isError(value){
       return value !== '';
    }



    render() {
        const {classes} = this.props;
        const stateKeys = Object.keys(this.state);

        let redirect = null;
        if (this.props.redirect) {
            redirect = <Redirect to={`${adminUrl}${animalsUrl}`} />;
        }
        let img = null;
        if (this.state.link !== ''){
            img = <img src={this.state.link}  className='img100x100' alt=""/>
        }





        return(

            <div className='mw70 mt5prs'>

                <form className="admin-users-form" onSubmit={this.onHandleSubmit}>
                    <Grid container spacing={24}>
                        {stateKeys.map((name, index) => {

                        if (name === 'user_id'
                            || name === 'errorInfo'
                            || name === 'valid'
                            || name === 'formValid'
                            || name === 'img'
                            || name === 'arrivalMinDate'
                            || name === 'alt_name') {
                                return null
                            }
                            else if (typeof this.state[name] === 'boolean') {
                                return (
                                    <Grid item xs={3} key={index} className='mt20px '>


                                            <span className='responsiveText'>{labels[name][1]}</span>
                                            <Switch   label={labels[name]}
                                                      name={name}
                                                      onChange={this.onChangeCheckbox}
                                                      checked={this.state[name]}
                                                      classes={{
                                                          switchBase: classes.colorSwitchBase,
                                                          checked: classes.colorChecked,
                                                          bar: classes.colorBar,
                                                      }}
                                            >

                                            </Switch>
                                            <span className='responsiveText'>{labels[name][0]}</span>




                                    </Grid>
                                )
                            } else if (name === 'kind' || name === 'health_status') {
                                return (
                                    <Grid item xs={3} key={index}>

                                    <InputSelect
                                        className='fullWidth asterisk'
                                            label={labels[name]}
                                            value={this.state[name]}
                                            items={selects[name]}
                                            name={name}
                                            handleChange={this.onInputData}
                                        />
                                    </Grid>
                                )

                            } else if (name === 'birth_date' || name === 'arrival_date') {
                                return (
                                    <Grid item xs={3} key={index} className='negativeMarginBott30px'>

                                    <MuiPickersUtilsProvider utils={DateFnsUtils} >
                                        <DatePicker
                                            maxDate={this.maxDate(name)}
                                            margin="normal"
                                            minDate={this.minDate(name)}
                                            label={labels[name]}
                                            value={this.state[name]}
                                            onChange={(date) => this.setState({[name]:date})}
                                            variant={"outlined"}
                                        />
                                        <Icon className='dateIcon'>date_range</Icon>
                                    </MuiPickersUtilsProvider>
                                    </Grid>
                                )

                        } else if (name === 'link'){
                                return (

                                    <Grid item xs={6} key={index}>

                                        <ButtonUploadImg name={name} onChange={this.onInputImg} textValue='Upload image'/>

                                        <FormErrors errorValue={this.state.errorInfo[name]} />
                                        {img}

                                    </Grid>
                                )


                            }

                            else if (name === 'about'){
                            return (
                                <Grid item xs={12} key={index}>
                                    <TextField multiline={true} rows={6}  variant={"outlined"} className='fullWidth' label={labels[name]} value={this.state[name]} onChange={this.onInputData} name={name}/>
                                </Grid>
                            )
                        }

                            else {
                                return (
                                    <Grid item xs={3} key={index}>
                                        <InputText className='fullWidth asterisk' label={labels[name]} value={this.state[name]} handleChange={this.onInputData} name={name}
                                                   errorValue={this.state.errorInfo[name]} error={AdminAnimalsAdd.isError(this.state.errorInfo[name])}  />
                                    </Grid>
                                )
                            }


                        })}
                    </Grid>
                    <ButtonText  type="submit" propsValue='Submit' disabled={!this.state.formValid} />
                    <FillToSubmit show={this.state.formValid} value='Fill the required fields to submit'/>
                </form>
                {redirect}

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        snackbarToggle: state.snackbarReducer.snackbarToggleStatus,
        user: state.sign.user,
        redirect: state.adminAnimalListReducer.redirect,
        img: state.adminAnimalListReducer.img,
    }
};

export default connect(mapStateToProps)(withStyles(styles)(AdminAnimalsAdd))
import React, { Component } from 'react';
import { connect } from 'react-redux';
// import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
// import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import * as actionCreators from './action-creators'
import '../sign.component/sign.scss'
import PropTypes from 'prop-types';
import { UserAvatar } from '../share.components/avatar/avatar';
import * as url from "../../config/apiUrl.js";

function mapDispatchToProps(dispatch) {
    return {
        addAction: payload => dispatch(payload)

    }

}

function mapStateToProps(state) {
    return {
        user: state.sign.user

    }

}

class SignOut extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userEmail: ''
        };
    }

    logOut = () => {
        this.props.addAction(actionCreators.logOut());
    }
    componentDidMount(){
        this.props.eraseErrors();
    }

    render() {
        const classes = {
            main: "mainSignIn",
            paper: "paperSignIn",
            fab: "fabSignIn",


        };
        return (
            <div>
                <main className={classes.main}>
                    <CssBaseline />
                    <Paper elevation={0} className={classes.paper}>
                        <Typography component="h3" variant="h6">
                            Signed As
                        </Typography>
                        <input
                            accept="image/*"
                            style={{ display: 'none' }}
                            id="updateAvatar"
                            type="file"
                            name="avatar"
                            onChange={this.props.handleLoad}
                        />
                        <label htmlFor="updateAvatar">
                            <UserAvatar errorValue={this.props.avatarErrorValue} toolTipTitle="Change Avatar" image={url.apiUrl + url.usersAuthUrl + "/upload/" + this.props.user.avatar} bigAvatar={this.props.bigAvatar} bigAvatarPlaceHolder={this.props.bigAvatarPlaceHolder} iconName="account_circle" />
                        </label>
                        <Typography color="primary" component="h3" variant="h6">
                            {this.props.user.email}
                        </Typography>
                        <Tooltip title="Log Out">
                            <Fab variant="extended" aria-label="Add" onClick={this.logOut} className={classes.fab}>
                                LogOut
                            </Fab>
                        </Tooltip>
                    </Paper>
                </main>
            </div>
        )
    }

}

SignOut.propTypes = {
    user: PropTypes.object.isRequired,
}



export default connect(mapStateToProps, mapDispatchToProps)(SignOut);
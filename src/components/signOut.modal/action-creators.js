import * as CONST from "../../constants/action-types";


export function logOut(payload) {
    localStorage.removeItem("authToken")
    return {
        type: CONST.LOG_OUT_USER,
        payload: payload
    }

}
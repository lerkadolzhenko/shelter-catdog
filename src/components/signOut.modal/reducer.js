import * as actions from "../../constants/action-types";

const initialState = {
    error: '',
    token: null
}


export function logOut(state = initialState, action) {
    switch (action.type) {
        case actions.LOG_OUT_USER:
            return {...state, token: null, error: ''}
        default:
            return state;
    }
}
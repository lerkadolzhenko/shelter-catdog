import * as React from 'react';
import { Link } from 'react-router-dom';
import './notFound.scss';
import Button from '@material-ui/core/Button';

class NotFound extends React.Component {
    render() {
        return (
            <div className="page-not-found">
                <div className="page-not-found_txt">Oops! Page not found!</div>

                <div className="page-not-fond-dog">
                    <div className="pug">
                        <div className="platform"></div>
                        <div className="body-dog"></div>
                        <div className="body-part-1"></div>
                        <div className="left-eye"></div>
                        <div className="body-part-2"></div>
                        <div className="right-eye"></div>
                        <div className="snout"></div>
                        <div className="brow"></div>
                        <div className="nose"></div>
                        <div className="burger-bun"></div>
                    </div>
                    <div className="burger">
                        <div className="salad"></div>
                        <div className="salad-copy"></div>
                        <div className="salad-shadow"></div>
                        <div className="beef"></div>
                        <div className="cheese"></div>
                        <div className="sauce"></div>
                        <div className="beef-shadow"></div>
                        <div className="sesame"></div>
                    </div>
                </div>
                <Link to={'/'}>
                    <Button variant="contained" className="not-found-btn">
                        Go home!
                    </Button>
                </Link>
            </div>
        );
    }
}
export default NotFound
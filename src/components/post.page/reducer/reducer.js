import { GET_PETS_BY_ID } from "../../../constants/action-types";

const initialState = {
	infoPets: [],
	petId: [],
};
export const infoPetsReducer = (state = initialState, action) => {
	switch (action.type) {

		case GET_PETS_BY_ID:
			return { ...state, infoPets: action.payload };

		default:
			return state;
	}
}

import React from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Slider from 'react-slick';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from "@material-ui/core/Icon";
import Grid from '@material-ui/core/Grid';
import Footer from "../Footer/Footer";
import Header from "../Header/Header"
import SnackbarComponent from '../share.components/snackbar/snackbar';
import Divider from '@material-ui/core/Divider';
import ButtonHome from "../share.components/buttonHome/buttonHome";
import ButtonVolunteer from "../share.components/buttonVolunteer/buttonVolunteer";
import ModalLoader from "../share.components/loader/loader";

import { connect } from "react-redux";
import { getInfoPets } from './actionCreators/actionCreator';

import "./post.page.scss";
import "./slider.scss";

class postPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null,
      post: null,
    };
  }

  componentDidMount() {

    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
    this.props.dispatch(getInfoPets(this.props.match.params.id))

  }


  componentDidUpdate(prevProps) {
    if (this.props.infoPets !== prevProps.infoPets) {
      this.setState({
        post: this.props.infoPets,
        is_boy: this.props.infoPets.animal.is_boy ? 'male' : 'female'
      })
    }
  };


  render() {

    const { links } = this.props.infoPets;
    const { infoPets } = this.props;
    let images = (!links) ? null : links.map((image, index) => {
      return <img src={image} alt={image} key={index} />
    });

    let snackbar = null;
    if (this.props.snackbarToggleStatus) {
      snackbar = <SnackbarComponent message='Accepted!' />
    }






    let settings_small = {

      asNavFor: this.state.nav1,
      ref: slider => (this.slider2 = slider),
      slidesToShow: 5,
      swipeToSlide: true,
      focusOnSelect: true,
      adaptiveHeight: true,
      responsive: [

        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }
      ]
    }

    let settings = {
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 1,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 3000,
      accessibility: true,
      className: 'slides',
      swipeToSlide: true,
      adaptiveHeight: true,
    };

    const petCard = (infoPets.length === 0) ? <ModalLoader /> : (
      <Card className='card_main'>
        <Slider {...settings} asNavFor={this.state.nav2}
          ref={slider => (this.slider1 = slider)} className='galery_main'>
          {images}
        </Slider>
        {(images.length === 1) ? null : (<Slider {...settings_small} asNavFor={this.state.nav1}
          ref={slider => (this.slider2 = slider)} className='galery_small'>
          {images}
        </Slider>)

        }


        <CardContent>

          <List className='card_info'>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center">

              <Grid item lg={3}>
                <ListItem className='card_info_block' >
                  <Avatar>
                    <Icon >pets</Icon>
                  </Avatar>
                  <ListItemText primary={<span className='secondary-text'>Name:</span>}
                    secondary={<span className='primary-text'>{infoPets.animal.name}</span>} />
                </ListItem>
              </Grid>

              <Grid item lg={3}>
                <ListItem className='card_info_block'>
                  <Avatar>
                    <Icon>turned_in</Icon>
                  </Avatar>
                  <ListItemText primary={<span className='secondary-text'>Kind:</span>} secondary={<span className='primary-text'>{infoPets.animal.kind}</span>} />
                </ListItem>
              </Grid>

              <Grid item lg={3}>
                <ListItem className='card_info_block'>
                  <Avatar>
                    <Icon>assignment_ind</Icon>
                  </Avatar>
                  <ListItemText primary={<span className='secondary-text'>Sex:</span>} secondary={<span className='primary-text'>{this.state.is_boy}</span>} />
                </ListItem>
              </Grid>

              <Grid item lg={3}>
                <ListItem className='card_info_block'>
                  <Avatar>
                    <Icon>face</Icon>
                  </Avatar>
                  <ListItemText primary={<span className='secondary-text'>Birth Date:</span>} secondary={<span className='primary-text'>{infoPets.animal.birth_date.slice(0, 10)}</span>} />
                </ListItem>
              </Grid>

              <ListItem className='card_info_block card_hidden'>
                <Avatar>
                  <Icon>check</Icon>
                </Avatar>
                <ListItemText primary='Health Status:' secondary={infoPets.animal.health_status} />
              </ListItem>

              <ListItem className='card_info_block card_hidden'>
                <Avatar>
                  <Icon>access_time</Icon>
                </Avatar>
                <ListItemText primary='Arrival Date:' secondary='{infoPets.animal.arrival_date}' />
              </ListItem>

              <ListItem className='card_info_block card_hidden'>
                <Avatar>
                  <Icon>access_time</Icon>
                </Avatar>
                <ListItemText primary='Is Sterilized:' secondary='{infoPets.animal.is_sterilized}' />
              </ListItem>

            </Grid>
          </List>
          <Divider variant="middle" />
          <Typography variant='body1'>
            {infoPets.animal.about}
          </Typography>
          <Typography variant="caption" gutterBottom align="right" color="primary">
            by @Admin
          </Typography>
          <div className='card_buttons'>
            <ButtonVolunteer />
            <ButtonHome />
          </div>
        </CardContent>

      </Card>
    )


    return (
      <div className="singlepostpage">
        <Header />
        {petCard}
        {snackbar}
        <Footer />
      </div >
    );;
  }
}
const mapStateToProps = (state) => {
  return {
    snackbarToggleStatus: state.snackbarReducer.snackbarToggleStatus,
    infoPets: state.infoPetsReducer.infoPets,
    animalId: state.getAnimalByIdReducer.animalById,
  }
}
export default connect(mapStateToProps)(postPage);

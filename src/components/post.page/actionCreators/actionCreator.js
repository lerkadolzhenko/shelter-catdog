import axios from "axios";
import { apiUrl } from "../../../config/apiUrl";
import { GET_PETS_BY_ID } from "../../../constants/action-types";


export const getInfoPets = (id) => {
    return (dispatch) => {
        return axios
            .get(`${apiUrl}/animal_images?id=${id}`)
            .then(response => response.data)
            .then(data => dispatch({ type: GET_PETS_BY_ID, payload: data }))
    }
}

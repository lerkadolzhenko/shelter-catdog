import React, { Component } from 'react';
import { Paper, Button, Collapse, List, ListItem} from '@material-ui/core';
import './sosInfo.component.scss';
import NothingToShow from '../../share.components/nothingToShow.component'
import { connect } from 'react-redux';
import { getSosList } from './actionCreators/actionCreator';
import PetsIcon from '@material-ui/icons/Pets';

class Sos extends Component {

  mobileHeight = 400

  state = {
    isShowMore: false,
    buttonName: 'show more',
  }

  componentDidMount() {
    this.props.dispatch(getSosList())
    const {innerHeight} = window
    if (innerHeight) {
      let amount = (innerHeight < this.mobileHeight)? 1 : Math.floor(innerHeight/150) 

      this.setState({amount})
    }
  }

  showMoreToggle = () => {
    let buttonName = (this.state.isShowMore) ? 'show more' : 'show less';
    this.setState({ isShowMore: !this.state.isShowMore, buttonName })
  }

  render() {

    const { sosList } = this.props;
    const { isShowMore, buttonName, amount } = this.state;

    let cardsBlock;

    if (sosList) {
      cardsBlock = sosList.map((item) => {
        return (
          <div className='sos-list-el' key={item.id}>
            <ListItem className="cards">
              <PetsIcon /><div className=' sos-text'>{item.description}</div>
            </ListItem>
          </div>
        )
      })
    }

    const listOfCards = (sosList.length === 0) ? (<NothingToShow />) : (
      <Paper className='sosInfo' style={{ backgroundColor: 'white' }}>
        <List>
          <div className='sos-list-first-els'>
            {cardsBlock.slice(0, amount)}
          </div>
          <Collapse in={isShowMore}>
            {cardsBlock.slice(amount)}
          </Collapse>
        </List>
        {(cardsBlock.length <= amount)? '' : (
          <div style={{ textAlign: 'center' }}>
            <Button variant='contained' className='button-blue button-with-margins' onClick={this.showMoreToggle}>{buttonName}</Button>
          </div>
        )}
      </Paper>
    );

    return (
      <div className='compSosInfo'>
        <div className='heading-white' align='center'>
          Ways To Help Us
        </div>
        {listOfCards}
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return { sosList: state.sosListReducer.sosList }
}

export default connect(mapStateToProps)(Sos);

import axios from "axios";
import { apiUrl, sosUrl } from "../../../../config/apiUrl";
import {GET_SOS_LIST, DELETE_SOS_LIST, UPDATE_SOS_LIST, CREATE_SOS_LIST, SNACKBAR_TOGGLE, SNACKBAR_RESET, SET_DEFAULT_SOS_STATUS} from '../../../../constants/action-types'

export const getSosList = () => {
    return (dispatch) => {
        axios 
            .get(apiUrl + '/sos')
            .then(response => response.data)
            .then(data => {
                dispatch({
                    type: GET_SOS_LIST,
                    payload: data 
                });
                dispatch({type: SET_DEFAULT_SOS_STATUS})
            })
    }
}
export const editSosList = (payload) => {
	return (dispatch) => {
		return axios
			.put(`${apiUrl}${sosUrl}?id=${payload.id}`, payload)
			.then(response => response.status === 200)
			.then(() => {
				dispatch({type: UPDATE_SOS_LIST, payload})})
            .then(() => dispatch({type: SNACKBAR_RESET}))
            .then(() => dispatch({type: SNACKBAR_TOGGLE}))
	}
}
export const deleteSosList = (payload) => {
    return (dispatch) => {
        return axios
            .delete(`${apiUrl}${sosUrl}?id=${payload}`)
            .then(response => response.status === 200 && response.data === ''
                ? dispatch({type: DELETE_SOS_LIST, payload})
                : dispatch({type: SNACKBAR_TOGGLE}))

    }
}
export const addSosList = (payload) => {
    return  (dispatch) => {
        return axios
            .post(`${apiUrl}${sosUrl}`, payload)
            .then(res => res.status === 200)
            .then(() => {
                dispatch({type: CREATE_SOS_LIST, payload})
            })
            .then(() => dispatch({type: SNACKBAR_RESET}))
            .then(() => dispatch({type: SNACKBAR_TOGGLE}))
    }
};

export default {getSosList, addSosList, editSosList, deleteSosList}
import {GET_SOS_LIST, UPDATE_SOS_LIST, DELETE_SOS_LIST, CREATE_SOS_LIST, SET_DEFAULT_SOS_STATUS} from '../../../../constants/action-types'

const initialState = {
    sosList: [],
    reqStatus: false,
    sosListAdded: false,
    sosListEdited: false,
    sosListDeleted: false
}

export const sosListReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_SOS_LIST: 
            return {...state, sosList: action.payload};
        
        case UPDATE_SOS_LIST:
            return { ...state, sosList: state.sosList.map((item) =>
                               item.id === action.payload.id ? {...action.payload} : item), 
                               sosListEdited: true, sosListAdded: false, sosListDeleted: false,
                               reqStatus: true };

        case DELETE_SOS_LIST:
            return {...state, sosList: state.sosList.filter((item) => item.id !== action.payload), 
                              sosListEdited: false, sosListAdded: false, sosListDeleted: true,
                              reqStatus: true };
        
        case CREATE_SOS_LIST:
            return {...state, sosListEdited: false, sosListAdded: true, sosListDeleted: false,
                              reqStatus: true };

        case SET_DEFAULT_SOS_STATUS: 
            return { ...state, reqStatus: false };

        default: return state;
    }
}
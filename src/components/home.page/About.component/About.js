import React from 'react';
import Grid from '@material-ui/core/Grid';
import './About.scss';
import Button from '@material-ui/core/Button';
import { withRouter } from "react-router-dom";


class About extends React.Component {
  infografClick = () => {
    this.props.history.push('/charts');
  }
  galleryClick = () => {
    this.props.history.push('/gallery');
  }
  render() {
    return (
      <div className='about-component'>
        <div className='about_content'>
          <div className='heading-white' color='inherit' align='center'>
            About Us
      </div>
          <div className='about_inside'>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Grid container spacing={16}>
                  <Grid item xs={12} md={6} >
                  <Button onClick={this.galleryClick} className='about_pets-bg about_button animation-button'>
                      <div className='about_header-button'>Our Pets</div>
                    </Button>
                  </Grid>
                  <Grid item xs={12} md={6} >
                    <Button onClick={this.infografClick} className='about_info-bg about_button animation-button'>
                      <div className='about_header-button'>Infocharts</div>
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <div className='text-white'>
                  <div>The salary of the Executive Director is determined by the Board of Directors. Any change in this salary must be presented and voted on by the Board. Decisions are only made by a majority vote.</div>
                  <p>Animal Haven does not share, sell or rent donor information. Donor information is only used to communicate with donors via email, mail and phone. Donors are always offered the opportunity to remove their name from our list.</p>
                  <p>If any employee reasonably believes that some policy, practice, or activity of Animal Haven is in violation of law, a written complaint must be filed by that employee with the Executive Director or the Board President.  It is the intent of Animal Haven to adhere to all laws and regulations that apply to the organization and the underlying purpose of this policy is to support  the organization’s goal of legal compliance. </p>
                  <p>Board members and staff notify all board members of any perceived or potential conflicts of interest and abstain from voting on any issues where a conflict of interest has been determined to exist.</p>
                </div>
              </Grid>
            </Grid>

          </div>

        </div>
      </div>
    );
  }
}

export default withRouter(About);
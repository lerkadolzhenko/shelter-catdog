import axios from "axios";
import { apiUrl } from "../../../../config/apiUrl";
import { GETTING_ANIMALS } from "../../../../constants/action-types";
import { POST_CLICKED } from "../../../../constants/action-types";

export function getAnimalById(id) {
    return function (dispatch) {
        return dispatch({type: POST_CLICKED, payload: id})
    }

}

export const miniGalGetPets = () => {
    return(dispatch) =>{//action for zapros
    return axios//zapros
    .get(`${apiUrl}/animal_images/animals?amount=6`)
    .then(response => response.data)
    .then(data=>dispatch({type:GETTING_ANIMALS, payload: data}))
    }
}

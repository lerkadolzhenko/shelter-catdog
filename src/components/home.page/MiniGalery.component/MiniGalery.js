import React from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import './MiniGalery.scss';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { miniGalGetPets } from './actionCreators/actionCreator';
import { getAnimalById } from "./actionCreators/actionCreator"
import { withRouter } from "react-router-dom";
import NothingToShow from '../../share.components/nothingToShow.component';


class MiniGalery extends React.Component {
    getGridListCols = () => {
        if (isWidthUp('xl', this.props.width)) {
            return 3;
        }
        if (isWidthUp('md', this.props.width)) {
            return 3;
        }
        if (isWidthUp('sm', this.props.width)) {
            return 2;
        }
        return 1;
    }
    componentDidMount() {
        this.props.dispatch(miniGalGetPets())
    }

    handleClick = () => {
        this.props.history.push("/gallery");
    }

    infoClick = () => {
        this.props.history.push("/post");
    }

    onPostClick = (id) => {
        this.props.dispatch(getAnimalById(id));
        this.props.history.push(`/post/${id}`)
    };

    render() {
        const { miniGal } = this.props;

        miniGal.forEach(miniGal => {
            if (miniGal.is_boy.toString() === 'true') {
                miniGal.sexType = 'male'
            } else miniGal.sexType = 'female'
        });

        const MiniGalery = (miniGal.length === 0) ? (<NothingToShow />) : (
            <GridList spacing={24} cellHeight={300} cols={this.getGridListCols()} className='mini_grid'>
                {miniGal.map((item, index) => (

                    <GridListTile key={index} >
                        <div className='mini_img-container'>
                            <img src={item.link} className='mini_img' onClick={() => this.onPostClick(item.animal_id)} alt={item.alt} />
                        </div>
                        <GridListTileBar
                            title={item.name} className='text-white mini_tile' onClick={() => this.onPostClick(item.animal_id)}
                            subtitle={<span> sex: {item.sexType} , health status: {item.health_status}</span>}
                            actionIcon={
                                <IconButton color="inherit">
                                    <InfoIcon />
                                </IconButton>
                            }
                        />
                    </GridListTile>
                ))}
            </GridList>
        );

        return (
            <div className='mini-gal-component'>
                <div className='heading-white' align='center'>
                    Our Lovely Pets
                </div>
                {MiniGalery}
                <div style={{ textAlign: 'center' }}>
                    <Button variant='contained' className='button-orange' onClick={this.handleClick}> View all pets</Button>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return { miniGal: state.miniGalReducer.miniGal }
}
export default connect(mapStateToProps)(withRouter(withWidth()(MiniGalery)));
import {GETTING_ANIMALS} from "../../../../constants/action-types";
import { POST_CLICKED } from "../../../../constants/action-types";

const initialState = {
	miniGal: [], //massiv with tvari
	animalById: null
};
export const miniGalReducer = (state = initialState, action) => {
	switch (action.type) {

		case GETTING_ANIMALS:
			return {...state,	miniGal: action.payload};

		default:
			return state;
	}
}

export const getAnimalByIdReducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_CLICKED:
            return { animalById: action.payload };
		default: 
		return state
    }
};

import React, { Component } from "react";
import './FirstInfo.scss'

export default class FirstInfo extends Component {
  render() {
    return (
      <div className='first-info-component'>
        <img alt='Img About us' className='first_img1' src={require("../../../assets/2.jpg")} />
        <img alt='Img About us' className='first_img2' src={require("../../../assets/3.jpg")} />
        <img alt='Img About us' className='first_img3' src={require("../../../assets/1.jpg")}/>

      </div>
    );
  }
}
import React from 'react';
import Grid from '@material-ui/core/Grid';
import Place from '@material-ui/icons/Place';
import LocalPhone from '@material-ui/icons/LocalPhone';
import Mail from '@material-ui/icons/Mail';
import IconButton from '@material-ui/core/IconButton';
import './Contacts.scss';
import { connect } from 'react-redux';
import { getContacts } from './acrionCreators/actionCreator';
import PetsIcon from '@material-ui/icons/Pets';
import NothingToShow from '../../share.components/nothingToShow.component';

class Contacts extends React.Component {
    componentDidMount() {
        this.props.dispatch(getContacts())
    }
    render() {
        const { contacts } = this.props;


        const Contacts = (contacts.length === 0) ? (<NothingToShow />) : (
            <Grid className='contacts_content' container>
                {contacts.map((item, index) => (
                    <Grid item xs={12} md={6} key={index}>

                        <div className='header-white'>
                            <IconButton color="inherit">
                                <PetsIcon />
                            </IconButton>
                            Shelter Cat&Dog
                            </div>

                        <div className='text-white'>

                            <p>
                                <IconButton color="inherit" >
                                    <Place />
                                </IconButton>
                                Adress: {item.shelter_address}
                            </p>

                            <p>
                                <IconButton color="inherit" >
                                    <LocalPhone />
                                </IconButton>
                                Telephone: <a href={`tel:${item.shelter_phone}`}> {item.shelter_phone}</a>
                            </p>

                            <p>
                                <IconButton color="inherit" >
                                    <Mail />
                                </IconButton>
                                E-mail:<a href={`mailto:${item.shelter_email}`}> {item.shelter_email} </a> 
                            </p>
                        </div>
                    </Grid>
                ))}
                {contacts.map((item, index) => (
                    <Grid item xs={12} md={6} key={index}>
                        <iframe className='contacts_map' title="Map" src={item.shelter_map_link}></iframe>
                    </Grid>
                ))}
            </Grid>
        );

        return (
            <div className='contact-component'>
                <div className='heading-white' align='center'>
                    Contacts
                </div>
                {Contacts}
            </div>
        );
    }

}
const mapStateToProps = (state) => {
    return { contacts: state.getContactsReducer.contacts }
}

export default connect(mapStateToProps)(Contacts);

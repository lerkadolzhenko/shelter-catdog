import axios from "axios";
import { apiUrl } from "../../../../config/apiUrl";
import { GETTING_CONTACTS } from "../../../../constants/action-types";


export const getContacts = () => {
    return(dispatch) =>{//action for zapros
    return axios//zapros
    .get(`${apiUrl}/shelter_contacts`)
    .then(response => response.data)
    .then(data=>dispatch({type:GETTING_CONTACTS, payload: data}))
    }
}
import {GETTING_CONTACTS} from "../../../../constants/action-types";

const initialState = {
	contacts: [], //massiv with tvari
};
export const getContactsReducer = (state = initialState, action) => {
	switch (action.type) {

		case GETTING_CONTACTS:
			return {...state,	contacts: action.payload};

		default:
			return state;
	}
}

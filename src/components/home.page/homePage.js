import MiniGalery from './MiniGalery.component/MiniGalery';
import SosInfo from './sosInfo.component';
import About from './About.component/About';
import Contacts from './Contacts.component/Contacts';
import FirstInfo from './FirstInfo.component/FirstInfo';
import Donate from '../donate/donate';
import React from "react";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import { connect } from 'react-redux';
import Dialog from '../share.components/dialog/dialog';
import "./homePage.scss";
import Axios from 'axios';
import queryString from 'query-string'
import * as CONSTANTS from "../../constants/messages";
import * as actionCreators from "../sign.component/action-creator";
import {Element, scroller} from "react-scroll";
import DonateChips from '../donate.page/donateChips.component';
import Button from '@material-ui/core/Button';

function mapStateToProps(state) {
    return {
        isRegistered: state.sign.isRegistered,
        message: state.sign.error
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addAction: payload => dispatch(payload)
    }
}

function buttons(action, title, className = []) {
    let res = [];
    for(let i = 0; i < action.length; i++){
        res.push({
            title: title[i],
            action: action[i],
            className: className[i]
        })
    }
    return res;
}

(function() {
    const token = localStorage.getItem("authToken");
    if (token) {
        Axios.defaults.headers.common['Authorization'] = token;
    } else {
        Axios.defaults.headers.common['Authorization'] = null;
    }
})();

class homePage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            isModalOpened: true,
            query: {
                response: null
            },
            message: ''
        }
    }

    handleInfoModalClose = () =>{
        this.setState({isModalOpened: false});
        this.props.addAction(actionCreators.eraseErrors(""));
        this.props.addAction(actionCreators.setDefaultRegisterState(""));
        this.props.history.replace({...this.props.history, query: null});
    }

    componentDidMount(){
        if (this.props.location.search) {
            this.setState({query: queryString.parse(this.props.location.search)});
        }

    }

    componentDidUpdate(nextProps){
        if ((this.props.message === CONSTANTS.CONFIRM_YOUR_EMAIL_MESSAGE) && (this.props.message !== nextProps.message) ) {
            this.setState({message: this.props.message});
            this.setState({isModalOpened: true});
        }
        if (this.props.isRegistered && !this.state.isModalOpened) {
            this.setState({isModalOpened: true});
        }
    }

    strollToComponent = (el) => {
        scroller.scrollTo(el, {
            duration: 500,
            smooth: true,
            offset: -78,
      })
    }

    handleClick = () => {
        this.props.history.push("/donate");
    }

	render() {
        let dialog = null;
        if ((this.props.isRegistered && this.state.isModalOpened) || (this.state.message.length > 0 && this.state.isModalOpened)){
            dialog = <Dialog
                state={this.state.isModalOpened}
                onCloseAction={this.handleInfoModalClose}
                title={this.state.message ? "Confirm your Email!" : "Confirmation" }
                text={this.state.message ? this.state.message : "Please confirm your account via link that we sent to Your email. Link will expire in 1 day!"}
                buttons={buttons([this.handleInfoModalClose], ["Close"], ["dialogCloseButtonFront"])}
            ></Dialog>
        }
        if (this.state.query.response && this.state.isModalOpened) {
            dialog = <Dialog
            state={this.state.isModalOpened}
            onCloseAction={this.handleInfoModalClose}
            title="Confirmed!"
            text= {this.state.query.response}
            buttons={buttons([this.handleInfoModalClose], ["Close"], ["dialogCloseButtonFront"])}
            ></Dialog>
        }

		return (
			<div className="homePage">
				<Header strollToComponent={this.strollToComponent} />
                <Element name="FirstInfo"><FirstInfo/></Element>
                <Element name="MiniGalery"><MiniGalery/></Element>
                <Element name="SosInfo"><SosInfo/></Element>
                <Element name="Donate">
                    <div className='body'>
                        <div className='card-div'>
                            <Donate/>
                            <DonateChips titleColor='white' type='last' amount={3} name='Last donations' />
                            <div style={{textAlign: 'center'}}>
                                <Button style={{marginTop: '1em'}} variant="contained" className='button-orange'
                                        onClick={this.handleClick}>show more</Button>
                            </div>
                        </div>
                    </div>
                </Element>
                <Element name="About"><About/></Element>
                <Element name="Contacts"><Contacts/></Element>
				<Footer/>
                {dialog}
			</div>
		)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(homePage);
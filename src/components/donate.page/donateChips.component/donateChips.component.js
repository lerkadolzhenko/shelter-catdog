import React, {PureComponent} from 'react'
import { Paper, Grid, Avatar, CssBaseline } from '@material-ui/core'
import {apiUrl, donatesUrl} from '../../../config/apiUrl'
import axios from 'axios'

class DonateChips extends PureComponent {

    state = {
        info: null
    }

    componentDidMount() {
        const {type, amount} = this.props;
        axios
            .get(`${apiUrl}${donatesUrl}/${type}/${amount}`)
            .then(res => {
                for (const el of res.data) {
                    el.fullname = el.user? el.user.fullname : 'Anonymous'
                    if (el.user) {
                        el.image = el.user.image                 
                    }
                }
                this.setState({info: res.data})
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {

        const {info} = this.state
        const {name, titleColor} = this.props;

        const data = (info)? info.map(item => {
            const date = new Date(item.date)
            return (
                <Grid item xs={12} sm={6} md={6} lg={4} key={item.id}>
                    <Paper className='circle-chips'>
                        {item.image ? (
                            <Avatar className='circle-chips-avatar' src={item.image} />
                        ) : (
                            <Avatar className='circle-chips-avatar' > {item.fullname.charAt(0)} </Avatar>
                        )}
                        <span className='circle-chips_text'>
                            <div className='chips-text_name'>
                                {item.fullname.length > 15? item.fullname.slice(0, 15) + '...' : item.fullname}
                            </div>
                            <span className='chips-text_amount'>
                                ₴ {item.cash_amount}
                            </span>
                            <div className='chips-text_date'>
                                {`${date.toLocaleDateString()}\xa0\xa0${date.toLocaleTimeString()}`}
                            </div>
                        </span>
                    </Paper>
                </Grid>
        )}) : null
    
        const chipsBlock = (!data)? null : (
            <div className='donate-page_spacing-block'>
                <div className={`heading-${titleColor}`} align='center'>
                    {name}
                </div>
                <CssBaseline />
                <Grid  container spacing={16}>
                    {data}
                </Grid>
            </div>
        )

        return (
            <div>
                {chipsBlock}
            </div>
        )
    }
}

export default DonateChips;
import React from "react";
import Donate from '../donate/donate'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import './donate.page.scss'
import { Paper } from '@material-ui/core'
import DonateChips from './donateChips.component'
import ReportsBlock from './reportsBlock.component'

const DonatePage = () => {

    const volunteerImages = ['https://d3td2int7n7fhj.cloudfront.net/sites/default/files/styles/crop_16_9_960x540/public/media/image/2017-06/IMG_0620.jpg?h=dbad6c61&itok=AoUahm6U',
                       'https://petallianceorlando.org/wp-content/uploads/2014/02/volunteer-with-kittens.jpg',
                       'http://d33fcr4ib8jtx6.cloudfront.net/wp-content/uploads/Volunteer_Girl.jpg']

    const volunteersBlock = (
        <div className='text-align_center donate-page_spacing-block'>
            <Paper className='volunteer-img-block'>
                <div className='heading-blue' style={{margin: '.3em 0'}} align='center'>Our volunteers</div>
                {volunteerImages.map((item, i) => {
                    return (
                        <span>
                            <span className='volunteer-img-container'>
                                <img src={item} alt='img' className='volunteer-img'></img>
                            </span>
                            {(i+1) % 3 === 0? <br /> : null}
                        </span>
                    )
                })}
            </Paper>
        </div>
    )

    return (
        <div>
            <Header />
            <div className='donates-background-block'>
                <div className='donate-standard-block'>
                    <Donate />
                    <DonateChips titleColor='black' type='last' amount={3} name='Last donations' />
                </div>
                {volunteersBlock}
                <div className='donate-standard-block'>
                    <DonateChips titleColor='black' type='top' amount={3} name='Top donations' />
                </div>
                <div className='donate-standard-block'>
                    <ReportsBlock />
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default DonatePage
import React, {PureComponent} from 'react'
import {Paper, Grid, Button, Collapse} from '@material-ui/core'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import './reportsBlock.component.scss'
import { connect } from "react-redux";
import actions from "../../admin.page/adminReportsList.component/actionCreators/actionCreators";

class ReportsBlock extends PureComponent {

    amount = 6;

    state = {
        buttonName: 'show more',
        isShowMore: false,
    }

    componentDidMount() {
        this.props.getReports()
    }

    showMoreToggle = () => {
      let buttonName = (this.state.isShowMore) ? 'show more' : 'show less';
      this.setState({ isShowMore: !this.state.isShowMore, buttonName })
    }

    render() {

        const {buttonName, isShowMore} = this.state
        const {reports, downloadReport} = this.props
        
        const leportsBlock = (reports.length === 0)? null : reports.map((item) => {
            return (
                <Grid item xs={12} sm={6} md={4} lg={4} key={item.id}>
                    <div className='reports-block-element'>
                        <span style={{display: 'inline-block'}}>
                            <span className='report-block-element-date'>
                                {item.creation_date}
                            </span><br />
                            <span className='report-block-element-title'>
                                {item.title.length > 15? item.title.slice(0, 15) + '...' : item.title}
                            </span>
                        </span>
                        <span className='report-block-element-icon' onClick={() => downloadReport(item.text)}>
                            <ArrowDownward fontSize='large'></ArrowDownward>
                        </span>
                    </div>
                </Grid>
            )
        })

        const reportsComponent = !(leportsBlock)? null : (
            <Paper className='donate-page_spacing-block reports-block'>
                <div className='heading-blue' style={{padding: '.3em 0'}} align='center'>Reports</div>
                <Grid container spacing={32}>
                    {leportsBlock.slice(0, this.amount)}
                </Grid>
                <Collapse in={isShowMore}>
                    <Grid container spacing={32} className='grid-spacing_margin-top'>
                        {leportsBlock.slice(this.amount)}
                    </Grid>
                </Collapse>
                {(reports.length <= this.amount)? null : (
                    <div style={{ textAlign: 'center' }}>
                        <Button style={{marginTop: '1em'}} 
                                onClick={this.showMoreToggle} 
                                variant='contained' 
                                className='button-blue' >{buttonName}</Button>
                    </div>
                )}
            </Paper>
        )

        return (
            <div>
                {reportsComponent}
            </div>
        )
    }
}

const mapStateToProps = state => {
	return {
		reports: state.adminReportsListReducer.reports
	}
}
export default connect(mapStateToProps, actions)(ReportsBlock);
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {orange} from "@material-ui/core/colors";
import './filterBar.scss'
import Grid from "@material-ui/core/Grid";
import {TextField} from "@material-ui/core";


const theme = createMuiTheme({
    palette: {

        primary: orange,
        secondary: {
            main: orange[700]
        }
    },
    typography: {useNextVariants: true},
});

class FilterBar extends Component {
    state = {
        labelWidth: 0,
    };

    componentDidMount() {
        this.setState({
            labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
        });
    }

    render() {
        const {sex, kind, healthStatus, isCut, onChange, searchValue, onSearch} = this.props;

        return (

            <div className='mtop'>
                <div className='heading-black' align='center'>
                    Pets Gallery
                </div>
                <form autoComplete="off">
                    <div className='filterGroup'>
                        <MuiThemeProvider theme={theme}>

                            <Grid container spacing={8}>
                                <Grid item xs={12} sm={6} md={4}>
                                    <FormControl className='searchBar  filtBarFormControl'>

                                        <TextField placeholder='Search name...'
                                                   value={searchValue}
                                                   onChange={onSearch}
                                                   name="searchValue"
                                                   variant={'outlined'}
                                        >
                                        </TextField>
                                    </FormControl>
                                </Grid>
                                <Grid item  xs={6} sm={6} md={2}>
                                    <FormControl variant="outlined" className='filtBarFormControl'>
                                        <InputLabel ref={ref => {
                                            this.InputLabelRef = ref;
                                        }}
                                                    htmlFor="outlined-age-simple">
                                            Kind
                                        </InputLabel>
                                        <Select value={kind}
                                                onChange={onChange}
                                                input={
                                                    <OutlinedInput labelWidth={this.state.labelWidth}
                                                                   name="kind"
                                                                   id="outlined-kind-simple"
                                                    className='fullWidth'/>
                                                }>
                                            <MenuItem value=''><em>All</em></MenuItem>

                                            <MenuItem value='dog'>Dog</MenuItem>
                                            <MenuItem value='cat'>Cat</MenuItem>
                                            <MenuItem value='parrot'>Parrot</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6} sm={4} md={2}>
                                    <FormControl variant="outlined" className='filtBarFormControl'>
                                        <InputLabel
                                            ref={ref => {
                                                this.InputLabelRef = ref;
                                            }}
                                            htmlFor="outlined-sex-simple">
                                            Sex
                                        </InputLabel>
                                        <Select value={sex}
                                                onChange={onChange}
                                                input={
                                                    <OutlinedInput labelWidth={this.state.labelWidth}
                                                                   name="sex"
                                                                   id="outlined-sex-simple"/>
                                                }>
                                            <MenuItem value=''><em>All</em></MenuItem>
                                            <MenuItem value={true}>Male</MenuItem>
                                            <MenuItem value={false}>Female</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6} sm={4} md={2}>
                                    <FormControl variant="outlined" className='filtBarFormControl'>
                                        <InputLabel
                                            ref={ref => {
                                                this.InputLabelRef = ref;
                                            }}
                                            htmlFor="outlined-status-simple">
                                            Status
                                        </InputLabel>
                                        <Select value={healthStatus}
                                                onChange={onChange}
                                                input={
                                                    <OutlinedInput labelWidth={this.state.labelWidth}
                                                                   name="healthStatus"
                                                                   id="outlined-healthStatus-simple"/>
                                                }>
                                            <MenuItem value=''><em>All</em></MenuItem>
                                            <MenuItem value='healthy'>Healthy</MenuItem>
                                            <MenuItem value='average'>Average</MenuItem>
                                            <MenuItem value='sos'>SOS</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6} sm={4} md={2}>
                                    <FormControl variant="outlined" className='filtBarFormControl'>
                                        <InputLabel
                                            ref={ref => {
                                                this.InputLabelRef = ref;
                                            }}
                                            htmlFor="outlined-sex-simple">
                                            Castrated
                                        </InputLabel>
                                        <Select value={isCut}
                                                onChange={onChange}
                                                input={
                                                    <OutlinedInput labelWidth={this.state.labelWidth}
                                                                   name="isCut"
                                                                   id="outlined-isCut-simple"/>
                                                }>
                                            <MenuItem value=''><em>All</em></MenuItem>
                                            <MenuItem value={true}>Cut</MenuItem>
                                            <MenuItem value={false}>Not cut</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>

                       </MuiThemeProvider>
                    </div>
                </form>
            </div>
        );
    }
}


export default (FilterBar);

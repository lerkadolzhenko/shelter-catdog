import React, { Component } from 'react';
import { connect } from "react-redux";
import FilterBar from './filterBar/filterBar'
import CardList from "./cardList/cardList";
import Pagin from "./pagination/pagination";
import Header from "../Header/Header"
import SnackbarComponent from '../share.components/snackbar/snackbar';
import { getAnimals } from "./actionCreators";
import { getAnimalById } from "./cardList/actionCreators"


class GalleryPage extends Component {

    state = {
        cards: [],
        kind: '',
        sex: '',
        healthStatus: '',
        isCut: '',
        searchValue: '',
        offset: 0,
        current: 0,
        next: 12,
        filtered: false,
        postClicked: null,

    };

    defaultList = () => {
        this.setState({
            cards: this.props.cardsList,
        })
    };
    componentDidMount() {
        this.props.dispatch(getAnimals());
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.cardsList !== prevProps.cardsList) {
            this.defaultList();
        }
        if (this.state.kind !== prevState.kind
            || this.state.sex !== prevState.sex
            || this.state.healthStatus !== prevState.healthStatus
            || this.state.isCut !== prevState.isCut) {
            setTimeout(() => { this.defaultList(); this.onFilter() }, 1)
        }
        if (this.state.searchValue !== prevState.searchValue) {
            this.filterState();
            if (this.state.searchValue === '') {
                setTimeout(() => { this.defaultList(); this.onFilter() }, 1)

            }
        }

    }


    filterSelected = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };


    onSearch = (event) => {
        this.setState({
            searchValue: event.target.value
        });
        this.onFilter()
    };


    onFilter = () => {
        this.setState({
            current: 0,
            offset: 0,
            filtered: true,
            next: this.state.offset + 12,
        });
        this.filterState();
    };


    onPostClick = (id) => {
        this.props.dispatch(getAnimalById(id));
        this.props.history.push(`/post/${id}`)
    };

    paginationClick = (e, offset) => {
        this.setState({
            current: offset,
            next: offset + 12,
            offset
        })
    };
    filterState = () => {
        return (
            this.setState({
                cards: this.props.cardsList
                    .filter(card => this.state.kind === '' ? true : card.kind === this.state.kind)
                    .filter(card => this.state.sex === '' ? true : card.is_boy === this.state.sex)
                    .filter(card => this.state.healthStatus === '' ? true : card.health_status === this.state.healthStatus)
                    .filter(card => this.state.isCut === '' ? true : card.is_sterilized === this.state.isCut)
                    .filter(card => card.name.toLowerCase().includes(this.state.searchValue.toLowerCase()))
            })
        )
    }


    render() {
        let snackbar = null;
        if (this.props.snackbarToggleStatus) {
            snackbar = <SnackbarComponent message='Accepted!' />
        }
        return (
            <div>
                <Header />

                <FilterBar kind={this.state.kind}
                           sex={this.state.sex}
                           isCut={this.state.isCut}
                           healthStatus={this.state.healthStatus}
                           onChange={this.filterSelected}
                           searchValue={this.state.searchValue}
                           onSearch={this.onSearch}
                />
                <CardList cards={this.state.cards}
                          filtered={this.state.filtered}
                          current={this.state.current}
                          next={this.state.next}
                          onPostClick={this.onPostClick}
                />
                <Pagin cards={this.state.cards}
                       onPagination={this.paginationClick}
                       offset={this.state.offset}
                />

                {snackbar}
            </div>
        );
    }


}

const mapStateToProps = state => {
    return {
        cardsList: state.animalListReducer.cards,
        snackbarToggleStatus: state.snackbarReducer.snackbarToggleStatus
    }
};
export default connect(mapStateToProps)(GalleryPage);

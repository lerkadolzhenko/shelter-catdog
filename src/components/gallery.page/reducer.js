import { GET_ANIMALS } from "../../constants/action-types";

const initialState = {
    cards: []
};

export const  animalListReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ANIMALS:
            return { cards: action.payload};
        default: return state
    }
};
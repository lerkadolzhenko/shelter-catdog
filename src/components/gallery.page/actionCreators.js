import axios from 'axios';
import {apiUrl, animalFull} from "../../config/apiUrl";
import { GET_ANIMALS} from "../../constants/action-types";

export function getAnimals() {
    return function (dispatch) {
        return axios
            .get(`${apiUrl}${animalFull}`)
            .then(res => res.data)
            .then(res => {dispatch({type: GET_ANIMALS, payload: res})
            });
    }

}
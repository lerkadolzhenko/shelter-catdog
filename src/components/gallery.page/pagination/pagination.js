import React from 'react';
import Pagination from "material-ui-flat-pagination";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import './pagination.scss'
import '../../../style/constants.scss'


const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#009688',
        },
        secondary: {
            main: '#f18c26',
        },
    },
    typography: {
        useNextVariants: true,
    }
});

function Pagin(props) {
    const { onPagination, offset, cards } = props;
    if (cards.length > 12) {
        return (

            <div className='paginationLocation'>
                <MuiThemeProvider theme={theme}>

                    <Pagination
                        currentPageColor="primary"
                        otherPageColor="secondary"
                        limit={12}
                        offset={offset}
                        total={cards.length}
                        onClick={onPagination}
                        size={"large"}
                    />
                </MuiThemeProvider>
            </div>
        )
    }
    return <div></div>

}

export default (Pagin)
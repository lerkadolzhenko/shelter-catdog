import React from 'react';
import classNames from 'classnames';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import './cardList.scss';


import ButtonHome from '../../share.components/buttonHome/buttonHome';
import Loader from '../../share.components/loader/loader'




function CardList(props) {

    const { cards, current, next, filtered, onPostClick } = props;
    const catIcon = require('../../../assets/catIcon1.png');
    const dogIcon = require('../../../assets/dogIcon.png');
    const maleIcon = require('../../../assets/maleIcon.png');
    const femaleIcon = require('../../../assets/femaleIcon.png');

    cards.forEach(card => {
        if (card.kind === 'dog') {
            card.kindIcon = dogIcon
        } else card.kindIcon = catIcon
    });
    cards.forEach(card => {
        if (card.is_boy.toString() === 'true') {
            card.sexType = maleIcon
        } else card.sexType = femaleIcon
    });

    cards.forEach(card => {
        if (card.is_sterilized.toString() === 'true') {
            card.isCut = "Sterilised"
        } else
            card.isCut = 'Not sterilised'
    });

    if (cards.length === 0 && !filtered){
        return (
            <Loader/>
        )
    }
    else if (cards.length === 0 && filtered) {
        return (
            <div className='noMatches'>
                <CssBaseline />

                <Typography  variant={"display4"}>
                    <div className='respFSize805040'>
                        No matches found
                    </div>
            </Typography>
            </div>
        )
    } else
        return (


            <div>
                <CssBaseline />

                <main>

                    <div className={classNames('layout cardGrid')}>

                        {/* End hero unit */}
                        <Grid container spacing={40}>
                            {cards.slice(current, next).map(card => (
                                <Grid item key={card.animal_id} xs={12} sm={6} md={4} lg={3}>

                                        {/*<Card className={classes.card}>*/}
                                            <Card className='galleryCard'>

                                            <CardMedia
                                                onClick={() => onPostClick(card.animal_id)}
                                                className='cardMedia'
                                                image={card.link}
                                                alt='Animal image'
                                            />
                                            <CardContent className='cardContent' onClick={() => onPostClick(card.animal_id)}>
                                                <Typography gutterBottom variant="h5" component="h2" className='fontSaira22'>
                                                        {card.name}
                                                </Typography>
                                                <Typography>
                                                    <img className='icons' src={card.kindIcon} alt={card.kind} />
                                                    <img className='icons' src={card.sexType} alt={card.sexType} />
                                                </Typography>
                                                <Typography className='fontSaira16'>
                                                        Health status: {card.health_status}
                                                </Typography>
                                                <Typography className="fontSaira16">
                                                        {card.isCut}
                                                </Typography>
                                            </CardContent>
                                            <CardActions className='centered'>
                                                {/*<ButtonVolunteer />*/}
                                                <ButtonHome />
                                            </CardActions>

                                        </Card>


                                </Grid>
                            ))}
                        </Grid>

                    </div>
                </main>
            </div>

        );
}


export default (CardList);
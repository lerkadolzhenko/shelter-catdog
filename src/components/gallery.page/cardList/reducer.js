import { POST_CLICKED } from "../../../constants/action-types";

const initialState = {
    animalById: null
};

export const getAnimalByIdReducer = (state = initialState, action) => {
    switch (action.type) {
        case (POST_CLICKED):
            return { animalById: action.payload };
        default: return state
    }
};
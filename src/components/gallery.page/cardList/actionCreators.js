import { POST_CLICKED } from "../../../constants/action-types";

export function getAnimalById(id) {
    return function (dispatch) {
        return dispatch({type: POST_CLICKED, payload: id})
    }

}
import * as CONST from "../constants";
import axios from 'axios';

export function createPost(payload) {
    return {
        type: CONST.CREATE_POST,
        payload
    }
}
export const apiUrl = 'http://localhost:5000/api';

export const adminUrl = '/admin';
export const usersUrl = '/users';
export const expenseUrl = '/expenses';
export const animalsUrl = '/animals';
export const animalFull = '/animal_images/animals';
export const animalImgUpload = '/animal_images/upload';
export const animalImg = '/animal_images/image';
export const usersAuthUrl = '/users/authenticate';
export const reportsUrl = '/reports';
export const donatesUrl = '/donates';
export const sosUrl = '/sos';
export const contactsUrl = '/shelter_contacts';

export const updateUrl = '/update';
export const deleteUrl = '/delete';

export const apiMapUrl = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2654.5409144135633!2d25.930304515206522!3d48.29244734834145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4734089965131759%3A0xbb45eac6058a9181!2z0KLQtdCw0YLRgNCw0LvRjNC90LDRjyDQv9C7LiwgMywg0KfQtdGA0L3QvtCy0YbRiywg0KfQtdGA0L3QvtCy0LjRhtC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNTgwMDA!5e0!3m2!1sru!2sua!4v1544807157392'

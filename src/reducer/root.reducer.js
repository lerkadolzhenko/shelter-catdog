import { adminUsersListReducer } from "../components/admin.page/adminUsersList.component/reducer/reducer";
import { combineReducers } from "redux";
import { adminReportsListReducer } from "../components/admin.page/adminReportsList.component/reducer/reducer";
import { donatesByMonthListReducer } from "../components/donate/reducer/reducer.js"
import { snackbarReducer } from "../components/share.components/snackbar/reducer/reducer";
import { miniGalReducer } from "../components/home.page/MiniGalery.component/reducer/reducer";
import { sosListReducer } from '../components/home.page/sosInfo.component/reducer/reducer'
import { sign } from "../components/share.components/signForm.modal/reducer"
import { logOut } from "../components/signOut.modal/reducer";
import { animalListReducer } from "../components/gallery.page/reducer";
import { getContactsReducer } from "../components/home.page/Contacts.component/reducer/reducer";
import { infoPetsReducer } from "../components/post.page/reducer/reducer";
import { getAnimalByIdReducer } from "../components/gallery.page/cardList/reducer";
import { adminExpensesListReducer } from "../components/admin.page/adminExpensesList.component/reducer/reducer"
import { adminAnimalListReducer } from "../components/admin.page/adminAnimalsList.component/reducer";


const rootReducer = combineReducers({
	adminUsersListReducer,
	adminReportsListReducer,
	snackbarReducer,
	donatesByMonthListReducer,
	miniGalReducer,
	sign,
	logOut,
	animalListReducer,
	getContactsReducer,
	infoPetsReducer,
	sosListReducer,
	getAnimalByIdReducer,
	adminExpensesListReducer,

	adminAnimalListReducer
})

export default rootReducer;


export const CONFIRM_YOUR_EMAIL_MESSAGE = 'Confirm your email address via link that We sent to Your Email box!';
export const ADMIN_USER_UPDATED = 'User updated!';
export const ADMIN_ERROR_LAST_ADMIN = 'Error: Admin can`t be deleted!';
export const IMG_NOT_FOUND = 'https://previews.123rf.com/images/monicaclick/monicaclick1705/monicaclick170500023/79069305-chien-carlin-avec-casque-de-s%C3%A9curit%C3%A9-constructeur-jaune-et-c%C3%B4ne-et-404-erreur-et-signe-de-cul-de-sac-sur-po.jpg'
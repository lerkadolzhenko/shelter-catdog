import React from 'react';
import Charts from './components/charts.page'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./store/app.store";
import GalleryPage from './components/gallery.page/gallery.page'
import AdminPage from "./components/admin.page/admin.page";
import DonatePage from './components/donate.page/donate.page';
import homePage from './components/home.page/homePage';
import postPage from './components/post.page/post.page';
import NotFound from './components/notFound.page/notFound.page';

class App extends React.Component {

    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div className="App" >
                        <Switch>
                            <Route exact path='/' component={homePage} />
                            <Route path='/charts' component={Charts} />
                            <Route path='/post/:id' component={postPage} />
                            <Route path='/gallery' component={GalleryPage} />
                            <Route path='/admin' component={AdminPage} />
                            <Route path='/donate' component={DonatePage} />
                            <Route component={NotFound} />
                        </Switch>
                    </div>
                </Router>
            </Provider>
        )
    }
}

export default App;


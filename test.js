function solve(s, ops) {
    s = [...s];
    ops = [...ops];
    let mergedArray = [];
    console.log(s,ops);
    for (let i = 0, j = 0; i < s.length + ops.length; i+=2, j++){
      mergedArray[i] = s[j] === "t" ? true : false;
      mergedArray[i+1] = ops[j];
    }
    mergedArray.pop();
    let arrayHolder = [...mergedArray];
    console.log(mergedArray);
    let result = 0;
    for (let i = 1; i < ops.length + s.length; i += 2){
      mergedArray = [...arrayHolder];
      let tempPart = [...mergedArray.splice(i - 1, i + 2)];
      console.log("Temp part ---"+tempPart);
      for (let j = 0; tempPart.length !== 1; ){
        tempPart.splice(0, 0, resolveCombination(tempPart.splice(0,3)));
      }
      mergedArray.splice(i - 1, 0, ...tempPart);
      console.log(mergedArray);
      for (let j = 0; mergedArray.length !== 1; ){
        mergedArray.splice(0, 0, resolveCombination(mergedArray.splice(0,3)));
        console.log(mergedArray);
      }
      result += (mergedArray.length === 1) && mergedArray[0] ? 1 : 0;
    }
    genNums(13)
    console.log(result);
    return result;
  };
  
  function genNums(n){
    let res = [];
    for (let i = 0; i < fact(n); i++) {
      let temp = generateArrayRandomNumber(0, n-1)
      let flag = false;
      for (let i = 0; i < res.length; i++) {
        if (res[i].equals(temp)) flag = true;
      }
      if (flag) {continue}
      else {
        res[i] = temp;
      }
    }
    console.log(res)
  }
  
  Array.prototype.equals = function (array) {
      // if the other array is a falsy value, return
      if (!array)
          return false;
  
      // compare lengths - can save a lot of time 
      if (this.length != array.length)
          return false;
  
      for (var i = 0, l=this.length; i < l; i++) {
          // Check if we have nested arrays
          if (this[i] instanceof Array && array[i] instanceof Array) {
              // recurse into the nested arrays
              if (!this[i].equals(array[i]))
                  return false;       
          }           
          else if (this[i] != array[i]) { 
              // Warning - two different object instances will never be equal: {x:20} != {x:20}
              return false;   
          }           
      }       
      return true;
  }
  
  function fact(x) {
     if(x==0) {
        return 1;
     }
     return x * fact(x-1);
  }
  
  function generateArrayRandomNumber (min, max) {
      var totalNumbers = max - min + 1,
          arrayTotalNumbers = [],
          arrayRandomNumbers = [],
          tempRandomNumber;
      while (totalNumbers--) {
          arrayTotalNumbers.push(totalNumbers + min);
      }
      while (arrayTotalNumbers.length) {
          tempRandomNumber = Math.round(Math.random() * (arrayTotalNumbers.length - 1));
          arrayRandomNumbers.push(arrayTotalNumbers[tempRandomNumber]);
          arrayTotalNumbers.splice(tempRandomNumber, 1);
      }
    console.log(arrayRandomNumbers);
      return arrayRandomNumbers;
  }
  
  function resolveCombination(array){
  console.log("FROM RESOLVE:"+array)
    switch (array[1]){
      case "&":
        return array[0] && array[2];
      case "|":
        return array[0] || array[2];
      case "^":
        return array[0] ? !array[2] : array[2];
    }
  }